﻿namespace SBL.DomainModel.Models.Question
{
    using SBL.DomainModel.Models.Constituency;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Ministery;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;

    [Serializable]
    [Table("OnlineQuestions")]
    public partial class OnlineQuestions
    {
        public OnlineQuestions()
        {
            tSubQuestions = new HashSet<tSubQuestion>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuestionID { get; set; }
        public int MainQuestionID { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        [StringLength(150)]
        public string DiaryNumber { get; set; }
        public int? QuestionNumber { get; set; }
        public int QuestionType { get; set; }
        public string ConstituencyNo { get; set; }
        [StringLength(200)]
        public string ConstituencyName { get; set; }
        public int MemberID { get; set; }
        [StringLength(7)]
        public string DepartmentID { get; set; }
        [StringLength(500)]
        public string Subject { get; set; }
        public string MainQuestion { get; set; }
        public int EventId { get; set; }
        public int? Priority { get; set; }
        public DateTime? MemberSubmitDate { get; set; }
        public TimeSpan? MemberSubmitTime { get; set; }
        public int? MinistryId { get; set; }
        public string SubInCatchWord { get; set; }
        public bool? IsHindi { get; set; }
        public bool SubmitToVS { get; set; }
        public string OriMemFilePath { get; set; }
        public string OriMemFileName { get; set; }
        public int? QuestPdfLang { get; set; }

        [NotMapped]
        public virtual ICollection<tSubQuestion> tSubQuestions { get; set; }
        [NotMapped]
        public virtual ICollection<mDepartment> mDepartment { get; set; }
        [NotMapped]
        public virtual List<mConstituency> mConstituency { get; set; }
        [NotMapped]
        public virtual List<mMinsitryMinister> mMinsitryMinister { get; set; }
        [NotMapped]
        public string Message { get; set; }

        [NotMapped]
        public virtual ICollection<tQuestionModel> tQuestionModel { get; set; }
        public bool? IsMobile { get; set; }
    }
}
