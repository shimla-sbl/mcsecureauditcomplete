﻿namespace SBL.DomainModel.Models.Question
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tSubQuestionsTemp")]
    [Serializable]
    public partial class tSubQuestionTemp
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SubQuestionID { get; set; }

        public int? QuestionTempId { get; set; }

        public string SubQuestionNum { get; set; }

        public string SubQuestionNumLocal { get; set; }

        public string SubQuestion { get; set; }

        public string SubQuestionLocal { get; set; }

        public int Status { get; set; }

        public string SubAnswer { get; set; }

        public string AnswerAttachLocation { get; set; }

        public string AnswerRemarks { get; set; }

        public DateTime DeptDateOfReply { get; set; }

        public string RejectRemarks { get; set; }

        public string ApproveRemarks { get; set; }
    }
}
