﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Question
{
    [Serializable]
    public class tQuestionTypeModel
    {
        public int? QuestionTypeId { get; set; }
        public int QuestionID { get; set; }
        public string QuestionTypeName { get; set; }
        public string QuestionTypeName_Hindi { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedWhen { get; set; }
        public int? QuestionNumber { get; set; }
    }
}
