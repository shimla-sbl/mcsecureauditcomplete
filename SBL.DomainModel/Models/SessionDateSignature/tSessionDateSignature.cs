﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.SessionDateSignature
{

    [Table("tSessionDateSignature")]
    [Serializable]
    public partial class tSessionDateSignature
    {
       
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SessionDateSignatureDetailsId { get; set; }

        public int AssemblyId { get; set; }

        public int SessionId { get; set; }

        public int? SessionDateId { get; set; }

        public string SignatureName { get; set; }

        public string SignatureNameLocal { get; set; }

        public string HeaderText { get; set; }

        public string HeaderTextLocal { get; set; }


        public string SignaturePlace { get; set; }

        public string SignaturePlaceLocal { get; set; }

        public string SignatureDate { get; set; }

        public string SignatureDateLocal { get; set; }

        public string SignatureDesignations { get; set; }

        public string SignatureDesignationsLocal { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public bool? IsActive { get; set; }

        [NotMapped]
        public string AssemblyName { get; set; }

        [NotMapped]
        public string SessionName { get; set; }

        [NotMapped]
        public string SessionDate { get; set; }

        public List<tSessionDateSignature> Signature { get; set; }

    }
}
