﻿using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.PISMolues;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.PISModules
{
    [Serializable]
    [Table("emppersonal_Log")]
    public class Emppersonal_Log
    {

        //[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public Int64 ID { get; set; }
        public Int64? transid { get; set; }
        [Required]
        public string AadharID { get; set; }
        [Required]
        public string empcd { get; set; }

        public string officeid { get; set; }
        public string empfname { get; set; }
        public string empmname { get; set; }
        public string emplname { get; set; }
        public string empnamelocal { get; set; }
        public DateTime? empdob { get; set; }
        public string empfmh { get; set; }
        public string empfmhname { get; set; }
        public string empmarst { get; set; }
        public string empspouse { get; set; }
        public string empidmasrk { get; set; }
        public string empcaste { get; set; }
        public string empreligion { get; set; }
        public string empcategory { get; set; }
        public string emphmst { get; set; }
        public string emphmdist { get; set; }
        public string emphmtown { get; set; }
        public string esalarycd { get; set; }
        public string emphomeoff { get; set; }
        public string empmedcer { get; set; }
        public string empcharcer { get; set; }
        public string empphoto { get; set; }
        public string empfinger { get; set; }
        public string emprail { get; set; }
        public string empremark { get; set; }
        public string empgender { get; set; }
        public string empblood { get; set; }
        public DateTime? dofvalid { get; set; }
        public string empheight { get; set; }
        public string currdesig { get; set; }
        public DateTime? nextincrement { get; set; }
        public DateTime? doeoffdesigincre { get; set; }
        public int? cadre_cd { get; set; }
        [Required]
        public string deptid { get; set; }
        public string DEuserid { get; set; }
        public DateTime? DEdate { get; set; }
        public string VALIDuserid { get; set; }
        public string verify { get; set; }
        public int? branchcd { get; set; }
        public string postdeptid { get; set; }
        public string desigdescription { get; set; }
        public string estabdeptid { get; set; }
        public string estabofficeid { get; set; }
        public string policepinno { get; set; }
        //[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? dofjoin { get; set; }
        // [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? dofretirecurr { get; set; }
        public string modeofreccurr { get; set; }
        public string emptypecurr { get; set; }
        public Int32? POfficeId { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public Int64? Mobile { get; set; }
        public Int64? PinCode { get; set; }
        public Int32? PDesignationId { get; set; }
        public bool? IsRelieved { get; set; }
        public string PISCode { get; set; }
        public string SalaryCode { get; set; }
        [NotMapped]
        public string DepartmentName { get; set; }
        [NotMapped]
        public string OfficeName { get; set; }
        [NotMapped]
        public string DesignationName { get; set; }
        [NotMapped]
        public List<string> AllDesignationIds { get; set; }
        [NotMapped]
        public string type { get; set; }
        [NotMapped]
        public List<mDepartment> DepartmentList { get; set; }
        [NotMapped]
        public List<mOffice> OfficeList { get; set; }
        [NotMapped]
        public List<mPISDesignation> DesignationList { get; set; }
        [NotMapped]
        public List<tPISEmployeePersonal> EmployeeList { get; set; }
        [NotMapped]
        public List<tPISEmployeePersonal> RelievedEmployeeList { get; set; }
        [NotMapped]
        public List<DistrictModel> DistrictList { get; set; }
        [NotMapped]
        public Int64 EmpKeyID { get; set; }

        [NotMapped]
        public int age { get; set; }
        [NotMapped]
        public string DateOFJoining { get; set; }
        public string DateOFBirth { get; set; }

    }
}