﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.PISMolues
{
    [Serializable]
    [Table("mdesigmast")]
    public class mPISDesignation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string desigcode { get; set; }
         [Required]
        public string designame { get; set; }
         [Required]
         public string deptid { get; set; }
         public string desiglocal { get; set; }
         public string desigabbr { get; set; }
        

        //added by umang

         public string ModifiedBy { get; set; }

         public DateTime? ModifiedDate { get; set; }

         public string CreatedBy { get; set; }

         public DateTime? CreatedDate { get; set; }

         public bool? Active { get; set; }

         public bool? IsDeleted { get; set; }

         [NotMapped]
         public string GetDepartmentName { get; set; }

    }
}
