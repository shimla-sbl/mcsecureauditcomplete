﻿using SBL.DomainModel.Models.Office;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.PISMolues
{
   
    [Serializable]
    [Table("pisempsanctionpost")]
    public class tPISEmpSanctionpost
    {
        public tPISEmpSanctionpost()
        {
            this.OfficeList = new List<mOffice>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Deptid { get; set; }
        [Required]
        public string officeid { get; set; }
        public Int32 POfficeId { get; set; }
        public Int32? PDesignationId { get; set; }
        [Required]
        public string desigcd { get; set; }
        public Int64? sanctpost { get; set; }
        public Int64? Filledpost { get; set; }
        public Int32? Priority { get; set; }

        [NotMapped]
        public string DepartmentName { get; set; }
        [NotMapped]
        public string OfficeName { get; set; }
        [NotMapped]
        public string DesignationName { get; set; }
        [NotMapped]
        public List<string> AllDesignationIds { get; set; }
        [NotMapped]
        public string type { get; set; }
          [NotMapped]
        public List<tPISEmpSanctionpost> PostList { get; set; }
          [NotMapped]
          public List<mOffice> OfficeList { get; set; }
          [NotMapped]
          public string SubId { get; set; }


          //For Sanctioned & Filled Posts
          [NotMapped]
          public List<SBL.DomainModel.Models.PISModules.SanFillPostPdf> SancFillPostList { get; set; }
          [NotMapped]
          public string FileLocation { get; set; }
          [NotMapped]
          public string Description { get; set; }
          [NotMapped]
          public string ModifiedDate { get; set; }
          [NotMapped]
          public string Mode { get; set; }
          [NotMapped]
          public string FileAccessPath { get; set; }
    }
}
