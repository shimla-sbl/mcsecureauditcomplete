﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Employee
{
    [Serializable]
    [Table("mMemberDesignations")]
    public partial class mMemberDesignation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int memDesigId { get; set; }

        public int memDesigcode { get; set; }

        [Required]
        public string memDesigname { get; set; }
      
        public string memDesiglocal { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool? Active { get; set; }

        public bool? IsDeleted { get; set; }

        [NotMapped]
        public IEnumerable<mMemberDesignation> MemberDesignationList { get; set; }

    }
}
