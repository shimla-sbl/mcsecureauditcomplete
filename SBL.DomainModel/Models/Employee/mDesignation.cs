﻿using SBL.DomainModel.Models.Role;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Employee
{
    [Serializable]
    [Table("mDesignations")]
    public partial class mDesignation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int desigcode { get; set; }
        
        [Required]
        public string designame { get; set; }

        public string deptid { get; set; }

        public string desiglocal { get; set; }
        
        [Required]
        [StringLength(10)]
        public string desigabbr { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool? Active { get; set; }

        public bool? IsDeleted { get; set; }

        [NotMapped]
        public string GetDepartmentName { get; set; }
    }
}
