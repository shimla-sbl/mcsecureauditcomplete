﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Employee
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

     [Serializable]
     [Table("tAuthorizedEmployees")]
    public partial class AuthorisedEmployee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmpAuthId { get; set; }
        public int SecretaryId { get; set; }
        public string EmployeeId { get; set; }
        public string AssociatedDepts { get; set; }
        [StringLength(12)]
        public string SecAadhaarID { get; set; }
        [StringLength(12)]
        public string EmpAadhaarID { get; set; }
        public bool IsPermissions { get; set; }
        public bool IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedWhen { get; set; }
        public Guid UserId { get; set; }
        public bool isAuthorized { get; set; }
        public string EmployeeSelectedDepts { get; set; }        
    }
}


