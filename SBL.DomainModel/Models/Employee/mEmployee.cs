﻿namespace SBL.DomainModel.Models.Employee
{
    using SBL.DomainModel.Models.Role;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("mEmployees")]
    public partial class mEmployee
    {
        public mEmployee()
        {
            
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id  { get; set; }

        [Required]
        [StringLength(50)]
        public string empcd { get; set; }

        [Required]
        [StringLength(50)]
        public string officeid { get; set; }

        [Required]
        [StringLength(50)]
        public string empfname { get; set; }

        [StringLength(50)]
        public string empmname { get; set; }

        [StringLength(50)]
        public string emplname { get; set; }

        [StringLength(100)]
        public string empnamelocal { get; set; }
      
        public DateTime? empdob { get; set; }

        [MaxLength(1)]
        [Column(TypeName = "char")]
        public string empfmh { get; set; }

        [StringLength(100)]
        public string empfmhname { get; set; }

        [MaxLength(1)]
        [Column(TypeName = "char")]
        public string empmarst { get; set; }

        [StringLength(50)]
        public string empspouse { get; set; }

        [StringLength(150)]
        public string empidmasrk { get; set; }

        [StringLength(25)]
        public string empcaste { get; set; }

        [MaxLength(1)]
        [Column(TypeName = "char")]
        public string empreligion { get; set; }

        [MaxLength(1)]
        [Column(TypeName = "char")]
        public string empcategory { get; set; }

        [MaxLength(2)]
        [Column(TypeName = "char")]
        public string emphmst { get; set; }

        [MaxLength(2)]
        [Column(TypeName = "char")]
        public string emphmdist { get; set; }

        [MaxLength(150)]
        [Column(TypeName = "char")]
        public string emphmtown { get; set; }

        [StringLength(10)]
        public string esalarycd { get; set; }

        [StringLength(12)]
        public string emphomeoff { get; set; }

        [MaxLength(1)]
        [Column(TypeName = "char")]
        public string empmedcer { get; set; }

        [MaxLength(1)]
        [Column(TypeName = "char")]
        public string empcharcer { get; set; }
      

        [Column(TypeName = "image")]
        public byte[] empphoto { get; set; }

        [Column(TypeName = "image")] 
        public byte[] empfinger { get; set; }

        [MaxLength(30)]
        [Column(TypeName = "char")]
        public string emprail { get; set; }

        [StringLength(2500)]
        public string empremark { get; set; }

        [MaxLength(1)]
        [Column(TypeName = "char")]
        public string empgender { get; set; }

        [MaxLength(1)]
        [Column(TypeName = "char")]
        public string empblood { get; set; }
      

        public DateTime? dofvalid { get; set; }

        [MaxLength(4)]
        [Column(TypeName = "char")]
        public string  empheight { get; set; }

        [MaxLength(3)]
        [Column(TypeName = "char")]
        public string currdesig { get; set; }
      
        public DateTime? nextincrement { get; set; }
      
        public DateTime? doeoffdesigincre { get; set; }
      
        public int? cadre_cd { get; set; }

        [Required]
        [MaxLength(7)]
        [Column(TypeName = "char")]
        public string deptid { get; set; }

        [MaxLength(12)]
        [Column(TypeName = "char")]
        public string DEuserid { get; set; }
      
        public DateTime? DEdate { get; set; }

        [MaxLength(12)]
        [Column(TypeName = "char")]
        public string VALIDuserid { get; set; }

        [StringLength(1)]
        public string verify { get; set; }
      

        public int? branchcd { get; set; }

        [MaxLength(7)]
        [Column(TypeName = "char")]
        public string postdeptid { get; set; }

        [StringLength(250)]
        public string desigdescription { get; set; }

        [MaxLength(7)]
        [Column(TypeName = "char")]
        public string estabdeptid { get; set; }

        [MaxLength(12)]
        [Column(TypeName = "char")]
        public string estabofficeid { get; set; }

        [MaxLength(8)]
        [Column(TypeName = "char")]
        public string policepinno { get; set; }
      
        public DateTime? dofjoin { get; set; }
      
        public DateTime? dofretirecurr { get; set; }

        [MaxLength(1)]
        [Column(TypeName = "char")]
        public string modeofreccurr { get; set; }

        [MaxLength(1)]
        [Column(TypeName = "char")]
        public string emptypecurr { get; set; }



        public bool? IsDeleted { get; set; }
             
        public bool? IsActive { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreationDate { get; set; }


        [NotMapped]
        public string GetDepartmentName { get; set; }



        //For Paging



        [NotMapped]
        public int ClickCount { get; set; }


        [NotMapped]
        public string SearchText { get; set; }

        [NotMapped]
        public List<mEmployee> EmployeeList { get; set; }

        [NotMapped]
        public int PAGE_SIZE { get; set; }

        [NotMapped]
        public int PageIndex { get; set; }

        [NotMapped]
        public int ResultCount { get; set; }

        [NotMapped]
        public int loopStart { get; set; }

        [NotMapped]
        public int loopEnd { get; set; }

        [NotMapped]
        public int selectedPage { get; set; }

        [NotMapped]
        public int PageNumber { get; set; }

        [NotMapped]
        public int RowsPerPage { get; set; }
        [NotMapped]
        public bool HasPreviousPage { get; set; }

        [NotMapped]
        public bool HasNextPage { get; set; }


    }
}

