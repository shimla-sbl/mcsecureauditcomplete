﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.GalleryThumb
{
    [Serializable]
    [Table("tGalleryThumb")]
    public class tGalleryThumb
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GalleryThumbId { get; set; }

        public int GalleryId { get; set; }

        public int CategoryId { get; set; }

    }
}
