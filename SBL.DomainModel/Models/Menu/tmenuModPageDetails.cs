﻿namespace SBL.DomainModel.Models.Menu
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tmenuModPageDetails")]
    [Serializable]
    public partial class tmenuModPageDetails
    {
        [Key]
        public Guid MMenuId { get; set; }

        public Guid? MenuId { get; set; }

        public Guid? ModuleidPageid { get; set; }

        [StringLength(50)]
        public string Type { get; set; }
    }
}
