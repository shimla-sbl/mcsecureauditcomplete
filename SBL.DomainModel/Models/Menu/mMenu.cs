﻿namespace SBL.DomainModel.Models.Menu
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("mMenu")]
    [Serializable]
    public partial class mMenu
    {
        [Key]
        public Guid MenuId { get; set; }

        [StringLength(50)]
        public string MenuName { get; set; }

        public Guid? ParentId { get; set; }

        public bool? Isactive { get; set; }

        [StringLength(500)]
        public string Action { get; set; }

        [StringLength(500)]
        public string ActionType { get; set; }

        [StringLength(500)]
        public string ActionValue { get; set; }

        [StringLength(500)]
        public string Controller { get; set; }

        [StringLength(500)]
        public string Module { get; set; }

        public int? MenuOrder { get; set; }
    }
}
