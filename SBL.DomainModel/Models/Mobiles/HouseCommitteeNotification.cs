﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Mobiles
{
    [Table("HouseCommitteeNotifications")]
    [Serializable]
    public class HouseCommitteeNotification
    {

            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public Int64 SlNo { get; set; }

            public string Title { get; set; }

            public string FilePath { get; set; }

            public DateTime? HCNDate { get; set; }

            public string CreatedBy { get; set; }

            public DateTime? CreatedWhen { get; set; }

            public string ModifiedBy { get; set; }

            public DateTime? ModifiedWhen { get; set; }

    }
}
