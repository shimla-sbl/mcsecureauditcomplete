﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Mobiles
{
    [Table("mErrorLog")]
    [Serializable]
    public class mErrorLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 ErrorLogId { get; set; }

        public string ErrorNumber { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorProcedure { get; set; }

        public string ErrorState { get; set; }

        public string ErrorSeverity { get; set; }

        public string ErrorLine { get; set; }

        public DateTime? CreationDate { get; set; }


    }
}
