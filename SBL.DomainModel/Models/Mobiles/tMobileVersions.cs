﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Mobiles
{
    [Table("tMobileVersions")]
    [Serializable]
   public partial class tMobileVersions
    {
       [Key]
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       public Int64 SlNo { get; set; }

       public string Code { get; set; }

       public int Major { get; set; }

       public int Minor { get; set; }

       public Int64 Build { get; set; }

       public string FilePath { get; set; }

       public DateTime? VersionDate { get; set; }

     
       public string CreatedBy { get; set; }

       public DateTime? CreatedWhen { get; set; }

       public string ModifiedBy { get; set; }

       public DateTime? ModifiedWhen { get; set; }

    }
}
