﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Session;

namespace SBL.DomainModel.Models.PrintingPress
{
    [Serializable]
    public class PrintingPressModel
    {
        public PrintingPressModel()
        {
            this.mAssemblyList = new List<mAssembly>();
            this.sessionList = new List<mSession>();
            this.mDepartment = new List<mDepartment>();
            this.mPaperType = new List<SBL.DomainModel.Models.PaperLaid.mPaperCategoryType>();
            this.tStatusPrintingPress = new List<tStatusPrintingPress>();
        }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public string DepartmentId { get; set; }
        public int PaperCategoryTypeId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mAssembly> mAssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AssesmblyName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mDepartment> mDepartment { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<SBL.DomainModel.Models.PaperLaid.mPaperCategoryType> mPaperType { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int PrintSeqenceId { get; set; }
        [Required()]
        [DisplayName("Subject")]
        [AllowHtml]
        public string Title { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int AssemblyId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int SessionId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int EventId { get; set; }
        [HiddenInput(DisplayValue = false)]
        [Required]
        public int MinistryId { get; set; }
        [StringLength(200)]
        [HiddenInput(DisplayValue = false)]
        public string MinistryName { get; set; }
        [StringLength(200)]
        [HiddenInput(DisplayValue = false)]
        public string DeparmentName { get; set; }
        [Required()]
        [HiddenInput(DisplayValue = false)]
        [AllowHtml]
        public string Description { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? PaperTypeID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? CommitteeId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? CommitteeChairmanMemberId { get; set; }
        public bool? Reciept { get; set; }
        public bool? Dispatch { get; set; }
        [NotMapped]
        public string PaperTypeName { get; set; }
        [StringLength(200)]
        public string FileName { get; set; }
        [StringLength(500)]
        public string FilePath { get; set; }
        [StringLength(200)]
        public string DocFileName { get; set; }
        [StringLength(500)]
        public string DocFilePath { get; set; }
        public int TotalReciptCount { get; set; }
        public int TotalDispatchCount { get; set; }
        public string FileStructurePath { get; set; }
        public DateTime? RecievedDate { get; set; }
        public DateTime? Acceptance { get; set; }
        public DateTime? Acknowledge { get; set; }
        public string AdhaarID { get; set; }
        public List<PrintingPressModel> objList { get; set; }
        public string AdhaarName { get; set; }
        public string ADOB { get; set; }
        public string AMobileNo { get; set; }
        public string AEmail { get; set; }
        public string AAddress { get; set; }
        public string ADistrict { get; set; }
        public string APinCode { get; set; }
        public DateTime? SubmitDate { get; set; }
        public bool? Resend { get; set; }
        public string RefNo { get; set; }
         [AllowHtml]
        public string Remarks { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<tStatusPrintingPress> tStatusPrintingPress { get; set; }
        public string StatusName { get; set; }
        public int StatusId { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public string FatherName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string DOB { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string District { get; set; }
        public string PinCode { get; set; }
        public string Photo { get; set; }
       
    }
}
