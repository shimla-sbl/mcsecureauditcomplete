﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.PrintingPress
{
    [Serializable]
    [Table("tStatusPrintingPress")]
   public partial class tStatusPrintingPress
    {
        public tStatusPrintingPress()
        {

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
