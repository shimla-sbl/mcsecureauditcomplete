﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.PrintingPress
{
    [Serializable]
    [Table("tPrintingTemp")]
    public partial class tPrintingTemp
    {
        public tPrintingTemp()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PrintingTempId { get; set; }
        public int PrintSeqenceId { get; set; }
        public int? Version { get; set; }
        [StringLength(200)]
        public string FileName { get; set; }
        [StringLength(500)]
        public string FilePath { get; set; }
        public int? DocFileVersion { get; set; }
        [StringLength(200)]
        public string DocFileName { get; set; }
        [StringLength(500)]
        public string DocFilePath { get; set; }
        public DateTime? SubmittedDate { get; set; }
        [StringLength(50)]
        public string SubmittedBy { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string Name { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int Length { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string Type { get; set; }
        [NotMapped]
        public virtual ICollection<PrintingPressModel> PrintingPressModel { get; set; }
        [NotMapped]
        public string FileStructurePath { get; set; }
        [NotMapped]
        [AllowHtml]
        public string Title { get; set; }
        [NotMapped]
        public string DepartmentId { get; set; }
        [NotMapped]
        public int PaperCategoryTypeId { get; set; }
        [NotMapped]
        public int AssemblyId { get; set; }
        [NotMapped]
        public int SessionId { get; set; }
        [NotMapped]
        public string DeparmentName { get; set; }
        [NotMapped]
        public string Remark { get; set; }
        [NotMapped]
        public string RefNo { get; set; }
        [NotMapped]
        public int StatusID { get; set; }
        [NotMapped]
        public string MB { get; set; }
    }
}
