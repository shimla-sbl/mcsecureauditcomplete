﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel;

namespace SBL.DomainModel.Models.PrintingPress
{
    [Serializable]
    [Table("tPrintingPress")]
    public partial class tPrintingPress
    {
        public tPrintingPress()
        {

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [HiddenInput(DisplayValue = false)]
        public int PrintSeqenceId { get; set; }
        //[Required()]
        [DisplayName("Subject")]
        [AllowHtml]
        public string Title { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int AssemblyId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int SessionId { get; set; }
        //[HiddenInput(DisplayValue = false)]
        //public int EventId { get; set; }
        //[HiddenInput(DisplayValue = false)]
        //public int MinistryId { get; set; }
        //[StringLength(200)]
        //[HiddenInput(DisplayValue = false)]
        //public string MinistryName { get; set; }
        [StringLength(20)]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentId { get; set; }
        [StringLength(200)]
        [HiddenInput(DisplayValue = false)]
        public string DeparmentName { get; set; }
        //[HiddenInput(DisplayValue = false)]
        //[AllowHtml]
        //public string Description { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? PaperTypeID { get; set; }
        //[HiddenInput(DisplayValue = false)]
        //public int? CommitteeId { get; set; }
        //[HiddenInput(DisplayValue = false)]
        //public int? CommitteeChairmanMemberId { get; set; }
        public bool? CreatedprintPress { get; set; }
        public bool? Dispatch { get; set; }
        [NotMapped]
        public virtual ICollection<PrintingPressModel> PrintingPressModel { get; set; }
        [NotMapped]
        public string PaperTypeName { get; set; }
        public string AdhaarID { get; set; }
        public string RefNo { get; set; }
        [AllowHtml]
        public string Remarks { get; set; }
        public DateTime? RecievedDate { get; set; }
        public DateTime? Acceptance { get; set; }
        public DateTime? Acknowledge { get; set; }
        public DateTime? SubmitDate { get; set; }
         public bool? Resend { get; set; }
         public int Status { get; set; }



    }
}





