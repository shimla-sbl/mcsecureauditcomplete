﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.States
{
    [Table("mStates")]
    [Serializable]
   public class mStates
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int mStateID { get; set; }
        
        [Required]
        public string   StateCode { get; set; }

        public string StateCodeLink { get; set; }

        public string StateAbbreviation { get; set; }

        public string StateName { get; set; }

        public string StateNameLocal { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }

    }
}
