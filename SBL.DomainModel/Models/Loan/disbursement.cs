﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Loan
{
    [Serializable]
    [Table("disbursementDetails")]
    public class disbursementDetails
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int disbursmentID { get; set; }

        public long loanNumber { get; set; }

        [DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}", ConvertEmptyStringToNull = false)]

        public DateTime disburmentDate { get; set; }

        public double disburmentAmount { get; set; }

        public double disburmentStatus { get; set; }

        public DateTime? disburmentRecDate { get; set; }

    }
}