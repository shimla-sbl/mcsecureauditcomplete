﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Loan
{
    [Serializable]
    [Table("loaneeDetails")]
   public class loaneeDetails
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int autoID { get; set; }
        
        public long loaneeID { get; set; }

        public int memberCode { get; set; }

        public string memberName { get; set; }

        public string designationID { get; set; }

        public string constituencyName { get; set; }

        public int constituencyCode { get; set; }

        public string address { get; set; }

        public string mobile { get; set; }

        public string bankName { get; set; }

        public string ifscCode { get; set; }

        public string bankAccountNumber { get; set; }
    }
}
