﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Loan
{
    [Serializable]
    [Table("loanDetails")]
    public class loanDetails
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AutoID { get; set; }
        public long loanNumber { get; set; }
        public int loanTypeID { get; set; }
        public string loanTypeName { get; set; }
        public string loanAccountNumber { get; set; }
        public long loaneeID { get; set; }
        public string loanSanctionDate { get; set; }
        public DateTime? releaseDate { get; set; }
        public double loanAmount { get; set; }
        public double loanInterestRate { get; set; }
        public int totalNoofEMI { get; set; }
        public DateTime submitDate { get; set; }
        public int submitBy { get; set; }
        public DateTime? lastEditDate { get; set; }
        public int? lastEditBy { get; set; }
        public bool loanStatus { get; set; }
        [NotMapped]
        public List<disbursementDetails> disbursementDetails { get; set; }
    }

}
