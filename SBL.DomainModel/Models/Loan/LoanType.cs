﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.Loan
{
    [Serializable]
    [Table("loanTypes")]
    [MetadataType(typeof(loanTypeMetaData))]
    public class LoanType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int loanID { get; set; }

        [Required]
        public string loanName { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 4, ErrorMessage = "Please Enter 4 Digit Major Code.")]
        public string majorCode { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Please Enter Major Code Text")]
        public string majorCodeText { get; set; }

        [Required]
        [StringLength(2, MinimumLength = 2, ErrorMessage = "Please Enter 2 Digit Sub-major Code.")]
        public string subMajorCode { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Please Enter  Sub-major Code Text")]
        public string subMajorCodeText { get; set; }

        [Required]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "Please Enter 3 Digit Minor Code.")]
        public string minorCode { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Please Enter  Minor Code Text")]
        public string minorCodeText { get; set; }

        [Required]
        [StringLength(2, MinimumLength = 1, ErrorMessage = "Please Enter 2 Digit Sub-major Code.")]
        public string subCode { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Please Enter Sub-major Code Text")]
        public string subCodeText { get; set; }


        public string accountHeadsName { get; set; }


        [NotMapped]
        public string Mode { get; set; }
    }

    public class loanTypeMetaData
    {
        [Remote("checkLoanTypeByName", "loan", ErrorMessage = "Loan Type Already Exists")]
        public string loanName { get; set; }
    }
}