﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Loan
{
    [Serializable]
    [Table("loanTransDetails")]
    public class loanTransDetails
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int autoID { get; set; }

        public long loanNumber { get; set; }

        [StringLength(2)]
        public string transType { get; set; }

        public string loanAccontNumber { get; set; }

        public int installmentNumber { get; set; }

        public DateTime payDate { get; set; }

        public int monthID { get; set; }

        public int yearID { get; set; }

        public double openingBalance { get; set; }

        public double closingBalance { get; set; }

        public double principalReceived { get; set; }
        public double intRate { get; set; }
        public double interestForMonth { get; set; }

        public double interestReceived { get; set; }

        public double outstandingInterest { get; set; }

        public string paymentMode { get; set; }

        public string paymentCode { get; set; }

        public string paymentDate { get; set; }

        public string treasuryName { get; set; }

        public string DDOCode { get; set; }

        public string voucherNumber { get; set; }

        public DateTime? voucherDate { get; set; }

        public double? disbursementAmount { get; set; }





    }
}
