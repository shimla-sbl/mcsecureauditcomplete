﻿namespace SBL.DomainModel.Models.Committee
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tCommitteeTransaction")]
    [Serializable]
    public partial class tCommitteeTransaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int CommitteeId { get; set; }

        public int FileId { get; set; }

        public string DeptId { get; set; }

        [Required]
        [StringLength(200)]
        public string Remark { get; set; }
    }
}
