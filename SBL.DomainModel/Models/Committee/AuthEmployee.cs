﻿using SBL.DomainModel.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.Committee
{
    [Serializable]
    [Table("AuthEmployee")]
    public partial class AuthEmployee
    {
        public AuthEmployee()
		{
      
		}
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Name { get; set; }
        public string AadharId { get; set; }
        public Boolean IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Remark { get; set; }
        [NotMapped]
        public List<AuthEmployee> AuthEmployeeList { get; set; }
        [NotMapped]
        public string Mode { get; set; }
        [NotMapped]
        public List<mUsers> obUserList { get; set; }
        //[NotMapped]
        //public List UserList { get; set; }

    }
}
