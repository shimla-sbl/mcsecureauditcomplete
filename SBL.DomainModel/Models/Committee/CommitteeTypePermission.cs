﻿using SBL.DomainModel.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Committee
{
	[Serializable]
	[Table("tUserCommitteeTypePerm")]
	public class CommitteeTypePermission
	{
		public CommitteeTypePermission()
		{
			IsActive = true;
		}

		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CommitteeTypePermId { get; set; }

		public Guid UserId { get; set; }

		public int CommitteeTypeId { get; set; }

		[NotMapped]
		public string Username { get; set; }

		[NotMapped]
		public string CommitteeTypeName { get; set; }

		[DefaultValue(true)]
		public bool IsActive { get; set; }

		[NotMapped]
		public List<mUsers> mUsers { get; set; }

		[NotMapped]
		public List<mCommitteeType> mCommitteeType { get; set; }
		[NotMapped]
		public List<CommitteesListView> CommitteeView { get; set; }
	}
}
