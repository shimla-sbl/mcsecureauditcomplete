﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Committee
{
    [Serializable]
    [Table("mCommitteeType")]
    public class mCommitteeType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommitteeTypeId { get; set; }
        public string CommitteeTypeName { get; set; }
        public bool IsSubCommittee { get; set; }
        public string Description { get; set; }
        [StringLength(10)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

        public bool? IsDeleted { get; set; }

        public string CommitteeNameLocal { get; set; }

    }
}
