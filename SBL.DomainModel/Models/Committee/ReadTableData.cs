﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Committee
{
     [Serializable]
     [Table("mReadTableData")]
        public class ReadTableData
        {
            [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public Int32 ReadId { get; set; }
            public string TableName { get; set; }
            public int TablePrimaryId { get; set; }
            public string UserAadharId { get; set; }
            public string UserId { get; set; }
            public DateTime? ReadDateTime { get; set; }
            public string CommitteeId { get; set; }
           
        }
  
}
