﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Committee
{
    [Serializable]
    [Table("tBranchesCommittee")]
     public partial class  tBranchesCommittee
    {
        public tBranchesCommittee()
		{			
		}

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int BranchId { get; set; }
        public int CommitteeId { get; set; }

    }
}
