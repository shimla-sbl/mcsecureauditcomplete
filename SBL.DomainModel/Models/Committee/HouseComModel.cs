﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.StaffManagement;
namespace SBL.DomainModel.Models.Committee
{
    [Serializable]
    public class HouseComModel
    {
        public HouseComModel()
        {
            this.List = new List<HouseComModel>();
         }
        public int ID { get; set; }
        public string Remark { get; set; }
        public int efileID { get; set; }
        public string EmpName { get; set; }
        public DateTime? AssignDateTime { get; set; }
        public string BranchName { get; set; }
        public string Remarks { get; set; }
        public virtual ICollection<HouseComModel> List { get; set; }
        public string AssignfrmAadharId { get; set; }
        public string AssignfrmName { get; set; }
        public string AssignfrmDesignation { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public int MId { get; set; }
        public string MemberName { get; set; }
        public int ConstituencyCode { get; set; }
        public string ConstituencyName { get; set; }
        public string OName { get; set; }
        public string OEmail { get; set; }
        public string ONumber { get; set; }
        public int OId { get; set; }
        public string eFileName { get; set; }
        public string Subject { get; set; }
        public string SignPath { get; set; }
        public string MarkedAadharId { get; set; }
        public string MarkedName { get; set; }
        public string MarkedDesignation { get; set; }
    }
    
      


}
