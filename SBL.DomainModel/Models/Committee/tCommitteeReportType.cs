﻿namespace SBL.DomainModel.Models.Committee
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tCommitteeReportType")]
    [Serializable]
    public partial class tCommitteeReportType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReportTypeId { get; set; }

        [Required]
        [StringLength(128)]
        public string ReportTypeName { get; set; }

        public int?  CommitteeTypeId { get; set; }

        public string Duration { get; set; }

        public string Description { get; set; }
    }
}
