﻿namespace SBL.DomainModel.Models.Committee
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tCommitteeCalendar")]
    [Serializable]
    public partial class tCommitteeCalendar
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKey { get; set; }

        public int CommitteeId { get; set; }

        public DateTime? ScheduledDay { get; set; }

        public DateTime? ScheduledTime { get; set; }

        public decimal ScheduledDuration { get; set; }

        public virtual tCommittee tCommittee { get; set; }
    }
}
