﻿namespace SBL.DomainModel.Models.Role
{
    using SBL.DomainModel.Models.User;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("mRoles")]
    [Serializable]
    public partial class mRoles
    {
        public mRoles()
        {
            tRoleDetails = new HashSet<tRoleDetails>();
            tUserRoles = new HashSet<tUserRoles>();
        }

        [Key]
        public Guid RoleId { get; set; }

        [StringLength(250)]
        public string RoleName { get; set; }

        [StringLength(250)]
        public string RoleNameLocal { get; set; }

        [StringLength(250)]
        public string RoleDescription { get; set; }

        public bool Isactive { get; set; }

        public Guid? ParentId { get; set; }

        public bool? Requiredlogin { get; set; }


        public string ModifiedBy { get; set; }


        public DateTime? ModifiedDate { get; set; }

        [StringLength(250)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public virtual ICollection<tRoleDetails> tRoleDetails { get; set; }

        public virtual ICollection<tUserRoles> tUserRoles { get; set; }

        [NotMapped]
        public virtual List<mRoles> ListmRoles { get; set; }

        // public virtual ICollection<mRoles> objList { get; set; }
        public List<mRoles> objList { get; set; }

        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public int AssemblyCode { get; set; }

        [NotMapped]
        public int SessionCode { get; set; }

        [NotMapped]
        public int AssemblyId { get; set; }

        [NotMapped]
        public int SessionId { get; set; }

        [NotMapped]

        public string SessionName { get; set; }
        [NotMapped]

        public string AssesmblyName { get; set; }
        [NotMapped]
         public int SubUserTypeID { get; set; }
        //ashwani
        [NotMapped]
        public virtual ICollection<mUsers> Users { get; set; }
        [NotMapped]
        public string iRoleId
        {
            get
            {
                return RoleId.ToString();
            }
            set { }
        }
      
    }
}
