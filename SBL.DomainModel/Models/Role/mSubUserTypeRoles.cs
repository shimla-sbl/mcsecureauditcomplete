﻿
namespace SBL.DomainModel.Models.Role
{
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.UserModule;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("mSubUserTypeRoles")]
    [Serializable]
    public class mSubUserTypeRoles
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public Guid RoleId { get; set; }

        public int SubUserTypeID { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        
        [NotMapped]
        public int UserTypeID { get; set; }

        public List<mUserModules> UserTypeList { get; set; }
        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public List<mUserModules> SubUserTypeList { get; set; }
        [NotMapped]
        public List<mRoles> RoleList { get; set; }

        [NotMapped]
        public string RoleName { get; set; }
        [NotMapped]
        public string SubUserTypeName { get; set; }
        [NotMapped]
        public int SubUserTypeNameSelected { get; set; }
        [NotMapped]
        public int UserTypeNameSelected { get; set; }
        [NotMapped]
        public Guid RoleNameSelected { get; set; }
        [NotMapped]
        public List<string> AllRoleList { get; set; }


    }
}
