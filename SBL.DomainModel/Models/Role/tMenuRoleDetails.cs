﻿namespace SBL.DomainModel.Models.Role
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tMenuRoleDetails")]
    [Serializable]
    public partial class tMenuRoleDetails
    {
        [Key]
        public Guid MenudtId { get; set; }

        public Guid? MenuId { get; set; }

        public Guid? RoledUser { get; set; }

        [StringLength(50)]
        public string Type { get; set; }
    }
}
