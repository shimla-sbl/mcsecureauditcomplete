﻿namespace SBL.DomainModel.Models.Role
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tRoleDetails")]
    [Serializable]
    public partial class tRoleDetails
    {
        [Key]
        public Guid RoleDetailsId { get; set; }

        public Guid? RoleIDUserid { get; set; }

        public Guid ModuleID { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(50)]
        public string Type { get; set; }

        public virtual mRoles mRoles { get; set; }
    }
}
