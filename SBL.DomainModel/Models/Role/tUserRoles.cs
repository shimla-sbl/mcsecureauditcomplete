﻿namespace SBL.DomainModel.Models.Role
{
    using SBL.DomainModel.Models.User;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tUserRoles")]
    [Serializable]
    public partial class tUserRoles
    {
        [Key]
        public Guid RoleDetailsID { get; set; }

        public Guid? Roleid { get; set; }

        public Guid? UserID { get; set; }

        public bool? IsActive { get; set; }

        public virtual mRoles mRoles { get; set; }

        public virtual mUsers mUsers { get; set; }
    }
}
