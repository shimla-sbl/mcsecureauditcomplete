﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mOtherFirm")]
    [Serializable]
    public class mOtherFirm
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FirmID { get; set; }
        [Required]
        public string AccNo { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }

        public string EmailId { get; set; }
        public string BankName { get; set; }
        public string IFSCCode { get; set; }
        public string MobileNo { get; set; }
        public bool IsReject { get; set; }
        [NotMapped]
        public string Mode { get; set; }

    }
}
