﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mSalaryTypeDetailForTax")]
    [Serializable]
    public class mSalaryTypeDetailForTax
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TypeID { get; set; }

        [Required]
        public long IncomeTaxId { get; set; }
        public string SalaryType { get; set; }
        [Required]
        public decimal Amount { get; set; }

        [Required]
        public int MonthAndDayNo { get; set; }
        [Required]
        public decimal TotalAmount { get; set; }
    }
}
