﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mSubVoucher")]
    [Serializable]
  public  class mSubVoucher
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SubVoucherID { get; set; }

       // public int SubVoucherNo { get; set; }

        public decimal SubVoucherAmt { get; set; }
     
        public string ClaimantName { get; set; }
        public int MemberCode { get; set; }
        public string ClaimantBillNo { get; set; }

        public DateTime ClaimantBillDate { get; set; }
        public DateTime ClaimantPeriodFrom { get; set; }
        public DateTime ClaimantPeriodTo { get; set; }

        public int SanctionNo { get; set; }
        public DateTime SanctionDate { get; set; }

        public string GpfNo { get; set; }
      //  public string Image { get; set; }
        public int ReimbursementBillId { get; set; }
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

    }
}
