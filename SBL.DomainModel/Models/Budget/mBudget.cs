﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
     [Serializable]
    [Table("mBudget")]
    public partial class mBudget
    {

         public mBudget()
         {

         } 
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BudgetID { get; set; }


        public string Object { get; set; }

        [Required]

        public string DDOCode { get; set; }

        [Required]
        public string TreasuryCode { get; set; }

        [Required]
        public string DemandNo { get; set; }
        [Required]
        public string Gazetted { get; set; }

        public string MajorHead { get; set; }

        public string SubMajorHead { get; set; }

        public string MinorHead { get; set; }

        public string SubHead { get; set; }

        public string BudgetCode { get; set; }
        public string ObjectCode { get; set; }
        public string ObjectCodeText { get; set; }

        public string Plan { get; set; }
        public int BudgetType { get; set; }
        public int? OrderBy { get; set; }
        public string VotedCharged { get; set; }
      
        [DefaultValue("true")]
        public bool DisplayInReport { get; set; }

        public bool IsShowExpenseReport { get; set; }
        [Required]
        public DateTime FYFromDate { get; set; }

        [Required]
        public DateTime FYToDate { get; set; }
        [Required]
        public string FinancialYear { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
        [Required]
        public string BudgetName { get; set; }

        [NotMapped]
        public int AllocateCount { get; set; }

        [NotMapped]
        public decimal AllocateFund { get; set; }

        [NotMapped]
        public string   BudgetTypeName { get; set; }

        public int BudgetHeadOrder { get; set; }
        [NotMapped]
        public string BCode { get; set; }

    }
}
