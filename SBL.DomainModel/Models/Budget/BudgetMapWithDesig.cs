﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("BudgetMapWithDesig")]
    [Serializable]
  public  class BudgetMapWithDesig
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MappedID { get; set; }
        [Required]
        public int BudgetId { get; set; }
        [Required]
        public int DesignationId { get; set; }
        [Required]
        public string FinancialYear { get; set; }
        [NotMapped]
        public string BudgetName { get; set; }
    }
}
