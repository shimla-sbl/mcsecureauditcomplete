﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mReimbursementBill")]
    [Serializable]
    public class mReimbursementBill
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReimbursementBillID { get; set; }

        public int EstablishBillId { get; set; }

        [Required]
        public int BillType { get; set; }

        public string FinancialYear { get; set; }

        public string PDFUrl { get; set; }
        public string Comette { get; set; }
        public string StationName { get; set; }
        public string AlongWith { get; set; }
        public string JourneyBy { get; set; }
        public int BudgetId { get; set; }

        public int Status { get; set; }

        [Required]
        public int Designation { get; set; }

        public string DesignationS { get; set; }

        [Required]
        public int ClaimantId { get; set; }
        public int NomineeId { get; set; }

        [Required]
        public decimal GrossAmount { get; set; }

        public decimal SactionAmount { get; set; }

        public decimal Deduction { get; set; }

        //public decimal TakenAmount { get; set; }
        //public int AdvanceBillNo { get; set; }
        //public DateTime? TakenAmountDate { get; set; }

        public DateTime? SactionDate { get; set; }

        public string SactionNumber { get; set; }

        [Required]
        public DateTime? DateOfPresentation { get; set; }

        public DateTime? BillFromDate { get; set; }
        public DateTime? BillToDate { get; set; }

        public DateTime? EncashmentDate { get; set; }

        [DefaultValue("false")]
        public bool IsChallanGenerate { get; set; }

        [DefaultValue("false")]
        public bool IsPDFAttached { get; set; }


        public bool IsSOApproved { get; set; }

        public bool IsSanctioned { get; set; }

        [DefaultValue("false")]
        public bool IsRejected { get; set; }

        public bool IsSetEncashment { get; set; }

        public bool IsValidAuthority { get; set; }


        public string Number { get; set; }

        public string Remarks { get; set; }

        public int? sMOnthID { get; set; }
        public int? sYearID { get; set; }


        public string ClaimantName { get; set; }
        public string HospitalName { get; set; }
        public string MedicalShopName { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        [NotMapped]
        public string BudgetName { get; set; }
        [NotMapped]
        public string Prefix { get; set; }
        [NotMapped]
        public string Mobile { get; set; }
        [NotMapped]
        public string Email { get; set; }

        [NotMapped]
        public string BillTypeName { get; set; }

        [NotMapped]
        public string DesignationName { get; set; }

        public DateTime? BillDateOfVoucher { get; set; }
        public string BillNumberOfVoucher { get; set; }
        public int EstblishBillNumber { get; set; }
        public bool IsCanceled { get; set; }

        public decimal RetirementGratuity { get; set; }
        public decimal LeaveEncashment { get; set; }
        public decimal LTC { get; set; }
        public decimal MiscAG { get; set; }
        public decimal MiscBT { get; set; }
        [NotMapped]
        public string PPONum { get; set; }
    }
}
