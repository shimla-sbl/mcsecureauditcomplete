﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mApprovedHospital")]
    [Serializable]
    public class mApprovedHospital
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HospitalID { get; set; }
        [Required]
        public string HospitalName { get; set; }

        public bool IsActive { get; set; }
        [NotMapped]
        public string Mode { get; set; }
    }
}
