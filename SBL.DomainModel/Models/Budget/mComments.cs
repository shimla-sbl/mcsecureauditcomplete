﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mComments")]
    [Serializable]
    public class mComments
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentsID { get; set; }
        public string Comments { get; set; }
        public string ParentName { get; set; }
        public string UserName { get; set; }
        public int ParentId { get; set; }
    }
}
