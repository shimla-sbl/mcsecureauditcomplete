﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mEstablishBill")]
    [Serializable]
    public class mEstablishBill
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EstablishBillID { get; set; }

        [Required]
        public int BudgetId { get; set; }

        // [Required]
        public DateTime? SanctionDate { get; set; }

        [Required]
        public int BillType { get; set; }

        [Required]
        public string SanctioNumber { get; set; }

        public string FinancialYear { get; set; }

        public int BillNumber { get; set; }
        public int Designation { get; set; }

        public int Status { get; set; }

        public int? AssemblyId { get; set; }

        public bool IsSOApproved { get; set; }

        public bool IsSanctioned { get; set; }

        public bool IsBillUpdate { get; set; }
        public bool IsValidAuthority { get; set; }
        public bool IsReject { get; set; }
        public string RejectedBy { get; set; }

        public bool IsChallanGenerate { get; set; }

        public bool IsEncashment { get; set; }

        public string PDFUrl { get; set; }

        public decimal TotalAmount { get; set; }
        public decimal TotalGrossAmount { get; set; }
        public decimal Deduction { get; set; }

        public DateTime? BillProcessingDate { get; set; }

        public DateTime? EncashmentDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
        [NotMapped]
        public string BudgetName { get; set; }

        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public string BillTypeName { get; set; }
        public bool IsCanceled { get; set; }
        public int? sMOnthID { get; set; }
        public int? sYearID { get; set; }
        public int? ispart { get; set; }
        // For Advance Bill

        public decimal TakenAmount { get; set; }
        public int AdvanceBillNo { get; set; }
        public DateTime? TakenAmountDate { get; set; }
        [NotMapped]
        public string TakenAmountDateS { get; set; }
        [NotMapped]
        public bool IsDebitReceiptExpense { get; set; }
        [NotMapped]
        public string DebitReceiptRemarks { get; set; }
    }
}
