﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mSpeakerIncomeTax")]
    [Serializable]
    public class mSpeakerIncomeTax
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long IncomeTaxID { get; set; }

        [Required]
        public int EmpId { get; set; }
        public int Quarter { get; set; }
        [Required]
        public string FinancialYear { get; set; }

        [Required]
        public DateTime FromDate { get; set; }
        [Required]
        public DateTime ToDate { get; set; }

        public decimal TotalSalary { get; set; }
        [Required]
        public decimal Accomodation { get; set; }

        public decimal AccomodPercentage { get; set; }
        public decimal TotalFurnitureCost { get; set; }
        public decimal FurnitureCost { get; set; }

        public decimal FurniturePerc { get; set; }

        public decimal TotalIncomeWithAF { get; set; }

        public decimal TotalHaltingAllowance { get; set; }

        public decimal ElectricityCharge { get; set; }

        public decimal WaterCharge { get; set; }

        public decimal FinalTotalIncome { get; set; }

        public decimal Tax1 { get; set; }
        public decimal Tax2 { get; set; }
        public decimal Tax3 { get; set; }
        public decimal TotalTax1 { get; set; }
        public decimal Cess1 { get; set; }
        public decimal TaxableAmtWithTotalTax { get; set; }


        public decimal TaxA1 { get; set; }
        public decimal TaxA2 { get; set; }
        public decimal TaxA3 { get; set; }
        public decimal TotalTaxA1 { get; set; }
        public decimal CessA1 { get; set; }
        public decimal TotalPayable { get; set; }
        public decimal FinalTotalTax { get; set; }
        public decimal PaidTax { get; set; }
        public decimal Balance { get; set; }
        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
        [NotMapped]
        public double AllowanceAmount { get; set; }
        [NotMapped]
        public string HeadName { get; set; }
        [NotMapped]
        public string EmpName { get; set; }
        [NotMapped]
        public string Designation { get; set; }
    }
}
