﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mRtgsSoft")]
    [Serializable]
    public class mRtgsSoft
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RtgsSoftID { get; set; }
        public decimal TransferAmt { get; set; }
        public decimal ComissionAmt { get; set; }
        //Remitter A/C No
        public string RemAcNo { get; set; }
        public string RemName { get; set; }
        public string RemAdd { get; set; }
        public string EmpEmailId { get; set; }
        public int FirmId { get; set; }
        public DateTime CreatedOn { get; set; }
        [NotMapped]
        public string FirmAcNo { get; set; }
        [NotMapped]
        public string FirmName { get; set; }
        [NotMapped]
        public string FirmAdd { get; set; }
        [NotMapped]
        public string EmailId { get; set; }
        [NotMapped]
        public string IFSCCode { get; set; }
        [NotMapped]
        public string Mode { get; set; }
        [NotMapped]
        public string BillIds { get; set; }
        [NotMapped]
        public string BankName { get; set; }
        [NotMapped]
        public int BillType { get; set; }
        [NotMapped]
        public int EstablishId { get; set; }

        [NotMapped]
        public int ClaimantId { get; set; }
    }
}

