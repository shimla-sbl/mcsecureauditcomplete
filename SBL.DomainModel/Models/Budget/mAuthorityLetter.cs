﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mAuthorityLetter")]
    [Serializable]
    public class mAuthorityLetter
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuthorityLetterID { get; set; }

        [Required]
        public DateTime EncashmentDate { get; set; }

        [Required]
        public string LetterType { get; set; }

        public string BillIds { get; set; }
        public string BillNumbers { get; set; }
        [DefaultValue(false)]
        public bool IsActive { get; set; }


        public string Remarks { get; set; }
        public string FileName { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        [NotMapped]
        public string BudgetName { get; set; }

         
    }
}
