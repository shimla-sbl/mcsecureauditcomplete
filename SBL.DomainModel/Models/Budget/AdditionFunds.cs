﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("AdditionFunds")]
    [Serializable]
    public class AdditionFunds
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AdditionFundsID { get; set; }

        [Required]
        public int BudgetId { get; set; }

        [Required]
        public decimal AdditionalFund { get; set; }

        [Required]
        public DateTime FundDate { get; set; }

        public string Remarks { get; set; }
        [Required]
        public string FundType { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }


        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
        [NotMapped]
        public string BudgetName { get; set; }
    }
}
