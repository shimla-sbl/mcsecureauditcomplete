﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mMedicine")]
    [Serializable]
    public class mMedicine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MedicineID { get; set; }
        [Required]
        public string MedicineName { get; set; }
        [Required]
        public int MeidicineType { get; set; }
        [Required]
        public bool IsApproved { get; set; }

        public bool IsActive { get; set; }
        [NotMapped]
        public string Mode { get; set; }
    }
}
