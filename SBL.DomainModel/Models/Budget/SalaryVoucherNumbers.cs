﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mSalaryVoucherNumbers")]
    [Serializable]
    public class SalaryVoucherNumbers
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SalaryVoucherNumbersID { get; set; }
        public string FinancialYear { get; set; }
        public string Months { get; set; }
        public int MonthId { get; set; }
        public string ClassI { get; set; }
        public string ClassII { get; set; }
        public string ClassIII { get; set; }
        public string ClassIV { get; set; }
        public string ClassIV_CPF { get; set; }
        public string ClassIII_CPF { get; set; }
    }
}
