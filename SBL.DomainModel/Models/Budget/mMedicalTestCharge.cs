﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mMedicalTestCharge")]
    [Serializable]
    public class mMedicalTestCharge
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TestChargeID { get; set; }
        [Required]
        public string TestName { get; set; }
        [Required]
        public decimal TestPrice { get; set; }
        [Required]
        public int Category { get; set; }

        public bool IsActive { get; set; }
        [NotMapped]
        public string Mode { get; set; }
    }
}
