﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Budget
{
    [Table("BudgetFiles")]
    [Serializable]
    public class BudgetFile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
      
        public string Year { get; set; }
        
        public string Budget_Title { get; set; }
        
        public string BudgetFolderPath { get; set; }

        public string F_Year { get; set; }
        
    }
}
