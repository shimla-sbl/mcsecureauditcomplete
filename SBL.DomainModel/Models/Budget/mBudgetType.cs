﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mBudgetType")]
    [Serializable]
    public class mBudgetType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BudgetTypeID { get; set; }
        [Required]
        public string TypeName { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }

        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public int? OrderBy { get; set; }
    }
}
