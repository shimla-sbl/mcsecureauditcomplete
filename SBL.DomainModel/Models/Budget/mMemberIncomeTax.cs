﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mMemberIncomeTax")]
    [Serializable]
    public class mMemberIncomeTax
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long MIncomeTaxID { get; set; }

        [Required]
        public int EmpId { get; set; }
        [Required]
        public string FinancialYear { get; set; }
        public decimal TotalSalary { get; set; }
        [Required]
        public decimal AllowanceExampt { get; set; }
        public decimal GrossIncome { get; set; }
        public decimal TaxOnIncome { get; set; }
        public decimal Cess { get; set; }
        public decimal TaxableIncome { get; set; }
        public decimal TaxPayable { get; set; }
        public decimal Cess1 { get; set; }
        public decimal TotalTaxPayable { get; set; }
        public DateTime? CreationDate { get; set; }
        public string Quarter { get; set; }
        public string CreatedBy { get; set; }
        [NotMapped]
        public string EmpName { get; set; }
        [NotMapped]
        public string Designation { get; set; }
        [NotMapped]
        public string PanNumber { get; set; }
    }
}
