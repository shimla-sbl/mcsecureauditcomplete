﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mBudgetBillTypes")]
    [Serializable]
    public class mBudgetBillTypes
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BillTypeID { get; set; }
        [Required]
        public string TypeName { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        [NotMapped]
        public string Mode { get; set; }
    }
}
