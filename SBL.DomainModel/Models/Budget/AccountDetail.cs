﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Serializable]
    public class AccountDetail
    {

        public int MemberCode { get; set; }
        public int EstablishId { get; set; }
        public int BillNumber { get; set; }
        public string ObjectCodeText { get; set; }
        public string AccountNo { get; set; }
        public string BankName { get; set; }
        public string IFSCCode { get; set; }
        public decimal VoucherAmount { get; set; }
        public string BudgetName { get; set; }
        public string FinancialYear { get; set; }
        public string ClaimantName { get; set; }
        public int Designation { get; set; }
        public string EmailId { get; set; }
        public string Address { get; set; }
        public int BillType { get; set; }
        public int ClaimantId { get; set; }
        public int AdvanceBillNumber { get; set; }
        public decimal AdvanceBillGrossAmt { get; set; }

        public string MemberPayeeCode { get; set; }
    }
}
