﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Budget
{
    [Table("tBudget")]
    [Serializable]
    public class tBudget
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int AssemblyId { get; set; }

        public int SessionId { get; set; }

        [Required]
        public int DemandType { get; set; }

        public string Year { get; set; }

        public string File_name { get; set; }

        public string File_path { get; set; }




    }
}
