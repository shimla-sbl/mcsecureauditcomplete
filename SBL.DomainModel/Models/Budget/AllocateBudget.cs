﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("AllocateBudget")]
    [Serializable]
  public  class AllocateBudget
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AllocateBudgetID { get; set; }

        [Required]
        public int BudgetId { get; set; }

        [Required]
        public decimal Sanctionedbudget { get; set; }

        [Required]
        public DateTime AllocateBudgetDate { get; set; }
        
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }


        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
        [NotMapped]
        public string  BudgetName { get; set; }
    }
}
