﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Budget
{
    [Table("mDebitReceiptExp")]
    [Serializable]
    public class mDebitReceiptExp
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DebitReceiptExpID { get; set; }

        [Required]
        public int BudgetId { get; set; }

        [Required]
        public DateTime DebitReceiptDate { get; set; }

        [Required]
        public decimal DebitReceiptAmount { get; set; }

       // public decimal DebitDeduction { get; set; }

       
      //  public decimal ReductionAmountTaken { get; set; }

        //[Required]
        //public decimal NetAmount { get; set; }

        public string Remarks { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
        [NotMapped]
        public string BudgetName { get; set; }
    }
}
