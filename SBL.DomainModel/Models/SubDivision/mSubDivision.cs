﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.SubDivision
{
    [Serializable]
    [Table("mSubDivision")]

    public class mSubDivision
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int mSubDivisionId { get; set; }
        [Required]
        public string SubDivisionName { get; set; }
        public string SubDivisionNameLocal { get; set; }
        public int ConstituencyID { get; set; }
        public string SubDivisionAbbreviation { get; set; }

        public int? DistrictCode { get; set; }

        public bool? IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedWhen { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        [NotMapped]
        public string GetConstituencyName { get; set; }
        [NotMapped]
        public int AssemblyId { get; set; }


    }
}