﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.SystemModule
{
    [Table("ModuleAction")]
    [Serializable]
    public partial class ModuleAction
    {
        public ModuleAction()
        {
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SystemFunctionID { get; set; }
        public string Name { get; set; }
        public int ModuleId { get; set; }
        public string ActionName { get; set; }
        public string Parameters { get; set; }
        public string AssociatedContactGroups { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public string SMSTemplate { get; set; }
        public string EmailTemplate { get; set; }
        public bool SendSMS { get; set; }
        public bool SendEmail { get; set; }
        [NotMapped]
        public string Mode { get; set; }
        [NotMapped]
        public bool SaveCreateNew { get; set; }
        [NotMapped]
        public List<SystemModuleModel> objList { get; set; }
    }
}
