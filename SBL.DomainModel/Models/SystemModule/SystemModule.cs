﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.SystemModule
{
    [Table("SystemModule")]
    [Serializable]
    public partial class SystemModule
    {
        public SystemModule()
        {
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ModuleIdID { get; set; }
        public string ModuleName { get; set; }
        public string ModuleDescription { get; set; }
    }
}
