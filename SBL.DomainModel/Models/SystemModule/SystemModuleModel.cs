﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.SystemModule
{
    [Serializable]
    public class SystemModuleModel
    {
        public SystemModuleModel()
        {
            this.ParamList = new List<string>();
            this.groupNameIDPair = new List<KeyValuePair<Int32, string>>();
        }
        public int Count { get; set; }
        public int SystemModuleID { get; set; }       
        public string Name { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int ModuleId { get; set; }
        public string Description { get; set; }
        public string Mode { get; set; }
        public bool SaveCreateNew { get; set; }
        public List<SystemModuleModel> objList { get; set; }
        public List<SystemModuleModel> RecipList { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string ActionName { get; set; }
        public string Parameters { get; set; }
        public string AssociatedContactGroups { get; set; }   
        public string SMSTemplate { get; set; }
        public string EmailTemplate { get; set; }
        public bool SendSMS { get; set; }
        public bool SendEmail { get; set; }
        public int RecipGroupID { get; set; }
        public string RecipName { get; set; }
        public List<string> RecipNameIds { get; set; }
        public List<KeyValuePair<Int32, string>> groupNameIDPair { get; set; }
        public List<SystemModuleModel> UpdatevalueList { get; set; }
        public virtual ICollection<SystemModuleModel> SystemModuleMod { get; set; }
        public int UAId { get; set; }
        public int GroupId { get; set; }
        public string Designation { get; set; }
        public string EMail { get; set; }
        public string Mobile { get; set; }
        public string Template { get; set; }
        public List<string> ParamList { get; set; }
        public string PName { get; set; }
        public string Message { get; set; }
        
    }
}
