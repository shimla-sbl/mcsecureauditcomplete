﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Party
{
    [Table("mParty")]
    [Serializable]
    public class mParty
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PartyID { get; set; }

        //[Range(1, 5)]
        public string PartyCode { get; set; }

        public string PartyName { get; set; }

        public string PartyName_Local { get; set; }

        [Column(TypeName = "image")]
        public byte[] Photo { get; set; }

        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }
        
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

    }
}
