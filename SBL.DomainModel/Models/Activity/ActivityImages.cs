﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Activity
{
   
    [Serializable]
    [Table("tActivityImages")]
    public class ActivityImages
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }

        public int ActivityId { get; set; }

        public string FileName { get; set; }

        public string FilePath { get; set; }

       // public bool IsActive { get; set; }
    }
}
