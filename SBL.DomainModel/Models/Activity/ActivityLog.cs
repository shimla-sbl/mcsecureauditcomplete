﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Activity
{
    [Serializable]
    [Table("tActivityLog")]
  
    public class ActivityLog
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }

        public int ActivityCategoryId { get; set; }

        public int ActivityCode { get; set; }

        public int MemberCode { get; set; }

        public DateTime Date { get; set; }
        
        public string ActivityLocation { get; set; }
        
        public string ActivityDescription { get; set; }

        public string FileLocation { get; set; }

      
    }
}
