﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Activity
{
   
    [Serializable]
    [Table("tActivityCategory")]
    public class ActivityCategory
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }

        public string ActivityName { get; set; }

        public string ActivityName_Local { get; set; }

        public bool IsActive { get; set; }

       
    }
}
