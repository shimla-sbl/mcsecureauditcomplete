﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.CutMotionDemand
{
    [Serializable]
    [Table("tCutMotionDemand")]
    public class tCutMotionDemand
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CMDemandId { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public int? DemandNo { get; set; }
        public string DemandName { get; set; }
        public string DemandName_Local { get; set; }
        public string DemandAmountType { get; set; }
        public string DemandAmountType_Local { get; set; }
        public string RevenueAmount { get; set; }
        public string CapitalAmount { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        [NotMapped]
        public string AssemblyName { get; set; }

        [NotMapped]
        public string SessionName { get; set; }
     
        [NotMapped]
        public List<tCutMotionDemand> DemandList { get; set; }
    }
}
