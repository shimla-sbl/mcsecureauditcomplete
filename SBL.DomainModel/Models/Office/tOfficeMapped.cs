﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SBL.DomainModel.Models.Office
{
	[Table("tDepartmentOfficeMapped")]
	[Serializable]
	public class tDepartmentOfficeMapped
	{
		[Key]
		public long MapId { get; set; }
		public string DeptId { get; set; }
		public int OfficeId { get; set; }
		public int ConstituencyCode { get; set; }
		public int AssemblyCode { get; set; }
        public int? MemberCode { get; set; }
       // public bool? IsNodalOffice { get; set; }
		[NotMapped]
		public string Ids { get; set; }
		[NotMapped]
		public string Names { get; set; }
		[NotMapped]
		public List<int> GetId { get; set; }
        [NotMapped]
        public string officename { get; set; }
        
	}
}
