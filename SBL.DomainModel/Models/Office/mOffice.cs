﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SBL.DomainModel.Models.Office
{
    [Table("mOffice")]
    [Serializable]
    public class mOffice
    {
        [Key]
        public int OfficeId { get; set; }
        public int? officelevel { get; set; }
        public string officename { get; set; }
        public string officeaddr { get; set; }
        public int ? distcd { get; set; }
        public string urbrul { get; set; }
        public string tehsilcode { get; set; }
        public string blockcd { get; set; }
        public string panchcd { get; set; }
        public string concd { get; set; }
        public string desigcode { get; set; }
        public string reportoffice { get; set; }
        public string EmailId { get; set; }
        public string Wh_SSt { get; set; }
        public string deptid { get; set; }
		public string officenamelocal { get; set; }
		public string officeaddresslocal { get; set; }
        public string NodalOfficerEmpCode { get; set; }
        public string grcellincharge { get; set; }
        public string IsOnline { get; set; }
        public string isgrcell { get; set; }
        public string grcellinchargedeptid { get; set; }
        public string DEuserid { get; set; }
        public string DEdate { get; set; }
        public string officephone { get; set; }
        public string GenOffice { get; set; }
        public string mapoldofficeid { get; set; }
        public string mapnewofficeid { get; set; }
        public string officetypecd { get; set; }
        public string tribalAreaCode { get; set; }
        public string ddodemandcd { get; set; }
        public string treasurycd { get; set; }
        public string mobilenumber { get; set; }
		public bool IsMapped { get; set; }
        public Int64? OfficeCode { get; set; }
        public bool? IsNodalOffice { get; set; }
    }
}
