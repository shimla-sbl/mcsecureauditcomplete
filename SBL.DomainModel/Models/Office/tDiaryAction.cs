﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;



namespace SBL.DomainModel.Models.Office
{
    [Table("tDiaryAction")]
    [Serializable]
    public class tDiaryAction
    {
        [Key]
        public int Id { get; set; }
        public int ? OfficeId { get; set; }
        public string DiaryRefId { get; set; }
        public int? ActionId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? ActionDate { get; set; }
        public string ActionBy { get; set; }
        public string ActionDescription { get; set; }
        public string Enclosure { get; set; }
        public string Status { get; set; }
        public string IsMember { get; set; }
        public string ActionByMember { get; set; }
    }
}
