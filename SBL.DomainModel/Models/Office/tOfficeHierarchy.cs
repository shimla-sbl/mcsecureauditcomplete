﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SBL.DomainModel.Models.Office
{
    [Table("tOfficeHierarchy")]
    [Serializable]
    public class tOfficeHierarchy
    {
        [Key]
        public int Id { get; set; }
        public int OfficeId { get; set; }
        public int SubOfficeId { get; set; }
        public int OrderNo { get; set; }
    }
}
