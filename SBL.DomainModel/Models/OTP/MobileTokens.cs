﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.OTP
{
    [Table("MobileTokens")]
    [Serializable]
    public partial class MobileTokens
    {

        public MobileTokens()
        {

        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string ClientID { get; set; }

        public string ClientKey { get; set; }

        public string Token { get; set; }

        public DateTime CreationDate { get; set; }

        public bool IsActive { get; set; }

    }
}
