﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.OTP
{
    [Table("tOTPDetails")]
    [Serializable]
    public partial class tOTPDetails
    {

        public tOTPDetails()
        {
          
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OTPid { get; set; }

        public string OTP { get; set; }

        public bool IsActive { get; set; }

        public string IsMatched { get; set; }

        public DateTime? CreationDate { get; set; }

        public string ModuleId { get; set; }

        public string MobileNo { get; set; }

        public string UserName { get; set; }

        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public string ProfilePic { get; set; }

        public string AadharID { get; set; }
      
    }
}
