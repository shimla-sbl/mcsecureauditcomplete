﻿namespace SBL.DomainModel.Models.Bill
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tBillMotion")]
    [Serializable]
    public partial class tBillMotion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BillMotionId { get; set; }

        public int BillId { get; set; }

        public int MotionId { get; set; }

        public DateTime? MotionDate { get; set; }

        public string NoticeId { get; set; }

        public int MotionActionId { get; set; }

        public DateTime? CirculationDispatchDate { get; set; }

        public DateTime? MotionActionDateDate { get; set; }

        public string MemberId { get; set; }

        public DateTime? MotionExtendedDate { get; set; }

        public string MotionExtendedTime { get; set; }

        public string MotionRemarks { get; set; }

        public DateTime? LastDate { get; set; }
    }
}
