﻿namespace SBL.DomainModel.Models.Bill
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tBillRuleRegister")]
    [Serializable]
    public partial class tBillRuleRegister
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RuleRegisterId { get; set; }

        public int RuleId { get; set; }

        public int BillId { get; set; }

        public bool Remarks { get; set; }

        public int SessionId { get; set; }

        public int AssemblyId { get; set; }
    }
}
