﻿namespace SBL.DomainModel.Models.Bill
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("mBillStatus")]
    [Serializable]
    public partial class mBillStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BillStatusId { get; set; }

        [Required]
        [StringLength(150)]
        public string BillStatus { get; set; }

        public string BillStatusLocal { get; set; }

        public string Description { get; set; }

        public bool? Active { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
