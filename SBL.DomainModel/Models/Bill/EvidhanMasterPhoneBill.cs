﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Member;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Bill
{
    [Table("EvidhanMasterPhoneBill")]
    [Serializable]
    public class EvidhanMasterPhoneBill
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MasterPhoneBillID { get; set; }

        public string TelephoneNumber { get; set; }
        
        public bool isMinister { get; set; }

        public string NameOfPersonOrDepart { get; set; }

        public string TelephoneOwnOrGovernment { get; set; }

        public string PlaceWherePhoneIsInstalled { get; set; }

        public string DateWhenPhoneInstalled { get; set; }

        public string SecurityOfPhone { get; set; }

        public int MinistersCODE { get; set; }

        [NotMapped]
        public ICollection<fillListGenricInt> MemberCol { get; set; }
        
    }
}
