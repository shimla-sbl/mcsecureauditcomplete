﻿namespace SBL.DomainModel.Models.Bill
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("mBillType")]
    [Serializable]
    public partial class mBillType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BillTypeId { get; set; }

        [Required]
        [StringLength(150)]
        public string BillType { get; set; }

        public string Description { get; set; }

        public bool? IsActive { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
