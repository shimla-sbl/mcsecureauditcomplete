﻿namespace SBL.DomainModel.Models.Bill
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tBillMotionOpinion")]
    [Serializable]
    public partial class tBillMotionOpinion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MotionOpinionId { get; set; }

        public int BillId { get; set; }

        public string Suggestion { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public long MobileNumber { get; set; }

        public string Email { get; set; }

        public string State { get; set; }

        public string District { get; set; }

        public DateTime? DateTime { get; set; }
    }
}
