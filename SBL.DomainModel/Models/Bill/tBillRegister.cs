﻿namespace SBL.DomainModel.Models.Bill
{
    using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

    [Table("tBillRegister")]
    [Serializable]
    public partial class tBillRegister
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BillId { get; set; }

        public long? PaperLaidId { get; set; }

        public int BillTypeId { get; set; }

        [Required]
        public string BillNumber { get; set; }

        public string BillFileNumber { get; set; }

        [Required]
        public string ShortTitle { get; set; }

        public string BillName { get; set; }

        public int AssemblyId { get; set; }
        public int Status { get; set; }

        public int SessionId { get; set; }

        public int MinisterId { get; set; }

        public DateTime? BillLayDate { get; set; }

        public string DepartmentId { get; set; }

        public bool IsMoneyBill { get; set; }

        public bool IsFinancialBillRule1 { get; set; }

        public bool IsFinancialBillRule2 { get; set; }

        public bool IsCirculation { get; set; }

        public int BillClauses { get; set; }

        public int BillSchedules { get; set; }

        public bool IsRecommendedArticle { get; set; }

        public bool IsClauseConsideration { get; set; }

        public bool IsSubmitted { get; set; }

        public bool IsForwordedToMinister { get; set; }

        public bool IsIntentionSigned { get; set; }

        public bool IsBillSigned { get; set; }

        public bool IsIntentioned { get; set; }

        public bool IsIntentionedToMinister { get; set; }

        public bool IsThirdReading { get; set; }

        public bool IsReferSelectCommitte { get; set; }

        public DateTime? BillDate { get; set; }

        public int BillStatusId { get; set; }

        public DateTime? BillIntroducedProposedDate { get; set; }

        public DateTime? BillIntroducedActualDate { get; set; }

        public DateTime? BillAssentedDate { get; set; }

        public string BillAssentedBy { get; set; }

        public DateTime? BillGeneralDiscussionDate { get; set; }

        public DateTime? BillPassedDate { get; set; }

        public string BillPassedRemarks { get; set; }

        public bool IsBillAct { get; set; }

        public string ActNumber { get; set; }

        public DateTime? ActDate { get; set; }

        public string ActDescription { get; set; }

        public string ActRemarks { get; set; }

        public DateTime? BillGazzetPublishedDate { get; set; }

        public DateTime? BillGazzetPublishedDatePassed { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public bool IsElicitingOpinion { get; set; }

        public bool IsWithdrawal { get; set; }

        public bool IsIntentionReturn { get; set; }

        public bool IsBillReturn { get; set; }

        public string IntentionReturnRemarks { get; set; }

        public string BillReturnRemarks { get; set; }

        [NotMapped]
        public virtual ICollection<mSession> mAssemblySession { get; set; }

        public int? BillNo { get; set; }

        public int? BillYear { get; set; }

        public bool? IsAmendment { get; set; }

        public int? PreviousBillId { get; set; }

        [StringLength(500)]
        public string SignedFilePath { get; set; }

        public DateTime? DeptSubmittedDate { get; set; }

         [NotMapped]
       public string Ids { get; set; }
          [NotMapped]
         public string FilePath { get; set; }
    }
}
