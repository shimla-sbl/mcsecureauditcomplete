﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Bill
{
    [Table("EVidhanEPWBillTransaction")]
    [Serializable]
    public class EVidhanEPWBillTransaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EPWBillTransactionID { get; set; }

        public string BillType { get; set; }

        public int BillersID { get; set; }

        [NotMapped]
        public string BillersName { get; set; }

        public bool IsMLA { get; set; }

        public bool IsMinster { get; set; }

        [NotMapped]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public string SubmitDate { get; set; }//for ui

        public DateTime SubmitDateDB { get; set; }//for database

        public string BillDate { get; set; }

        public string BillNumber { get; set; }

        public string BillRangeFrom { get; set; }

        public string BillRangeTo { get; set; }

        public string TelephoneRentalRangeFrom { get; set; }

        public string TelephoneRentalRangeTo { get; set; }

        public string TotalAmount { get; set; }

        public string AmountPayable { get; set; }

        public string Remarks { get; set; }

        [NotMapped]
        public DateTime SanctionSubmitRangeFrom { get; set; }

        [NotMapped]
        public DateTime SanctionSubmitRangeTo { get; set; }

        [NotMapped]
        public int FinancialYear { get; set; }

        [NotMapped]
        public int FinancialMonth { get; set; }

        public string BillFileName { get; set; }

        [NotMapped]
        public string edit { get; set; }


    }
}
