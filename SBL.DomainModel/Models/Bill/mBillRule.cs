﻿namespace SBL.DomainModel.Models.Bill
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("mBillRules")]
    [Serializable]
    public partial class mBillRule
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BillRuleId { get; set; }

        public int RuleId { get; set; }

        [Required]
        public string Rules { get; set; }

        public string Remarks { get; set; }

        public int RuleSubId { get; set; }
    }
}
