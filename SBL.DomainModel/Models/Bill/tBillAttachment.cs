﻿namespace SBL.DomainModel.Models.Bill
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tBillAttachment")]
    [Serializable]
    public partial class tBillAttachment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BillAttachmentId { get; set; }

        public int BillId { get; set; }

        [StringLength(150)]
        public string BillStage { get; set; }

        [StringLength(350)]
        public string FilePath { get; set; }

        public DateTime UploadDate { get; set; }
    }
}
