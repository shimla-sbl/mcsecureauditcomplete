﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;

namespace SBL.DomainModel.Models.Bill
{
    [Serializable]
    public class BillWorkModel
    {
        public BillWorkModel()
        {
            this.mAssemblyList = new List<mAssembly>();
            this.sessionList = new List<mSession>();
            this.List = new List<mBills>();

        }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mAssembly> mAssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }
        public List<mBills> List { get; set; }

        public int ID { get; set; }

        public float? SNO { get; set; }

        public string BillTitle { get; set; }

        public string BillNo { get; set; }

        public DateTime? IntroductionDate { get; set; }

        public DateTime? PassingDate { get; set; }

        public DateTime? AssesntDate { get; set; }
        public DateTime? PublishedDate { get; set; }
        public string AssentedBy { get; set; }

        public string ActNo { get; set; }

        public string IntroductionFilePath { get; set; }

        public string PassingFilePath { get; set; }

        public string AccentedFilePath { get; set; }

        public string ActFilePath { get; set; }

        public int? AssemblyId { get; set; }

        public int? SessionId { get; set; }
        public string AssesmblyName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<BillWorkModel> SessionLt { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public bool? IsFreeze { get; set; }

    }
}
