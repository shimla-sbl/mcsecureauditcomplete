﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Session;

namespace SBL.DomainModel.Models.Bill
{
    [Table("mBills")]
    [Serializable]
    public partial class mBills
    {
        public mBills()
        {

            this.sessionList = new List<mSession>();
            this.mAssemblyList = new List<mAssembly>();
            this.MinistryList = new List<mMinisteryMinisterModel>();
            this.DepartmentList = new List<mDepartment>();


        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public float? SNO { get; set; }

        public string BillTitle { get; set; }

        public string BillNo { get; set; }

        public DateTime? IntroductionDate { get; set; }

        public DateTime? PassingDate { get; set; }

        public DateTime? AssesntDate { get; set; }

        public DateTime? PublishedDate { get; set; }

        public string AssentedBy { get; set; }

        public string ActNo { get; set; }

        public string IntroductionFilePath { get; set; }

        public string PassingFilePath { get; set; }

        public string AccentedFilePath { get; set; }

        public string ActFilePath { get; set; }

        //umang

        public int? PaperCategoryId { get; set; }

        public int? IsProofReaderApproved { get; set; }

        public int? IsOldData { get; set; }

        public string Remarks { get; set; }

        public string RefActNo { get; set; }

        public bool? IsFreeze { get; set; }

        [NotMapped]
        public bool TempIsFreeze { get; set; }

        public int? AssemblyId { get; set; }

        public int? SessionId { get; set; }

        [NotMapped]
        public virtual ICollection<BillWorkModel> BillWorkModel { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<mAssembly> mAssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<BillWorkModel> AssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mMinisteryMinisterModel> MinistryList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mDepartment> DepartmentList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Select Minister")]
        public int MinisterId { get; set; }
        [NotMapped]
        public List<mMinisteryMinisterModel> ministerList { get; set; }
        [NotMapped]
        [StringLength(50)]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<tMemberNotice> DepartmentLt { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<BillWorkModel> SessionLt { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<mSession> SessLst { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string IDate { get; set; }
        [NotMapped]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [HiddenInput(DisplayValue = false)]
        public string PDate { get; set; }
        [NotMapped]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [HiddenInput(DisplayValue = false)]
        public string ADate { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<SelectListItem> yearList { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int BillNumberYear { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int BillYearS { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int BillNoS { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int ActYearS { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int ActNoS { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? AssemblyID { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? SessionID { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AssesmblyName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string Name { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int Length { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string Type { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string IntroFileName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string PassFileName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AssentFileName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string ActFileName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string FileStructurePath { get; set; }

        [NotMapped]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [HiddenInput(DisplayValue = false)]
        public string PubDate { get; set; }

        public bool? IsLaid { get; set; }
    }
}
