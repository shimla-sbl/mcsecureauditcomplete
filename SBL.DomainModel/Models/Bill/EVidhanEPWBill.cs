﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Bill
{
    [Table("eVidhanEPWBill")]
    [Serializable]
    public class EVidhanEPWBill
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EPWBillID { get; set; }

        public string BillType { get; set; }

        public int BillersID { get; set; }

        public string TelephoneNo { get; set; }

        public bool IsMLA { get; set; }

        public string ElectricityMeterNo { get; set; }

        public string WaterMeterNo { get; set; }

        public string TelephoneOwnOrGovt { get; set; }

        public string PlaceWherePhoneInstalled { get; set; }

        public string DateOfPhoneInstalled { get; set; }

        public string SecurityDepositOfPhone { get; set; }

        public string SetNo { get; set; }

        public string MLAHostel { get; set; }

        [NotMapped]
        public string BillersName { get; set; }

        [NotMapped]
        public string BillersDesignation { get; set; }

        [NotMapped]
        public ICollection<EVidhanEPWBill> EPWList { get; set; }

        [NotMapped]
        public ICollection<EvidhanMasterPhoneBill> PhoneList { get; set; }

    }
}
