﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SBL.DomainModel.Models.LOB
{
    [Serializable]
    [Table("mResolution")]
     public partial class mResolution
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ResolutionId { get; set; }

        public string Resolution { get; set; }

        public string PageSizeWidth { get; set; }
        public string PageSizeHeight { get; set; }
    }
}
