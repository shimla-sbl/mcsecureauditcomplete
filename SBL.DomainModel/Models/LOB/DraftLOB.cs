﻿namespace SBL.DomainModel.Models.LOB
{
    using Lib.Web.Mvc.JQuery.JqGrid.Constants;
    using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;

    [Table("DraftLOB")]
    [Serializable]
    public partial class DraftLOB
    {
        public DraftLOB()
        {

        }

        public DraftLOB(DraftLOB draftLOB)
        {
            this.AssemblyName = draftLOB.AssemblyName;
            this.SessionName = draftLOB.SessionName;
            this.SessionDate = draftLOB.SessionDate;
            this.ConcernedEventName = draftLOB.ConcernedEventName;
            this.SessionDateLOB = draftLOB.SessionDate;
            this.SessionDateSQuestion = draftLOB.SessionDate;
            this.SessionDateUSQuestion = draftLOB.SessionDate;
        }

        [HiddenInput(DisplayValue = false)]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int LOBId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? AssemblyId { get; set; }

        [DisplayName("Assembly Name")]
        [StringLength(200)]
        public string AssemblyName { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string AssemblyNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SessionId { get; set; }

        [DisplayName("Session Name")]
        [StringLength(200)]
        public string SessionName { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string SessionNameLocal { get; set; }

        [DisplayName("Session Date")]
        [JqGridColumnFormatter(JqGridColumnPredefinedFormatters.Date, SourceFormat = "d.m.Y G:i:s", OutputFormat = "d/m/Y")]
        public DateTime? SessionDate { get; set; }



        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string SessionDateLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string SessionTime { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string SessionTimeLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SrNo1 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SrNo2 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SrNo3 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextLOB { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextSpeaker { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextMinister { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextBrief { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextCurrent { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? ConcernedEventId { get; set; }

        [DisplayName("Event Name")]
        [StringLength(500)]
        public string ConcernedEventName { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string ConcernedEventNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? CommitteeId { get; set; }


        [StringLength(500)]
        public string CommitteeTitle { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? CommitteeRepTypId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DeptId { get; set; }

        [NotMapped]
        [DisplayName("LOB")]
        [JqGridColumnLayout(Width = 75)]
        [JqGridColumnFormatter("$.SessionDateLOBFormatter")]
        public DateTime? SessionDateLOB { get; set; }

        [NotMapped]
        [DisplayName("Starred Question")]
        [JqGridColumnFormatter("$.SessionDateSQuestionFormatter")]
        public DateTime? SessionDateSQuestion { get; set; }

        [NotMapped]
        [DisplayName("Unstarred Question")]
        [JqGridColumnFormatter("$.SessionDateUSQuestionFormatter")]
        public DateTime? SessionDateUSQuestion { get; set; }


        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string PPTLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string PDFLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string VideoLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(100)]
        public string ActionDocumentTypePPT { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(100)]
        public string ActionDocumentTypePDF { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(100)]
        public string ActionDocumentTypeVideo { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? PageBreak { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsEVoting { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string CreatedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? CreatedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string ModifiedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? ModifiedDate { get; set; }


        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string DraftLOBxmlLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsSubmitted { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? SubmittedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string SubmittedLOBPath { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsApproved { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsPublished { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsSpeakerPageBreak { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsDeleted { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string BillNo { get; set; }


        [HiddenInput(DisplayValue = false)]
        public bool? IsCorrigendum { get; set; }

        [HiddenInput(DisplayValue = false)]
        public TimeSpan? SubmittedTime { get; set; }
        public string XmlEventLineNo { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string PaperLaidId { get; set; }
    }
}
