﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.LOB
{
    [Serializable]
    public partial class CorrigendumLOB
    {
        [HiddenInput(DisplayValue = false)]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CorrigendumId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int LOBId { get; set; }

        public DateTime? SubmittedDate { get; set; }

        public TimeSpan? SubmittedTime { get; set; }

        public bool IsSubmitted { get; set; }

        public string SubmittedFile { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public TimeSpan? ApprovedTime { get; set; }

        public bool IsApproved { get; set; }

        public string ApprovedFile { get; set; }
    }
}
