﻿namespace SBL.DomainModel.Models.LOB
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using System.ComponentModel;

    [Serializable]
    [Table("AdminLOB")]
    public partial class AdminLOB
    {
        public AdminLOB()
        {
            this.ListDraftLOB = new List<DraftLOB>();
        }

        public AdminLOB(AdminLOB adminLOB)
        {
            this.AssemblyName = adminLOB.AssemblyName;
            this.SessionName = adminLOB.SessionName;
            this.SessionDate = adminLOB.SessionDate;
            this.ConcernedEventName = adminLOB.ConcernedEventName;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int DraftRecordId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int LOBId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? AssemblyId { get; set; }

        [StringLength(200)]
        [DisplayName("Assembly Name")]
        public string AssemblyName { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string AssemblyNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SessionId { get; set; }

        [StringLength(200)]
        [DisplayName("Session Name")]
        public string SessionName { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string SessionNameLocal { get; set; }

        [DisplayName("Session Date")]
        public DateTime? SessionDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string SessionDateLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string SessionTime { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string SessionTimeLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SrNo1 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SrNo2 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SrNo3 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextLOB { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextSpeaker { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextMinister { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextBrief { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextCurrent { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? ConcernedEventId { get; set; }

        [StringLength(500)]
        [DisplayName("Event Name")]
        public string ConcernedEventName { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string ConcernedEventNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? CommitteeId { get; set; }


        [StringLength(500)]
        public string CommitteeTitle { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? CommitteeRepTypId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DeptId { get; set; }
        
        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string PPTLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string PDFLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string VideoLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(100)]
        public string ActionDocumentTypePPT { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(100)]
        public string ActionDocumentTypePDF { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(100)]
        public string ActionDocumentTypeVideo { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? PageBreak { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsEVoting { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string CreatedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? CreatedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string ApprovedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? ApprovedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(200)]
        public string ModifiedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? ModifiedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string LOBxmlLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string SQuestionsxmlLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(500)]
        public string USQuestionsxmlLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string LOBPath { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsPublished { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsSpeakerSubmitted { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(50)]
        public string SpeakerSubmittedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(50)]
        public string SpeakerSubmittedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsSpeakerApproved { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(50)]
        public string SpeakerApprovedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        [StringLength(50)]
        public string SpeakerApprovedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsSpeakerPublished { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string SpeakerSubmittedLOBPath { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string SpeakerApprovedLOBPath { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsSpeakerPageBreak { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public int? MemberId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string BillNo { get; set; }
        public string XmlEventLineNo { get; set; }
        //Paging Properties
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PAGE_SIZE { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageIndex { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int loopStart { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int loopEnd { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int selectedPage { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageNumber { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int RowsPerPage { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsDeleted { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsCorrigendum { get; set; }

        [HiddenInput(DisplayValue = false)]
        public TimeSpan? SubmittedTime { get; set; }

        [HiddenInput(DisplayValue = false)]
        public TimeSpan? ApprovedTime { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<DraftLOB> ListDraftLOB { get; set; }
        public bool? IsLaid { get; set; }
    }
}
