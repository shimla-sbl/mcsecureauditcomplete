﻿namespace SBL.DomainModel.Models.LOB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;

    [Table("CurrentLOB")]
    [Serializable]
    public partial class CurrentLOB
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LOBId { get; set; }

        public int DraftRecordId { get; set; }

        public int? AssemblyId { get; set; }

        [StringLength(200)]
        public string AssemblyName { get; set; }

        [StringLength(200)]
        public string AssemblyNameLocal { get; set; }

        public int? SessionId { get; set; }

        [StringLength(200)]
        public string SessionName { get; set; }

        [StringLength(200)]
        public string SessionNameLocal { get; set; }

        public DateTime? SessionDate { get; set; }

        [StringLength(200)]
        public string SessionDateLocal { get; set; }

        [StringLength(200)]
        public string SessionTime { get; set; }

        [StringLength(200)]
        public string SessionTimeLocal { get; set; }

        public int? SrNo1 { get; set; }

        public int? SrNo2 { get; set; }

        public int? SrNo3 { get; set; }

        public string TextLOB { get; set; }

        public string TextSpeaker { get; set; }

        public string TextMinister { get; set; }

        public string TextBrief { get; set; }

        public string TextCurrent { get; set; }

        public int? ConcernedEventId { get; set; }

        [StringLength(500)]
        public string ConcernedEventName { get; set; }

        [StringLength(500)]
        public string ConcernedEventNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? CommitteeId { get; set; }


        [StringLength(500)]
        public string CommitteeTitle { get; set; }

        [StringLength(500)]
        public string PPTLocation { get; set; }

        [StringLength(500)]
        public string PDFLocation { get; set; }

        [StringLength(500)]
        public string VideoLocation { get; set; }

        [StringLength(100)]
        public string ActionDocumentTypePPT { get; set; }

        [StringLength(100)]
        public string ActionDocumentTypePDF { get; set; }

        [StringLength(100)]
        public string ActionDocumentTypeVideo { get; set; }

        public bool? PageBreak { get; set; }

        public bool? IsEVoting { get; set; }

        [StringLength(200)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(200)]
        public string ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public DateTime? PublishedDate { get; set; }

        [StringLength(200)]
        public string PublishedBy { get; set; }

        [StringLength(200)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool? IsSpeakerSubmitted { get; set; }

        [StringLength(50)]
        public string SpeakerSubmittedBy { get; set; }

        [StringLength(50)]
        public string SpeakerSubmittedDate { get; set; }

        public bool? IsSpeakerApproved { get; set; }

        [StringLength(50)]
        public string SpeakerApprovedBy { get; set; }

        [StringLength(50)]
        public string SpeakerApprovedDate { get; set; }

        public bool? IsSpeakerPublished { get; set; }

        [StringLength(50)]
        public string SpeakerPublishedBy { get; set; }

        [StringLength(50)]
        public string SpeakerPublishedDate { get; set; }

        public string PublishLOBPath { get; set; }

        public string SpeakerPublishedLOBPath { get; set; }

        public string SpeakerSubmittedLOBPath { get; set; }

        public string SpeakerApprovedLOBPath { get; set; }

        public bool? IsSpeakerPageBreak { get; set; }

        public bool? IsDeleted { get; set; }

        public bool? IsCorrigendum { get; set; }

        
        public TimeSpan? ApprovedTime { get; set; }

        public TimeSpan? SubmittedTime { get; set; }
    }
}
