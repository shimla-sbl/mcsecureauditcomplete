﻿namespace SBL.DomainModel.Models.LOB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("Audit_PublishedLOB")]
    public partial class Audit_PublishedLOB
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int LOBId { get; set; }

        public int? AssemblyId { get; set; }

        [StringLength(200)]
        public string AssemblyName { get; set; }

        [StringLength(200)]
        public string AssemblyNameLocal { get; set; }

        public int? SessionId { get; set; }

        [StringLength(200)]
        public string SessionName { get; set; }

        [StringLength(200)]
        public string SessionNameLocal { get; set; }

        public DateTime? SessionDate { get; set; }

        [StringLength(200)]
        public string SessionDateLocal { get; set; }

        [StringLength(200)]
        public string SessionTime { get; set; }

        [StringLength(200)]
        public string SessionTimeLocal { get; set; }

        public int? SrNo1 { get; set; }

        public int? SrNo2 { get; set; }

        public int? SrNo3 { get; set; }

        public string TextLOB { get; set; }

        public string TextSpeaker { get; set; }

        public string TextMinister { get; set; }

        public string TextBrief { get; set; }

        public string TextCurrent { get; set; }

        public int? ConcernedEventId { get; set; }

        [StringLength(500)]
        public string ConcernedEventName { get; set; }

        [StringLength(500)]
        public string ConcernedEventNameLocal { get; set; }

        [StringLength(500)]
        public string PPTLocation { get; set; }

        [StringLength(500)]
        public string PDFLocation { get; set; }

        [StringLength(500)]
        public string VideoLocation { get; set; }

        [StringLength(100)]
        public string ActionDocumentTypePPT { get; set; }

        [StringLength(100)]
        public string ActionDocumentTypePDF { get; set; }

        [StringLength(100)]
        public string ActionDocumentTypeVideo { get; set; }

        public bool? PageBreak { get; set; }

        public bool? IsEVoting { get; set; }

        [StringLength(200)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(200)]
        public string ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public DateTime? PublishedDate { get; set; }

        [StringLength(200)]
        public string PublishedBy { get; set; }

        public DateTime? ChangeDate { get; set; }

        public TimeSpan? ChangeTime { get; set; }

        public bool? IsSpeakerPageBreak { get; set; }

        public bool? IsDeleted { get; set; }

        public bool? IsCorrigendum { get; set; }
    }
}
