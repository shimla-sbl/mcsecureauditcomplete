﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.StaffManagement;

namespace SBL.DomainModel.Models.FormTR2
{
    [Serializable]
    public class Tr2Model
    {
        public Tr2Model()
        {
           
         
            this.Mstaff = new List<mStaff>();
            this.Values = new List<Tr2Model>();
        
        }
  
        public string StatusName { get; set; }
        public int StatusId { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public string FatherName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string DOB { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string District { get; set; }
        public string PinCode { get; set; }
        public string Photo { get; set; }

        public string FinancialYear { get; set; }
        public SelectList FinancialYearList { get; set; }
        public string FinancialMonths { get; set; }
        public SelectList FinancialMonthList { get; set; }

        public int MonthId { get; set; }
        public string Month { get; set; }
        public virtual ICollection<mStaff> Mstaff { get; set; }
        public int StaffID { get; set; }
        public string StaffName { get; set; }
        public List<string> MstaffIds { get; set; }
        public List<mBudget> BudgetHeads { get; set; }
        public int BudgetId { get; set; }
        public string BudgetName { get; set; }
        public string EmpName { get; set; }
        public string TreasuryCD { get; set; }
        public string DDOCD { get; set; }
        public string MajorHead { get; set; }
        public string SubmajorHead { get; set; }
        public string MinorHead { get; set; }
        public string SubHead { get; set; }
        public string BudgetCode { get; set; }
        public string Gazett { get; set; }
        public string Plan { get; set; }
        public string ObjectCode { get; set; }
        public string VotedCharged { get; set; }
        public string DemandNo { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<Tr2Model> Values { get; set; }
        public decimal BasicPay { get; set; }
        public string CertiText { get; set; }
        public string CertiDate { get; set; }
         public string FromDate { get; set; }
         public string ToDate { get; set; }

         public SelectList QuaterList { get; set; }
         public string Quaters { get; set; }
    }
}
