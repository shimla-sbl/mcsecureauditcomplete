﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.FormTR2
{
    [Table("TR2LoanDetail")]
    [Serializable]
   public class TR2LoanDetail
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int OpeningBalance { get; set; }
        public int MonthlyDeduction { get; set; }
        public int InterestPer { get; set; }
        public string Month { get; set; }
        public int RemainingValue { get; set; }
        public int EmpId { get; set; }
        public string FinancialYear { get; set; }
        public string LoanType { get; set; }
        public int Form16Id { get; set; }
    }
}
