﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.FormTR2
{
    [Table("tRebate")]
    [Serializable]
   public class tRebate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int EmployeeID { get; set; }
        public String RebateType { get; set; }
        public double GPF { get; set; }
        public double GI { get; set; }
        public double PLI { get; set; }
        public double ULIP { get; set; }
        public double TutionFee { get; set; }
        public double NSC { get; set; }
        public double LIC { get; set; }
        public double PPF { get; set; }
        public double StampRegistrationFees { get; set; }
        public double AnyOtherDeductionUs { get; set; }
        public double AnyOtherDeductionUs2 { get; set; }
        public double HBALoanPrincpal { get; set; }
        public double FixedDepositAbove5Year { get; set; }
        public double EquilityLinkSavingBond { get; set; }
        public double Section80CCC { get; set; }
        public double Section80CCD { get; set; }
        public double Section80E { get; set; }
        public double Section80G { get; set; }
        public double Section80GG { get; set; }
        public double Section80GGA { get; set; }
        public double Section80GGC { get; set; }
        public double Section80U { get; set; }
        public double Section80DD { get; set; }
        public double Section80D { get; set; }
        public double Section80DDB { get; set; }
        public double Section80TTA { get; set; }
        public double Section80CCG { get; set; }
        public double OtherFiel1 { get; set; }
        public double OtherFiel2 { get; set; }
        public double OtherFiel3 { get; set; }
        public double OtherFiel4 { get; set; }
        public DateTime CreatedDate { get; set; }
        public String FinancialYear { get; set; }
        public double GrandTotal { get; set; }
        [NotMapped]
        public int? AccountFID { get; set; }

        public int Form16Id { get; set; }
    }
}
