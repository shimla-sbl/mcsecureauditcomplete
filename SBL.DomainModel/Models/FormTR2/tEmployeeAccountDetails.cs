﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.FormTR2
{
    [Table("tEmployeeAccountDetails")]
    [Serializable]
    public class tEmployeeAccountDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string finanacialYear { get; set; }
        public int AccountFieldId { get; set; }
        public decimal AccountFieldValue { get; set; }
        public int? AccountMonth { get; set; }
        public int? AccountYear { get; set; }
        public decimal AdditionValue1 { get; set; }
        public decimal AdditionValue2 { get; set; }
        public decimal AdditionValue3 { get; set; }
        public DateTime AddUpdateDate1 { get; set; }
        public DateTime AddUpdateDate2 { get; set; }
        public DateTime AddUpdateDate3 { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public decimal IncrementAmt { get; set; }
        [NotMapped]
        public virtual ICollection<Tr2Model> Tr2Model { get; set; }
        [NotMapped]        
        public int BID { get; set; }
         [NotMapped]  
        public int BudgetId { get; set; }
         [NotMapped]  
        public string BudgetName { get; set; }
         [NotMapped]  
        public string EmpName { get; set; }
         [NotMapped]  
        public string TreasuryCD { get; set; }
         [NotMapped]  
        public string DDOCD { get; set; }
         [NotMapped]  
        public string MajorHead { get; set; }
         [NotMapped]  
        public string SubmajorHead { get; set; }
         [NotMapped]  
        public string MinorHead { get; set; }
         [NotMapped]  
        public string SubHead { get; set; }
         [NotMapped]  
        public string BudgetCode { get; set; }
         [NotMapped]  
        public string Gazett { get; set; }
         [NotMapped]  
        public string Plan { get; set; }
         [NotMapped]  
        public string ObjectCode { get; set; }
         [NotMapped]  
        public string VotedCharged { get; set; }
         [NotMapped]
         public string DemandNo { get; set; }
         [NotMapped]
         public virtual ICollection<Tr2Model> Values { get; set; }
         [NotMapped]
         public List<string> MstaffIds { get; set; }
         [NotMapped]
         public decimal BasicPay { get; set; }
         [NotMapped]
         public decimal SpecialPay { get; set; }
         [NotMapped]
         public decimal DearnessAllowns { get; set; }
          [NotMapped]
         public decimal CompenseAllowns { get; set; }
         [NotMapped]
         public decimal HRAAllowns { get; set; }
         [NotMapped]
         public decimal CapitalAllowns { get; set; }
         [NotMapped]
         public decimal  ConveyaceAllowns { get; set; }
        [NotMapped]
         public decimal  WashingAllowns { get; set; }
        [NotMapped]
         public decimal  GPFSubs { get; set; }
        [NotMapped]
         public decimal  GPFAdvans { get; set; }
        [NotMapped]
         public decimal  HBAInterest { get; set; }
        [NotMapped]
         public decimal  NCarScooterAdvance { get; set; }
       
         [NotMapped]
         public decimal  MCScooterInterest { get; set; }
          [NotMapped]
         public decimal  WarmclothAdvance { get; set; }
          [NotMapped]
         public decimal  WarmclothInterest { get; set; } 
         [NotMapped]
         public decimal  LTCTAAdvance { get; set; }  
         [NotMapped]
         public decimal  FestivalAdvance { get; set; }
          [NotMapped]
         public decimal  MiscelRecov { get; set; }
          [NotMapped]
         public decimal  InsurFund { get; set; }
          [NotMapped]
         public decimal  SavingFund { get; set; }
          [NotMapped]
         public decimal  HouseRent { get; set; }
         [NotMapped]
         public decimal  PLI { get; set; }
         [NotMapped]
         public decimal  LIC { get; set; }
         [NotMapped]
         public decimal  IncomeTax { get; set; }
         [NotMapped]
         public decimal  Surcharge { get; set; }
          [NotMapped]
         public decimal  OtherBT { get; set; }
          [NotMapped]
          public decimal GP { get; set; }
         [NotMapped]
         public decimal  HBA { get; set; }

         [NotMapped]
         public decimal GrossTotalHead { get; set; }

         [NotMapped]
         public decimal TotalDeducA { get; set; }

         [NotMapped]
         public decimal TotalDeducB { get; set; }

         [NotMapped]
         public decimal BalanceAmount { get; set; }
         [NotMapped]
         public decimal NetAmount { get; set; }
          [NotMapped]
         public String FilePath { get; set; }
          [NotMapped]
          public String GetFilePath { get; set; }
          [NotMapped]
          public decimal SectPay { get; set; }
          [NotMapped]
          public string ArrayVal { get; set; }
         [NotMapped]
          public Tuple<int, int> Nww { get; set; }
         [NotMapped]
         public string Quaters { get; set; }

         [NotMapped]
         public List<string> MonthsQuater { get; set; }
    }
}