﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.FormTR2
{
    [Table("mAccountFields")]
    [Serializable]
   public class mAccountFields
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
     
        public int Id { get; set; }
        public int Value { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public bool ? IsTaxable { get; set; }
        public string UnderRule { get; set; }

    }
}