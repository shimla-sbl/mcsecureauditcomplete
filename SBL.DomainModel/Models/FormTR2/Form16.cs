﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.FormTR2
{
    [Table("Form16")]
    [Serializable]
    public class Form16
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int EmpId { get; set; }
        public string FinancialYear { get; set; }

        public int TotalGrossSalary { get; set; }
        public int IncomeFromOtherSources { get; set; }
        public int Rebate { get; set; }
        public int IntersetOnHBA { get; set; }
        public int TaxableAmt { get; set; }
        public int Exampted { get; set; }

        public int CalculateTax1 { get; set; }
        public int CalculateTax2 { get; set; }
        public int CalculateTax3 { get; set; }
        public int NetTax { get; set; }
        public int Surcharge { get; set; }
        public int TotalTax { get; set; }
        public string PDFUrl { get; set; }

        [NotMapped]
        public string StaffName { get; set; }
        [NotMapped]
        public string Designation { get; set; }


    }
}
