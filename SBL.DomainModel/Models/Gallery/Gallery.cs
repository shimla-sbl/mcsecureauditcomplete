﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Gallery
{
    [Serializable]
    [Table("tGallery")]
    public class tGallery
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GalleryId { get; set; }

        [Required]
        public string GalleryTitle { get; set; }

        public string LocalGalleryTitle { get; set; }
        //[Required]
        public string GalleryDescription { get; set; }

        public string LocalGalleryDescription { get; set; }

        public byte GalleryType { get; set; }

        public string FileName { get; set; }
        public string ThumbName { get; set; }

        public string FilePath { get; set; }

        public byte Status { get; set; }

        public int CategoryID { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public DateTime StartPublish { get; set; }

        public DateTime StopPublish { get; set; }

        public int Odering { get; set; }

        public int Hits { get; set; }

        [NotMapped]
        public string CategoryName { get; set; }

        [NotMapped]
        public string ImageAcessurl { get; set; }
    }
}
