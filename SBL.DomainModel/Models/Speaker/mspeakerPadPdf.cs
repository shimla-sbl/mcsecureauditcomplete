﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Speaker
{
    [Serializable]
    [Table("mSpeakers_PadPdf")]
    public class mspeakerPadPdf
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }
        public Int32 Pdfvesion { get; set; }
        public string Pdfpath { get; set; }
        public string SessionDates { get; set; }
        public DateTime? PdfDateTime { get; set; }
        [NotMapped]
        public List<mspeakerPadPdf> SpeakerPadPdfList { get; set; }
        [NotMapped]
        public List<mspeakerPadPdf> SessionDatesList { get; set; }
        [NotMapped]
        public string AssemblyId { get; set; }
        [NotMapped]
        public string SessionId { get; set; }
        [NotMapped]
        public string IsPdf { get; set; }
    }
}
