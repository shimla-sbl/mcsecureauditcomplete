﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Speaker
{
    [Table("mVSpeaker")]
    public class mVSpeaker
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SpeakerId { get; set; }

        public string SpeakerName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool? IsDeputySpeaker { get; set; }

    }
}
