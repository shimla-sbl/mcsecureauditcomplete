﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Secretory
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("mSecretoryDepartment")]
    public partial class mSecretoryDepartment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SecretoryDepartmentID { get; set; }

        public int SecretoryID { get; set; }

        public string DepartmentID { get; set; }


        public bool? IsActive { get; set; }

        public int? OrderID { get; set; }

        public int? AssemblyID { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public bool? IsDeleted { get; set; }

        [NotMapped]
        public string GetAssemblyName { get; set; }

        [NotMapped]
        public string GetDeptName { get; set; }


        [NotMapped]
        public string GetSecretoryName { get; set; }

    }
}
