﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Event
{
    [Table("mSubEvents")]
    [Serializable]
    public class mSubEvents
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SubEventId { get; set; }

        public int? EventId { get; set; }

        public string EventName { get; set; }

        public string EventNameLocal { get; set; }

        public string CategoryCode { get; set; }

        public bool? IsLOB { get; set; }

        public bool? IsProceeding { get; set; }

        public int? OrderingID { get; set; }

        //[MaxLength(10)]
        //[Column(TypeName = "nchar")]
        public string CategoryName { get; set; }

        public bool? IsDepartment { get; set; }
        public bool? IsCommittee { get; set; }
        public bool? IsVS { get; set; }
        public int? NoCopies { get; set; }
        public bool? IsOtherNotices { get; set; }
        public bool? IsQA { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }

        
    }
}
