﻿namespace SBL.DomainModel.Models.Event
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    #endregion

    [Table("mEvent")]
    [Serializable]
    public partial class mEvent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventId { get; set; }

        [Required]
        [StringLength(500)]
        public string EventName { get; set; }

        [Required]
        [StringLength(500)]
        public string EventNameLocal { get; set; }

        public int? PaperCategoryTypeId { get; set; }

        public bool? IsLOB { get; set; }

        public bool? IsDepartment { get; set; }

        public bool? IsCommittee { get; set; }

        public bool? IsProceeding { get; set; }

        public bool? IsMember { get; set; }
        public bool? IsSearch { get; set; }
        public int OrderingID { get; set; }

        [StringLength(250)]
        public string PdfPath { get; set; }

        [Column(TypeName = "date")]
        public DateTime? AddedDate { get; set; }

        public string ModifiedBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ModifiedWhen { get; set; }


        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public bool? Active { get; set; }

        public bool? IsDeleted { get; set; }

        [NotMapped]
        public List<mEvent> EventList { get; set; }

        //New Field added to show the RuleNo in case of Notice 

        public string RuleNo { get; set; }

        [NotMapped]
        public string GetPapCatName { get; set; }

        public bool? IsTyipstReqired { get; set; }

        //new field added to order rules       BY DURGESH 20-MAY-2015
        public int? ruleOrderNumber { get; set; }

    }
}
