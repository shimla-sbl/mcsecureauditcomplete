﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.ActionButtons
{
    [Table("ActionButtonControl")]
    [Serializable]
    public partial class ActionButtonControl
    {
        public ActionButtonControl()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ActionButtonControlId { get; set; }

        public string UserId { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Area { get; set; }
        public string ButtonCaption { get; set; }
        public string Status { get; set; }
        public bool IsValid { get; set; }
    }
}
