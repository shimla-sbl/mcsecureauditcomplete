﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.JobsPosting
{
    [Table("Job")]
    [Serializable]
    public class Job
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int JOBID { get; set; }
        public string JobTitle { get; set; }
        public DateTime PublishedOn { get; set; }
        public string JobDetailPdfUrl { get; set; }
        public DateTime LastDate { get; set; }
        public string SuppInfoPdfUrl { get; set; }
        public string ResultPdfUrl { get; set; }
        public bool IsActive { get; set; }
        public bool ConfirmationForApply { get; set; }
        public string LastDateS { get; set; }
        public string PostingDateS { get; set; }
        public string PostIds { get; set; }
        [NotMapped]
        public List<string> PostIdList { get; set; }
        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string Type { get; set; }
        [NotMapped]
        public string Action { get; set; }
        [NotMapped]
        public int Length { get; set; }
    }
}
