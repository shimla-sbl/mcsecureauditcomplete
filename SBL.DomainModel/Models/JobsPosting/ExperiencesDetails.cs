﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.JobsPosting
{

    
	[Serializable]
	[Table("ExperiencesDetails")]
	public partial class ExperiencesDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int PostId { get; set; }
        public string ExamPassed { get; set; }
        public string PassingYear { get; set; }
        public string Percentage { get; set; }
        public string Board { get; set; }
    }
}
