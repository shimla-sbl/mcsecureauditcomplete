﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.JobsPosting
{
    [Table("Candidates")]
    [Serializable]
    public class Candidates
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CandidatesID { get; set; }
        public int JobId { get; set; }
        public int postId { get; set; }
        public string AadharId { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public DateTime DOB { get; set; }
        public string Mobile { get; set; }
        public string EmailId { get; set; }
        public string Category { get; set; }
        public string Photo { get; set; }
        public string PermanetAdd { get; set; }
        public string CorresAdd { get; set; }
        public string DocumentPdf { get; set; }
        public string PayMentMode { get; set; }
        public string DraftNo { get; set; }
        public string DraftDate { get; set; }
        public decimal DraftAmount { get; set; }
        public string OtherInformation { get; set; }
        public string SingImage { get; set; }
        public bool IsApprove { get; set; }
        public bool IsDeclare { get; set; }
        public bool IsAdmitCardDetailsEntered { get; set; }
        public bool IsAdmitCardDetailsLocked { get; set; }
        public string Reason { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime ApporvedOn { get; set; }
        [NotMapped]
        public string JobName { get; set; }
        [NotMapped]
        public string PostName { get; set; }
        public bool IsRegisterBarCouncil { get; set; }
        public bool IsRegisterBarAsc { get; set; }
        public string EnrollmentNumber { get; set; }
        public DateTime? EnrollmentDate { get; set; }
        public string TotalExperience { get; set; }
        public bool IsTranslationExp { get; set; }
    }
}
