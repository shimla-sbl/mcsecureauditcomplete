﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.JobsPosting
{
    [Serializable]
    [Table("ExamDetails")]
    public partial class ExamDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int PostId { get; set; }
        public string OfficeName { get; set; }
        public string Designation { get; set; }
        public string ServiceFrom { get; set; }
        public string ServiceTo { get; set; }
        public string PayScale { get; set; }
        public string CurrentPayScale { get; set; }


    }
}
