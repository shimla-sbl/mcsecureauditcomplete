﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.JobsPosting
{
    [Serializable]
    [Table("tCandidateAdmitDetails")]
    public partial class tCandidateAdmitDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PID { get; set; }
        public int CandidateID { get; set; }
        public string RollNo { get; set; }
        public string ExamDate { get; set; }
        public string ExamVenue { get; set; }
        public string ExamTime { get; set; }
        public bool IsAdmit { get; set; }    
      
    }

    //public partial class ViewmodelCandidateAdmitDetail
    //{        
    //    public int PID { get; set; }
    //    public int CandidateID { get; set; }
    //    public string RollNo { get; set; }
    //    public string ExamDate { get; set; }
    //    public string ExamVenue { get; set; }
    //    public string ExamTime { get; set; }
    //    public bool IsAdmit { get; set; }
    //}
}
