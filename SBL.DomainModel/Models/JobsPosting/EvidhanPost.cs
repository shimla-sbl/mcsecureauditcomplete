﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.JobsPosting
{
    [Table("EvidhanPost")]
    [Serializable]
    public class EvidhanPost    
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        
        public int PostId { get; set; }
        public string PostName { get; set; }
        public bool PostStatus { get; set; }
        [NotMapped]
        public string Mode { get; set; }
    }
}
