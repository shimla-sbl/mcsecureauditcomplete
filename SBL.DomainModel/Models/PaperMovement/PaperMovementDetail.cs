﻿namespace SBL.DomainModel.Models.PaperMovement
{
    #region Namespace References
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Ministery;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.Event;
    using SBL.DomainModel.Models.Document;
    using SBL.DomainModel.Models.Bill;
    using SBL.DomainModel.Models.Question;
    using SBL.DomainModel.Models.Notice;
    using System.Web.Mvc;
    using SBL.DomainModel.ComplexModel;
    using System.ComponentModel;
    using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
    using Lib.Web.Mvc.JQuery.JqGrid;
    #endregion 

    [Table("tPaperMovementDetail")]
    [Serializable]
    public partial class PaperMovementDetail
    {
        public PaperMovementDetail()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long MovementDetailId { get; set; }
       
        public int AssemblyId { get; set; }
        
        public int SessionId { get; set; }

        [StringLength(50)]
        public string AadarId { get; set; }

        [StringLength(50)]
        public string Number { get; set; }
       
        public int? BussinessTypeId { get; set; }

        public int? PaperCategoryTypeId { get; set; }

        public string DepartmentId { get; set; }

        [StringLength(50)]
        public string MovementUser { get; set; }
        
        public DateTime? MovementDate { get; set; }

        public bool? IsActive { get; set; }

        public int? EmpCode { get; set; }     

       


    }
}
