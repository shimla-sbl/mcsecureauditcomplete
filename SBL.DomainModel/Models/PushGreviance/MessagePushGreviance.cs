﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.pushgreviance
{
    [Table("MessagePushGreviance")]
    [Serializable]
    public class MessagePushGreviance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string UserCode { get; set; }
        public string Message { get; set; }
        public string MobileNo { get; set; }
        public string MemberId { get; set; }
        public DateTime? DateTime { get; set; }
    }
}
