﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.StaffManagement
{
    [Serializable]
    [Table("mStaff")]
    public class mStaff
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StaffID { get; set; }

        [Display(Name = "Name")]
        [Required]
        public string StaffName { get; set; }

        [Display(Name = "Designation")]
        [Required]
        public string Designation { get; set; }

        [Display(Name = "isActive")]
        [Required]
        [DefaultValue(true)]
        public bool isActive { get; set; }
        [Display(Name = "isNominal")]
        [Required]
        [DefaultValue(true)]
        public bool isNominal { get; set; }

        [DefaultValue(true)]
        [Display(Name = "isAlive")]
        [Required]
        public bool isAlive { get; set; }

        [Display(Name = "Group")]
        [Required]
        public string Group { get; set; }


        [Display(Name = "Class")]
        [Required]
        public int Classes { get; set; }

        [Display(Name = "Aadhar ID")]
        //  [StringLength(12, MinimumLength = 12, ErrorMessage = "Please Enter a Valid 12 digit AAdhar ID")]
        public string AadharID { get; set; }

        [Display(Name = "Bank")]
        public string BankName { get; set; }

        [Display(Name = "Branch")]
        public string Branch { get; set; }

        [Display(Name = "Account")]
        public string BankAccountNo { get; set; }

        [Display(Name = "Mobile")]
        public string MobileNo { get; set; }


        [Display(Name = "IFSC")]
        public string IFSC { get; set; }

        public bool IsReject { get; set; }

        [Display(Name = "Month")]
        public int monthID { get; set; }
        [Display(Name = "Year")]
        public int yearID { get; set; }
        [Display(Name = "Basic Pay")]
        [DefaultValue("0")]
        public double? Basic { get; set; }
        [Display(Name = "Grade Pay")]
        public double? gradePay { get; set; }
        [Display(Name = "Sectt. Pay")]
        public double? SecttPay { get; set; }
        [Display(Name = "Spl. Pay")]
        public double? SplPay { get; set; }
        [Display(Name = "HRA")]
        public double? hra { get; set; }
        [Display(Name = "Cony. Allow.")]
        public double? ConyAllow { get; set; }
        [Display(Name = "Wash. Allow.")]
        public double? washAllow { get; set; }
        [Display(Name = "FPA")]
        public double? fpa { get; set; }
        public string GPFCPF { get; set; }



        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public string NameWithDesignation { get; set; }

        [NotMapped]
        public IEnumerable<mStaffNominee> mStaffNominee { get; set; }

        [NotMapped]
        public IEnumerable<mStaff> mStaffList { get; set; }

        //sanjay-Added

        [Display(Name = "Father Name")]
        public string FatherName { get; set; }

        [Display(Name = "Spouse Name")]
        public string SpouseName { get; set; }

        [Display(Name = "Category")]
        public string Category { get; set; }



        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }


        [Display(Name = "Nationality")]
        public string Nationality { get; set; }


        [Display(Name = "Qualification")]
        public string EducationalQualification { get; set; }


        [Display(Name = "District")]
        public string District { get; set; }

        [Display(Name = "Pin Code")]
        public int PinCode { get; set; }

        [Display(Name = "State")]
        public string State { get; set; }

        [Display(Name = "PAN No")]
        public string PANNo { get; set; }


        [Display(Name = "Identifi.. Mark")]
        public string IdentificationMark { get; set; }

        [Display(Name = "Height")]
        public string Height { get; set; }

        [Display(Name = "Landline No")]
        public string LandlineNo { get; set; }


        [Display(Name = "Gender")]
        public string Gender { get; set; }


        [Display(Name = "Email Id")]
        public string EmailId { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }


        [Display(Name = "Date of Birth")]
        public DateTime? DOB { get; set; }

        [Display(Name = "Date of Joining")]
        public DateTime? DOJ { get; set; }


        [Display(Name = "Date of Superannuation")]
        public int DateofSuperannuation { get; set; }


        [Display(Name = "Photo")]
        public string Photo { get; set; }

        [Display(Name = "Employee Code")]
        public string EmployeeCode { get; set; }

        [Display(Name = "DDO Code")]
        public string DDOCode { get; set; }

        [Display(Name = "ID-Card No")]
        public string IDCardNo { get; set; }

        [Display(Name = "e-Salary Id")]
        public string eSalaryId { get; set; }

        [Display(Name = "Rate of Increment")]
        public decimal RateofIncrement { get; set; }

        public string RejectedBy { get; set; }

        public string DeactivatedBy { get; set; }
        [Display(Name = "PPO Number")]
        public string PPONum { get; set; }

        //New Requirement For Committee dated 25/05/2016
        public string BranchIDs { get; set; }
    }

    [Serializable]
    [Table("mStaffNominee")]
    public class mStaffNominee
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NomineeID { get; set; }

        public int StaffID { get; set; }

        [Display(Name = "Nominee Name")]
        [Required]
        public string NomineeName { get; set; }

        [Display(Name = "Relationship")]
        [Required]
        public string Relatioanship { get; set; }


        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [Display(Name = "Branch")]
        public string Branch { get; set; }

        [Display(Name = "Bank Account No.")]
        public string BankAccountNo { get; set; }


        [Display(Name = "IFSC")]
        public string IFSC { get; set; }

        [Display(Name = "isActive")]
        [Required]
        [DefaultValue(true)]
        public bool isActive { get; set; }

        [NotMapped]
        public string Mode { get; set; }

        public string RejectedBy { get; set; }
    }

    [Serializable]
    [Table("mStaffDesignation")]
    public class mStaffDesignation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DesignationID { get; set; }

        public int DesignationCode { get; set; }

        public string designation { get; set; }

        public int post { get; set; }

        public string text { get; set; }

        public int orderNO { get; set; }

        public int? Basic { get; set; }

        public int? gradePay { get; set; }

        public int? SecttPay { get; set; }

        public int? SplPay { get; set; }

        [NotMapped]
        public string Mode { get; set; }



    }
}
