﻿namespace SBL.DomainModel.Models.Ministery
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    #endregion

    [Serializable]
    [Table("mMinistryDepartments")]
    public partial class mMinistryDepartment
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MinistryDepartmentsID { get; set; }

        //[Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AssemblyID { get; set; }

        //[Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MinistryID { get; set; }

        [StringLength(25)]
        public string DeptID { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }

        public int? OrderID { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }


        [NotMapped]
        public string GetAssemblyName { get; set; }

        [NotMapped]
        public string GetDeptName { get; set; }

       
        [NotMapped]
        public string GetMinistryName { get; set; }
    }
}
