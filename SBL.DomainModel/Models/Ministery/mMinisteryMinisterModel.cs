﻿using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.Ministery
{
    [Serializable]
    public class mMinisteryMinisterModel
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MinsitryMinistersID { get; set; }

        public int AssemblyID { get; set; }

        public int EventId { get; set; }

        public int MinistryID { get; set; }

        public string MinisterName { get; set; }

      
        public string PaperTypeName { get; set; }

        public string EventName { get; set; }

        public string MinisterNameLocal { get; set; }

        public int MemberCode { get; set; }
        public string MemberName { get; set; }

        public DateTime MemberStartDate { get; set; }

        public DateTime MemberEndDate { get; set; }

        public bool? IsActive { get; set; }

        public int? OrderID { get; set; }

        public string ModifiedBy { get; set; }

        public long? PaperLaidIdTemp { get; set; }
        public long? MinisterActivePaperId { get; set; }

        public int? PaperTypeID { get; set; }

        public int? CommitteeId { get; set; }

        public long? PaperLaidTempId { get; set; }
        public int MinisterSubmittedBy { get; set; }

        public DateTime? MinisterSubmittedDate { get; set; }

        public string CommitteeName { get; set; }

        public int? CommitteeChairmanMemberId { get; set; }

        public string Title { get; set; }
        public long? PaperLaidId { get; set; }
        public string DeparmentName { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }

        public int SessionId { get; set; }
        public string ProvisionUnderWhich { get; set; }
        public int PaperCategoryTypeId { get; set; }

        public int CategoryTypeId { get; set; }
        public string PaperCategoryTypeName { get; set; }


        public string BillNumber { get; set; }
        public string Number { get; set; }

        public int? QuesNumber { get; set; }

        public DateTime? ModifiedWhen { get; set; }
        public string MinistryName { get; set; }
        public string MinisterMinistryName { get; set; }

        public string FilePath { get; set; }
        public long? DeptActivePaperId { get; set; }

       [JqGridColumnFormatter("$.FixedDateOnFormatter")]
        public DateTime? DesireLayingDate { get; set; }

        public string Message { get; set; }

        public string FileName { get; set; }



        public string SignedFilePath { get; set; }
        public string UploadedFileName { get; set; }

        public int? Version { get; set; }


        [NotMapped]
        public string CurrentUserName { get; set; }

        [NotMapped]
        public string UserDesignation { get; set; }

        public int AssemblyCode { get; set; }
        [Required(ErrorMessage = "Please Select Assembly")]
        public int AssemblyId { get; set; }
        public int SessionCode { get; set; }
        [NotMapped]
        public string SessionName { get; set; }
        [NotMapped]
        public string AssesmblyName { get; set; }
        public int ComInboxCount { get; set; }
        public int ComSentCount { get; set; }
        public string ActionType { get; set; }
        [NotMapped]
        public IList<tPaperLaidV> tPaperLaidV { get; set; }
        public virtual ICollection<mMinisteryMinisterModel> ComModel { get; set; }
        [NotMapped]
        public IList<mMinisteryMinisterModel> mMinisteryMinisterModelList { get; set; }

        public virtual ICollection<mAssembly> AssemList { get; set; }
        public virtual ICollection<mSession> SessList { get; set; }
        public virtual ICollection<mEvent> EventList { get; set; }
        //Paging Properties
        [NotMapped]
        public int PAGE_SIZE { get; set; }
        [NotMapped]
        public int PageIndex { get; set; }
        [NotMapped]
        public int ResultCount { get; set; }
        [NotMapped]
        public int loopStart { get; set; }
        [NotMapped]
        public int loopEnd { get; set; }
        [NotMapped]
        public int selectedPage { get; set; }
        [NotMapped]
        public int PageNumber { get; set; }
        [NotMapped]
        public int RowsPerPage { get; set; }
        [NotMapped]
        public int TotalStaredReceived { get; set; }
        [NotMapped]
        public int MinInboxCount { get; set; }
        [NotMapped]
        public int MinSentCount { get; set; }

        [NotMapped]
        public string ForSave { get; set; }


        public string LoginId { get; set; }

        [NotMapped]
        public string HashKey { get; set; }

     
        public DateTime? DeptSubmittedDate { get; set; }


       
        public string OriDiaryFilePath { get; set; }

      
        public string OriDiaryFileName { get; set; }

        public string NoticePdfPath { get; set; }

        public bool? IsFinalApprove { get; set; }

        public string SupFilePath { get; set; }

        public string SupFileName { get; set; }

       
        public string SupDocFileName { get; set; }

        public string DocFileName { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? SupFileVersion { get; set; }

    }
}