namespace SBL.DomainModel.Models.Ministery
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    #endregion

    [Table("mMinistry")]
    [Serializable]
    public partial class mMinistry
    {
        //[Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AssemblyID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MinistryID { get; set; }

        [Required]
        [StringLength(500)]
        public string MinistryName { get; set; }

        [StringLength(500)]
        public string MinistryNameLocal { get; set; }

        [StringLength(250)]
        public string MinisterName { get; set; }

        [StringLength(250)]
        public string MinisterNameLocal { get; set; }

        //[Required]
        public int MemberCode { get; set; }

        public DateTime? MemberStartDate { get; set; }

        public DateTime? MemberEndDate { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }

        public int? OrderID { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string MinisterPrefix { get; set; }

        public string MinistryPrefix { get; set; }

        [NotMapped]
        public string GetAssemblyName { get; set; }

        [NotMapped]
        public string GetMemberName { get; set; }

        //Created as per as requirement came on 31st January 2016
        public string AdditionalMinistry { get; set; }



    }
}
