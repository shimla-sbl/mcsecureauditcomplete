﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.MemberSMSQuota
{
    [Serializable]
    [Table("mMemberSMSQuotaAddOn")]
    public class mMemberSMSQuotaAddOn
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int MemberCode { get; set; }

        public int AddonValue { get; set; }

        public DateTime? AddonDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }

        [NotMapped]
        public string GetMemberName { get; set; }
    }
}
