﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace SBL.DomainModel.Models.eFile
{
    [Serializable]
    [Table("meTreeViewStructure")]
    public class TreeViewStructure
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TreeViewID { get; set; }
        public string AadharID { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string Parent_AadharID { get; set; }
        public string Parent_Name { get; set; }
        public string Parent_Designation { get; set; }
        public string Description { get; set; }
    }

}
