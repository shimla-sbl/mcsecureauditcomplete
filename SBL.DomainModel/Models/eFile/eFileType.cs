﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.eFile
{
	[Table("meFileType")]
	[Serializable]
	public class eFileType
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int eTypeID { get; set; }
		public string eType { get; set; }
		public int IsDeleted { get; set; }
	}
}
