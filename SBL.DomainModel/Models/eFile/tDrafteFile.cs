﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.eFile
{
    [Serializable]
    [Table("tDrafteFile")]
    public class tDrafteFile
    {
        public tDrafteFile()
		{            
		}
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int eFileAttachmentId { get; set; }
        public int eFileID { get; set; }
        public int DocumentTypeId { get; set; }
        public string eFileName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DraftBy { get; set; }
        public DateTime DraftDate { get; set; }
        public bool SendStatus { get; set; }
        public string DepartmentId { get; set; }
        public string Title { get; set; }
        public Guid eFileNameId { get; set; }
        public string Type { get; set; }
        public int ParentId { get; set; }
        public int SplitFileCount { get; set; }
        public int ReportLayingHouse { get; set; }
        public int Approve { get; set; }
        public int OfficeCode { get; set; }
        public int PaperNature { get; set; }
        public string PaperNatureDays { get; set; }
        public string ToDepartmentID { get; set; }
        public int ToOfficeCode { get; set; }
        public string PaperStatus { get; set; }
        public DateTime SendDate { get; set; }
        public string PType { get; set; }
        public string eFilePath { get; set; }      
        public DateTime ReceivedDate { get; set; }
        public string PaperRefNo { get; set; }
        public string ReplyStatus { get; set; }
        public DateTime ReplyDate { get; set; }     
        public string LinkedRefNo { get; set; }
        public int ToPaperNature { get; set; }
        public string ToPaperNatureDays { get; set; }
        public string RevedOption { get; set; }
        public string RecvDetails { get; set; }
        public string ToRecvDetails { get; set; }
        public int eMode { get; set; }               
        public string PaperTicketValue { get; set; }
        public string PaperNo { get; set; }
        public string DispatchtoWhom { get; set; }
        public bool IsDeleted { get; set; }       
        public string ToDepartmentName { get; set; }
        public string eFilePathWord { get; set; }
        public int ReFileID { get; set; }       
        public int CurYear { get; set; }
        public int CurDNo { get; set; }
        public int CurrentBranchId { get; set; }
        public string eFilePathPDF { get; set; }
        public string eFilePathDOC { get; set; }
        public string TODepts { get; set; }
        public string CCDepts { get; set; }
        public string TOMembers { get; set; }
        public string CCMembers { get; set; }
        public string ToOthers { get; set; }
        public string CCOthers { get; set; }
        public string ToAllRecipName { get; set; }
        public string CCAllRecipName { get; set; }
        public string ToAllRecipIds { get; set; }
        public string CCAllRecipIds { get; set; }
        public string CurrentEmpAadharID { get; set; }
        public DateTime? AssignDateTime { get; set; }
        public bool? isDraft { get; set; }
        public int? CountAPS { get; set; }
        public int ItemID { get; set; }
        public String ItemName { get; set; }
        public int DraftId { get; set; }
        public string PaperRefrenceManualy { get; set; }
        //[UIHint("tinymce_jquery_full_LOB"), AllowHtml]
        //public string PaperDraft { get; set; }
        public String AnnexPdf { get; set; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
        [NotMapped]
        public int FileAttched_DraftId { get; set; }
    }

    [Serializable]
    [Table("tDrafteFile_Data")]
    public class tDrafteFile_Data
    {
        public tDrafteFile_Data()
        {
        }
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int eFileAttachmentId { get; set; }
        public int eFileID { get; set; }
        public int DocumentTypeId { get; set; }
        public string eFileName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DraftBy { get; set; }
        public DateTime DraftDate { get; set; }
        public bool SendStatus { get; set; }
        public string DepartmentId { get; set; }
        public string Title { get; set; }
        public Guid eFileNameId { get; set; }
        public string Type { get; set; }
        public int ParentId { get; set; }
        public int SplitFileCount { get; set; }
        public int ReportLayingHouse { get; set; }
        public int Approve { get; set; }
        public int OfficeCode { get; set; }
        public int PaperNature { get; set; }
        public string PaperNatureDays { get; set; }
        public string ToDepartmentID { get; set; }
        public int ToOfficeCode { get; set; }
        public string PaperStatus { get; set; }
        public DateTime SendDate { get; set; }
        public string PType { get; set; }
        public string eFilePath { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string PaperRefNo { get; set; }
        public string ReplyStatus { get; set; }
        public DateTime ReplyDate { get; set; }
        public string LinkedRefNo { get; set; }
        public int ToPaperNature { get; set; }
        public string ToPaperNatureDays { get; set; }
        public string RevedOption { get; set; }
        public string RecvDetails { get; set; }
        public string ToRecvDetails { get; set; }
        public int eMode { get; set; }
        public string PaperTicketValue { get; set; }
        public string PaperNo { get; set; }
        public string DispatchtoWhom { get; set; }
        public bool IsDeleted { get; set; }
        public string ToDepartmentName { get; set; }
        public string eFilePathWord { get; set; }
        public int ReFileID { get; set; }
        public int CurYear { get; set; }
        public int CurDNo { get; set; }
        public int CurrentBranchId { get; set; }
        public string eFilePathPDF { get; set; }
        public string eFilePathDOC { get; set; }
        public string TODepts { get; set; }
        public string CCDepts { get; set; }
        public string TOMembers { get; set; }
        public string CCMembers { get; set; }
        public string ToOthers { get; set; }
        public string CCOthers { get; set; }
        public string ToAllRecipName { get; set; }
        public string CCAllRecipName { get; set; }
        public string ToAllRecipIds { get; set; }
        public string CCAllRecipIds { get; set; }
        public string CurrentEmpAadharID { get; set; }
        public DateTime? AssignDateTime { get; set; }
        public bool? isDraft { get; set; }
        public int? CountAPS { get; set; }
        public int ItemID { get; set; }
        public String ItemName { get; set; }
        public int DraftId { get; set; }
        public string PaperRefrenceManualy { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string PaperDraft { get; set; }
        public String AnnexPdf { get; set; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
    }
}
