﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Committee;

namespace SBL.DomainModel.Models.eFile
{
	[Table("tCommitteeProceeding")]
	[Serializable]
	public class COmmitteeProceeding
	{
        public COmmitteeProceeding()
        {
           // this.DepartmentList = new List<mDepartment>();
            this.CommitteeList = new List<CommitteesListView>();
        }
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ProID { get; set; }
		//public int JournlistID { get; set; }
       // public Int64  Aadhar_ID { get; set; }
        //public Int64 _AadharId { get; set; }
        public string Aadharid { get; set; }
		public string ProFileNamePdf { get; set; }
		public string ProFilePathPdf { get; set; }
		public string ProFileNameWord { get; set; }
		public string ProFilePathWord { get; set; }
		public string Remark { get; set; }		 
		public DateTime CreatedDate { get; set; }
		public bool IsDeleted { get; set; }
        //public string DeptID { get; set; }
		public int OfficeCode { get; set; }
		public int CommitteeId { get; set; }
        public bool? IsActive { get; set; }
        public string ForwardBy { get; set; }		 
        public DateTime? ForwardDate { get; set; }
		[NotMapped]
		public string CommitteeName { get; set; }
        //[NotMapped]
        //public List<mDepartment> DepartmentList { get; set; }
        [NotMapped]
        public List<CommitteesListView> CommitteeList { get; set; }
        [NotMapped]
        public string DepartmentId { get; set; }
        [NotMapped]
        public bool IsReporter { get; set; }
        [NotMapped]
        public List<COmmitteeProceeding> My_ComProcList { get; set; }
        [NotMapped]
        public string IsPdf { get; set; }
        [NotMapped]
        public string IsDoc { get; set; }
        [NotMapped]
        public string UploadedBy { get; set; }
        [NotMapped]
        public string UploadByDesignation { get; set; }
        [NotMapped]
        public int[] ProcIds { get; set; }
        [NotMapped]
        public string IpAddress { get; set; } 
        [NotMapped]
        public string MacAddress { get; set; }
        [NotMapped]
        public string UserID { get; set; }
        [NotMapped]
        public string CurrentDesignation { get; set; }
        [NotMapped]
        public string CreateBy { get; set; }


	}
}
