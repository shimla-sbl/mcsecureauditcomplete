﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Committee;
using System.Web.Mvc;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.CommitteeReport;

namespace SBL.DomainModel.Models.eFile
{
    [Serializable]
    [Table("pHouseCommitteeFiles")]
    public class pHouseCommitteeFiles
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        //public int eFileAttachmentId { get; set; }
        
        public int eFileID { get; set; }
        public string eFileName { get; set; }
        public int? DocumentTypeId { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Description { get; set; }
        public string Title { get; set; }
        public bool SendStatus { get; set; }
        public string ByDepartmentId { get; set; }
        public string ToDepartmentID { get; set; }
        public int? ParentId { get; set; }
        public int? ByOfficeCode { get; set; }
        public int? ToOfficeCode { get; set; }
        public int? PaperNature { get; set; }
        public DateTime? SendDateToDept { get; set; }
        public DateTime? ReceivedDateByDept { get; set; }
        public string ItemName { get; set; }
        public int? ItemId { get; set; }
        public int? CommitteeId { get; set; }
        public string CommitteeName { get; set; }
        public string eFilePath { get; set; }
        public string ReplyPdfPath { get; set; }
        public string AnnexPdfPath { get; set; }
        public string AnnexFilesNameByUser { get; set; }
        public string DocFilePath { get; set; }
        public string DocFilesNameByUser { get; set; }
        public string PaperRefNo { get; set; }
        public string LinkedRefNo { get; set; }
        public bool IsDeleted { get; set; }
        public int? CurYear { get; set; }
        public int? CurrentBranchId { get; set; }
        public string CurrentEmpAadharID { get; set; }
        public string SelectedPaperDate { get; set; }
        public int? LetterStatusType { get; set; }
        public Guid? CreatedBy { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy HH:mm:ss}")]
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }

        public bool? IsVerified { get; set; }
        public string RejectReason { get; set; }
        public string VerifiedBy { get; set; }



        public DateTime? ModifiedDate { get; set; }
        [NotMapped]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string DescriptionRO { get; set; }
        [NotMapped]
        public List<tCommittee> tCommitteeList { get; set; }
        [NotMapped]
        public int RecordId { get; set; }
        [NotMapped]
        public string FileAccessingUrlPath { get; set; }
        [NotMapped]
        public string ToDepartmentNameAbbrev { get; set; }
        [NotMapped]
        public string CommitteeNameAbbrev { get; set; }  
        
        [NotMapped]
        public string eFileNumber { get; set; }
        [NotMapped]
        public string eFileFromYear { get; set; }
        [NotMapped]
        public string eFileToYear { get; set; }
        [NotMapped]
        public string eFileSubject { get; set; }
        [NotMapped]
        public int? PreviousLinkId { get; set; }
        [NotMapped]
        public int? FutureLinkId { get; set; }
        [NotMapped]
        public string PreviousFileNumber { get; set; }
        [NotMapped]
        public string FutureFileNumber { get; set; }
        [NotMapped]
        public List<pHouseCommitteeFiles> HouseCommAllFilesList { get; set; }
        [NotMapped]
        public string CurrentDeptId { get; set; }
        [NotMapped]
        public string ToDepartmentName { get; set; }
        [NotMapped]
        public string ByDepartmentName { get; set; }
        [NotMapped]
        public List<tCommittee> CommitteeList = new List<tCommittee>();
        [NotMapped]
        public List<eFileList> eFileLists = new List<eFileList>();
        [NotMapped]
        public List<mDepartment> DepartmentList = new List<mDepartment>();
        [NotMapped]
        public List<eFilePaperType> eFilePaperTypeList = new List<eFilePaperType>();
        [NotMapped]
        public List<mCommitteeReplyItemType> mCommitteeReplyItemTypeList = new List<mCommitteeReplyItemType>();
        [NotMapped]
        public int AssemblyID { get; set; }
        [NotMapped]
        public int? SessionID { get; set; }
        [NotMapped]
        public string PendencyIDs { get; set; }
    }
}
