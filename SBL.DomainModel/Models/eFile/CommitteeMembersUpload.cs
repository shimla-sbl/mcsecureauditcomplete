﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.eFile
{
	[Table("meCommitteeMembersUpload")]
	[Serializable]
	public class CommitteeMembersUpload
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CommUploadID { get; set; }
		public string CommFileName { get; set; }
		public string CommFilePath { get; set; }	
		public string Remark { get; set; }
		public int CommStatus { get; set; }	
		public Guid CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedDate { get; set; }				
		public bool IsDeleted { get; set; }
		public int CommitteeId { get; set; }
	}
}

