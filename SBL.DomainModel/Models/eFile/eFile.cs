﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SBL.DomainModel.Models.Committee;

namespace SBL.DomainModel.Models.eFile
{
	[Serializable]
	[Table("meFile")]
	public class eFile
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int eFileID { get; set; }
		public string eFileNumber { get; set; }
		public string eFileSubject { get; set; }
		public string DepartmentId { get; set; }
		public bool Status { get; set; }
		public string OldDescription { get; set; }
		 
		public string NewDescription { get; set; }
		public int CommitteeId { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedDate { get; set; }
		public string FromYear { get; set; }
		public string ToYear { get; set; }
		public int Officecode { get; set; }
		[NotMapped]
		public string OfficeName { get; set; }
		[NotMapped]
		public string DepartmentName { get; set; }
		public bool IsDeleted { get; set; }
		[NotMapped]
		public string DeptAbbr { get; set; }
		public string AadharID { get; set; }
		public int eFileTypeID { get; set; }
        [NotMapped]
        public int BranchId { get; set; }
        public string SelectedDeptId { get; set; }
        public int? SelectedBranchId { get; set; }
        public int? FileAttched_DraftId { get; set; }
        //New requirement
        public int? PreviousLinkId { get; set; }
        public int? FutureLinkId { get; set; }
	}

	[Serializable]
	public class eFileList
	{
		public string Assembly { get; set; }
		public string Session { get; set; }
		public string CommitteeType { get; set; }
		public string Committee { get; set; }
		public int eFileAttachmentId { get; set; }
		public int eFileID { get; set; }
		public string eFileNumber { get; set; }
		public string eFileSubject { get; set; }
		public string Department { get; set; }
		public bool Status { get; set; }
		public string CreatedBy { get; set; }
		public DateTime CreateDate { get; set; }
		public string Remark { get; set; }
		public List<eFileAttachment> eAttachments { get; set; }
		public string olddesc { get; set; }
		public string newdesc { get; set; }
		public int CommitteeId { get; set; }
		public string DepartmentId { get; set; }
		public string DeptAbbr { get; set; }
        public string CommitteeAbbr { get; set; }
		public string FromYear { get; set; }
		public string ToYear { get; set; }
		public string eFileTypeName { get; set; }
        public int? FileAttched_DraftId { get; set; }
        //For New eFile
        public string SelectedDeptId { get; set; }
        public int eFileTypeId { get; set; }
        public int? PreviousLinkId { get; set; }
        public int? FutureLinkId { get; set; }
        public string PreviousFileNumber { get; set; }
        public string FutureFileNumber { get; set; }
	}

	[Serializable]
	public class eFileListAll
	{
		public List<SBL.DomainModel.Models.Department.mDepartment> Departments { get; set; }
		public List<eFileList> eFileLists { get; set; }
		public List<CommitteesListView> CommitteeView { get; set; }
        public List<mBranches> AllBrancheList { get; set; }
        public int BranchId { get; set; }
	}

	[Serializable]
	public class eFileSearch
	{
		public string Department { get; set; }
		public string eFileNo { get; set; }
		public string Subject { get; set; }
		public int CommitteeId { get; set; }
		public bool Status { get; set; }
	}
}
