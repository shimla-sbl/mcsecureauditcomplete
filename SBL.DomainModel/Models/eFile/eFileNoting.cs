﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.eFile
{
	[Table("teFileNoting")]
	[Serializable]
	public class eFileNoting
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int eNotingID { get; set; }
		public int eFileID { get; set; }
		public string eNotingFileName { get; set; }
		public string Remark { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedDate { get; set; }
		public int NotingStatus { get; set; }
		public string FilePath { get; set; }
		public int FileUploadSeq { get; set; }
		public bool IsDeleted { get; set; }

	}
}
