﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.CommitteeReport;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Committee;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.eFile
{
	[Table("teFileAttachment")]
	[Serializable]
	public class eFileAttachment
	{
		public int eFileAttachmentId { get; set; }
		public int eFileID { get; set; }
		public int DocumentTypeId { get; set; }
		public string eFileName { get; set; }
		public string Description { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedDate { get; set; }
		public bool SendStatus { get; set; }
		public string DepartmentId { get; set; }
        [NotMapped]
        public string AllDeptName { get; set; }
		public string Title { get; set; }
		public Guid eFileNameId { get; set; }
		public string Type { get; set; }
		public int ParentId { get; set; }
		public int SplitFileCount { get; set; }
		public int ReportLayingHouse { get; set; }
		public int Approve { get; set; }
		[NotMapped]
		public string mode { get; set; }
		[NotMapped]
		public List<int> ImageCount { get; set; }
		public int OfficeCode { get; set; }
		public int PaperNature { get; set; }
		public string PaperNatureDays { get; set; }
		public string ToDepartmentID { get; set; }
		public int ToOfficeCode { get; set; }
		public string PaperStatus { get; set; }
		public DateTime SendDate { get; set; }
		public string PType { get; set; }
		public string eFilePath { get; set; }
		[NotMapped]
		public string DepartmentName { get; set; }

		public DateTime ReceivedDate { get; set; }
		public string PaperRefNo { get; set; }
        public List<string> PaperRefNoList { get; set; }
		public string ReplyStatus { get; set; }
		public DateTime ReplyDate { get; set; }

		[NotMapped]
		public string Name { get; set; }
		[NotMapped]
		public int Length { get; set; }
		[NotMapped]
		public int[] eFileAttachmentIds { get; set; }

		public string LinkedRefNo { get; set; }
		public int ToPaperNature { get; set; }
		public string ToPaperNatureDays { get; set; }
		public string RevedOption { get; set; }
		public string RecvDetails { get; set; }
		public string ToRecvDetails { get; set; }
		public int eMode { get; set; }
		[NotMapped]
		public string DocuemntPaperType { get; set; }
		
		public int CommitteeId { get; set; }
        [NotMapped]
        public string CommitteeName { get; set; }
		[NotMapped]
		public string RecvdPaperNature { get; set; }

		public string PaperTicketValue { get; set; }
		public string PaperNo { get; set; }
		public string DispatchtoWhom { get; set; }
		public bool IsDeleted { get; set; }
		[NotMapped]
		public string DeptAbbr { get; set; }
		[NotMapped]
		public string ToDeptAbbr { get; set; }
		[NotMapped]
		public string ToDepartmentName { get; set; }

		public string eFilePathWord { get; set; }

		public int ReFileID { get; set; }

        [NotMapped]
        public string MainEFileName { get; set; }
        public string DiaryNo { get; set; }
        [NotMapped]
        public string Year { get; set; }
        public int CurYear { get; set; }
        public int CurDNo { get; set; }
        public int CurrentBranchId { get; set; }
        public string CurrentEmpAadharID { get; set; }
        public DateTime? AssignDateTime { get; set; }
        [NotMapped]
        public string BranchName { get; set; }
        [NotMapped]
        public string EmpName { get; set; }
        [NotMapped]
        public List<string> DeptIds { get; set; }
       [NotMapped]
        public string ToListComplN { get; set; }
       [NotMapped]
       public string CCListComplN { get; set; }
       [NotMapped]
       public string ToMemIds { get; set; }
       [NotMapped]
       public string ToDepIds { get; set; }
       [NotMapped]
       public string ToOtherIds { get; set; }
       [NotMapped]
       public string CCMemIds { get; set; }
       [NotMapped]
       public string CCDepIds { get; set; }
       [NotMapped]
       public string CCOtherIds { get; set; }
       [NotMapped]
       public string ToListComplIds { get; set; }
       [NotMapped]
       public string CCListComplIds { get; set; }
       [NotMapped]
       public string eFilePathPDFN { get; set; }
       [NotMapped]
       public string eFilePathWordN { get; set; }
       [NotMapped]       
       public virtual ICollection<mDepartment> mDepartment { get; set; }
       public int? DraftId { get; set; }
       public int? ToMemberId { get; set; }
       public int? ToOtherId { get; set; }
       public string TOCCtype { get; set; }
       [NotMapped]
       public virtual ICollection<HouseComModel> HouseComModel { get; set; }
       [NotMapped]
       public string Desingnation { get; set; }
       [NotMapped]
       public string AbbrevBranch { get; set; }
       [NotMapped]
       public string ToAllDraft { get; set; }
       [NotMapped]
       public string CCAllDraft { get; set; }
          [NotMapped]
       public string SignFilePath { get; set; }
       public string AnnexPdfPath { get; set; }       
       public int? CountsAPS { get; set; }
        [NotMapped]
        public string DraftedBy { get; set; }
           [NotMapped]
        public int ItemTypeName { get; set; }
           [NotMapped]
           public string  ValueType { get; set; }
           [NotMapped]
           public int YearId { get; set; }
           [NotMapped]
           public string ItemName { get; set; }
           public int ItemId { get; set; }
           public string ItemNumber { get; set; }
           public string ReplyRefNo { get; set; }
           [NotMapped]
           public string ItemSubject { get; set; }
           [NotMapped]
           public string ItemDescription { get; set; }
           [NotMapped]
           public int CountRply { get; set; }
           public int creDraftID { get; set; }
           public bool IsVerified { get; set; }
           public string VerifiedBy { get; set; }
           public DateTime? VerifiedDate { get; set; }
           [NotMapped]
           public string IsPdf { get; set; }
           [NotMapped]
           public string IsWord { get; set; }
           public string PaperRefrenceManualy { get; set; }
           //[UIHint("tinymce_jquery_full_LOB"), AllowHtml]
           //public string PaperDraft { get; set; }
           [NotMapped]
           public string ComAbbr { get; set; }
           [NotMapped]
           public string AnnexPDF { get; set; }
           public bool IsBackLog { get; set; }
           public bool IsRejected { get; set; }
           public string RejectedRemarks { get; set; }
           public string IpAddress { get; set; }
           public string MacAddress { get; set; }
        [NotMapped]
           public string IsDataRead { get; set; }
        [NotMapped]
        public string SettingValue_file { get; set; }
        [NotMapped]
        public string eFileNumber { get; set; }
        [NotMapped]
        public List<eFileAttachment> DocDatalist { get; set; }
        //For Letter/Correspondence
        [NotMapped]
        public List<tCommittee> tCommittee { get; set; }
      
	}
	[Serializable]
	public class eFileAttachments
	{
		public int eFileAttachmentId { get; set; }
		public int eFileID { get; set; }
		public int DocumentTypeId { get; set; }
		public string eFileName { get; set; }
		public string Description { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedDate { get; set; }
		public bool SendStatus { get; set; }
		public string DepartmentId { get; set; }
		public string Title { get; set; }
		public Guid eFileNameId { get; set; }
		public string mode { get; set; }
		public List<eFileAttachment> eFileAttachemts { get; set; }
		public List<DepartmentReply> departmentReplys { get; set; }
		public List<eFileList> eFileLists { get; set; }
        [NotMapped]
        public string RolesId { get; set; }
        public List<CommitteMeetingPapersModel> CommitteMeetingPapersList { get; set; }
        public List<CommittePapersLaidModel> CommittePapersLaidList { get; set; }
        [NotMapped]
        public int MeetingId { get; set; }
        [NotMapped]
        public string papersIds { get; set; }
        [NotMapped]
        public string CurrentSessionAId { get; set; }
        [NotMapped]
        public string Sessiondesign { get; set; }
         [NotMapped]
        public string ManualAttach { get; set; }
         [NotMapped]
         public string ButtonShow { get; set; }
         [NotMapped]
         public int CheckAuthorize { get; set; }
         [NotMapped]
         public int DraftListCount { get; set; }
         [NotMapped]
         public int SentPaperCount { get; set; }
	}

	[Serializable]
	public class DepartmentReply
	{
		public DepartmentReply()
		{
			ChildNode = new List<DepartmentReply>();
		}
		public int eFileAttachmentId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public DateTime CreatedDate { get; set; }
		public int ParentId { get; set; }
		public int eFileID { get; set; }
		public string eFileName { get; set; }
		public List<DepartmentReply> ChildNode { get; set; }
	}
}
