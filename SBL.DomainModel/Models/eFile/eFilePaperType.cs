﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.eFile
{
	[Table("meFilePaperType")]
	[Serializable]
	public class eFilePaperType
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int PaperTypeID { get; set; }

		public string PaperType { get; set; }

        public bool IsActive { get; set; }//added by robin

        public string CreatedBy { get; set; }//added by robin

        public DateTime CreatedOn { get; set; }//added by robin

	}
}
