﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.eFile
{
	[Table("meFilePaperNature")]
	[Serializable]
	public class eFilePaperNature
	{

		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int PaperNatureID { get; set; }

		public string PaperNature { get; set; }

		public int RPaperNatureId { get; set; }

        public bool IsActive { get; set; }//added by robin

        public string CreatedBy { get; set; }//added by robin

        public DateTime CreatedOn { get; set; }//added by robin

        public bool ActiveVs { get; set; }
        public bool ActiveDept { get; set; }
	}
}
