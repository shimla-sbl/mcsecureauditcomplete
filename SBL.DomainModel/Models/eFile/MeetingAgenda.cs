﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SBL.DomainModel.Models.Committee;

namespace SBL.DomainModel.Models.eFile
{
    [Serializable]
    public partial class MeetingAgenda
    {
        public Int32 MemberCode { get; set; }
        public string CommitteeId { get; set; }
        public string MeetingDate { get; set; }
        public string NewMeetingDate { get; set; }
        public string Meetingtime { get; set; }
        public string CommitteeName { get; set; }
        public string Abbreviation { get; set; }
        public string COMId { get; set; }
        public string eFIleAttachmentId { get; set; }

        public string TextCOM { get; set; }
        public string PDFLocation { get; set; }
        public string SrNo1 { get; set; }
        public string SrNo2 { get; set; }
        public string SrNo3 { get; set; }
        public string MainSrNo { get; set; }
        public string IsDeleted { get; set; }
        public string UserId { get; set; }
        [NotMapped]
        public List<CommitteesListView> CommitteeList { get; set; }
        //public List<SBL.DomainModel.ComplexModel.QuestionModelCustom> MemberList { get; set; }
        [NotMapped]
        public List<SelectListItem> FinancialYearlist { get; set; }
        [NotMapped]
        public string YearId { get; set; }
        [NotMapped]
        public List<MeetingAgenda> MeetingDataList { get; set; }
        [NotMapped]
        public List<MeetingAgenda> MeetingPdfList { get; set; }
    }
}
