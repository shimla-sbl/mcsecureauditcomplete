﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.User;

namespace SBL.DomainModel.Models.UserAction
{
    [Table("mUserActions")]
    [Serializable]
    public partial class mUserActions
    {

        public mUserActions()
        {

            this.objList = new List<mUserActions>();
        }

        [Key]
        public int ActionId { get; set; }

        [StringLength(250)]
        public string ActionName { get; set; }

        [StringLength(250)]
        public string ActionNameLocal { get; set; }

        [StringLength(500)]
        public string ActionDescription { get; set; }

        public bool Isactive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }


        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        //[NotMapped]
        //public virtual List<mUserModules> ListmRoles { get; set; }

        [NotMapped]
        public List<mUserActions> objList { get; set; }

        [NotMapped]
        public List<mUserType> mUserTypeList { get; set; }

         [NotMapped]
        public int UserTypeID { get; set; }

        [NotMapped]
        public string UserTypeName { get; set; }

        [NotMapped]
        public string Mode { get; set; }
    }
}
