﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SBL.DomainModel.Models.Governor
{
    [Serializable]
    [Table("mGoverner")]
   public class mGoverner
    {
       [Key]
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int mGovernerID { get; set; }
        [Required]
        public int mStateID { get; set; }
        public string Prefix { get; set; }
        public string Name { get; set; }
        public string NameLocal { get; set; }
        [Required]
        public int MemberCode { get; set; }
        public string District { get; set; }
        public string ShimlaAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string Sex { get; set; }
        public string TelOffice { get; set; }
        public string TelResidence { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string FatherName { get; set; }
        public bool? Active { get; set; }
        public string LoginID { get; set; }
        [StringLength(10)]
        public string CmRefnicVipCode { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

        public bool? IsDeleted { get; set; }

        [NotMapped]
        public string GetStateName { get; set; }
        public string FileName { get; set; }
        public string ThumbName { get; set; }

        public string FilePath { get; set; }
        [NotMapped]
        public string ImageAcessurl { get; set; }
    }
}
