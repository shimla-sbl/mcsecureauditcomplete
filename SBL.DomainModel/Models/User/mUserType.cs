﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.User
{
    using SBL.DomainModel.Models.UserModule;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Serializable]
    [Table("mUserType")]
    public partial class mUserType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserTypeID { get; set; }
        [Required]
        public string UserTypeName { get; set; }

        public string UserTypeNameLocal { get; set; }

        public bool IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }

        [NotMapped]
        public List<mUserType> objList { get; set; }

        [NotMapped]
        public List<mUserModules> ListModule { get; set; }

        [NotMapped]
        public string Mode { get; set; }
    }
}
