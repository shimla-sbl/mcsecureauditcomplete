﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SBL.DomainModel.Models.User
{

    [Table("tOneTimeUserRegistration")]
    [Serializable]
    public class tOneTimeUserRegistration
    {
        public tOneTimeUserRegistration()
        {
           
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string OTPPassword { get; set; }
        [StringLength(12)]
        public string MobileNumber { get; set; }
        public bool? IsActive { get; set; }

        [NotMapped]
        public int RadioNewRequest{get;set;}
        [NotMapped]
        public int RadioExistingRequest { get; set; }

          [NotMapped]
        public int AssemblyId { get; set; }

          [NotMapped]
        public int SessionId { get; set; }

        [NotMapped]
        public string RecommendationType { get; set; }

        [NotMapped]
        public string DiaryNumber { get; set; }

        [NotMapped]
        public string CustomMessage { get; set; }

        [NotMapped]
        public string Subject { get; set; }

        [NotMapped]
        public string AssemblyName { get; set; }

        [NotMapped]
        public string SessionName { get; set; }

        [NotMapped]
        public int PaperTypeId { get; set; }

     
        public string UserName { get; set; }

       
        public string AddressLocations { get; set; }

        public string Email { get; set; }

        [NotMapped]

        public virtual ICollection<tOneTimeUserRegistration> OneTimeUserRegistrationModel { get; set; }
    }
}
