﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Assembly;

namespace SBL.DomainModel.Models.User
{
    [Serializable]
    [Table("mSiteSettings")]
    public class mSiteSettingsVS
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SettingId { get; set; }

        public string SettingName { get; set; }

        public string SettingValue { get; set; }

        public bool IsActive { get; set; }

        [NotMapped]
        public virtual ICollection<mAssembly> mAssembly { get; set; }
    }
}
