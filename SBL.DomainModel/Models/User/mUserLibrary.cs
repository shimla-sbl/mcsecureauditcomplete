﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.User
{
    [Serializable]
    class mUserLibrary
    {
        public int userID { get; set; }
        public string userName { get; set; }
        public string roleName { get; set; }
        public string roleId { get; set; }
        [NotMapped]
        public int? SubUserTypeID { get; set; }
        [NotMapped]
        public List<LibraryRoleList> RolesList { get; set; }
    }

    [Serializable]
    public class LibraryRoleList
    {
        public string SubUserTypeName { get; set; }
        public int SubUserTypeID { get; set; }

    }
}
