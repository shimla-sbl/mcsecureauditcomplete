﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.User
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("mUserDSHDetails")]
    public partial class mUserDSHDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKey { get; set; }

        public Guid UserId { get; set; }
        public string HashKey { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public DateTime? EnrollDate { get; set; }
        public DateTime? Validity { get; set; }
        public bool IsActive { get; set; }
    }
}
