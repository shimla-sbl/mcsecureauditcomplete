﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;

namespace SBL.DomainModel.Models.User
{
    [Serializable]
    public class OneTimeUserRegistrationModel
    {

        public OneTimeUserRegistrationModel()
        {
            this.mAssemblyList = new List<mAssembly>();
            this.sessionList = new List<mSession>();
            this.List = new List<tUserRegistrationDetails>();

        }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public int Id { get; set; }
        public string UserId { get; set; }
        public Guid OTPPassword { get; set; }
        public int DocumentTypeId { get; set; }
        [StringLength(150)]
        public string DiaryNumber { get; set; }
        [StringLength(12)]
        public string MobileNumber { get; set; }
        public DateTime? IsSubmittedDate { get; set; }
        public bool? IsActive { get; set; }
        [StringLength(150)]
        public bool? IsLocked { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AssesmblyName { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mAssembly> mAssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }
        public string OTPId { get; set; }
        public List<tUserRegistrationDetails> List { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int ? QuestionNumber { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int  QuestionId { get; set; }
        public int Status { get; set; }
        public long? PaperLaiId { get; set; }
        public int NoticeTypeId { get; set; }
        public string EventName { get; set; }
        public int TotalStaredReceived { get; set; }
        public int TotalUnStaredReceived { get; set; }
        public int TotalNoticesReceived { get; set; }
        public int? RuleId { get; set; }
        public int ModuleId { get; set; }
        public string Subject { get; set; }
        public DateTime? RecieveDateTime { get; set; }
        public TimeSpan? NoticeRecievedTime { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string Addresslocation { get; set; }
    }
}
