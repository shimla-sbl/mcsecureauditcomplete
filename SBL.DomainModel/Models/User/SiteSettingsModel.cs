﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Assembly;

namespace SBL.DomainModel.Models.User
{
    [Serializable]
    public class SiteSettingsModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SettingId { get; set; }

        public string SettingName { get; set; }

        public string SettingValue { get; set; }

        public string IsActive { get; set; }

        [NotMapped]
        public virtual ICollection<mAssembly> mAssembly { get; set; }

        [NotMapped]
        public string AssemblyId { get; set; }

        [NotMapped]
        public string SessionId { get; set; }

        [NotMapped]
        public virtual ICollection<mSession> mAssemblySession { get; set; }

    }
}
