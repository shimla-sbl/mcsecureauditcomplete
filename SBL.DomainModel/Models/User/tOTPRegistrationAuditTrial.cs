﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.User
{

    [Table("tOTPRegistrationAuditTrial")]
    [Serializable]
   public class tOTPRegistrationAuditTrial
    {
        public tOTPRegistrationAuditTrial()
        {
            this.OTPRegistrationModel = new List<tOTPRegistrationAuditTrial>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string DiaryNumber { get; set; }
        [StringLength(12)]
        public string MobileNumber { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public int DocTypeId { get; set; }
        public string MainReplyPdfPath { get; set; }
        public string MainReplyDocPath { get; set; }
        public string MainReplySupDocPath { get; set; }
        public string OTPId { get; set; }
        public DateTime? UpdatedDT { get; set; }
        public int? RuleId { get; set; }

        [NotMapped]

        public virtual ICollection<tOTPRegistrationAuditTrial> OTPRegistrationModel{ get; set; }

        public string AadharId { get; set; }
    }
}
