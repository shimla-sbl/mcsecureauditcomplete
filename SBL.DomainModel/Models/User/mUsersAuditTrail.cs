﻿ 
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.ComplexModel.UserComplexModel;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.HOD;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.Role;
using SBL.DomainModel.Models.Secretory;
using SBL.DomainModel.Models.UserAction;
using SBL.DomainModel.Models.UserModule;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.User
{
	[Serializable]
	[Table("mUsersAuditTrail")]
	public class mUsersAuditTrail
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long ID { get; set; }
		public Guid UserId { get; set; }
		[Required]
		[StringLength(250)]
		public string UserName { get; set; }

		[Required]
		[StringLength(250)]
		public string Password { get; set; }

		public bool? IsActive { get; set; }
		 
		public string DeptId { get; set; }

		[StringLength(50)]
		public string OfficeId { get; set; }

		[StringLength(50)]
		public string OfficeLevel { get; set; }

		[StringLength(50)]
		public string BranchId { get; set; }

		[StringLength(50)]
		public string ConstituencyId { get; set; }

		[StringLength(50)]
		public string Designation { get; set; }

		[StringLength(50)]
		public string AadarId { get; set; }

		[StringLength(250)]
		public string DSCHash { get; set; }

		[StringLength(50)]
		public string IsMember { get; set; }

		[StringLength(50)]
		public string IsMedia { get; set; }

		[StringLength(25)]
		public string ModifiedBy { get; set; }

		public DateTime? ModifiedWhen { get; set; }

		public bool IsDSCAuthorized { get; set; }

		public int? SecretoryId { get; set; }

		public string DepartmentIDs { get; set; }
		public string EmpId { get; set; }
		public string Photo { get; set; }
		public string MobileNo { get; set; }
		public string EmailId { get; set; }
		public string Address { get; set; }
		public string Gender { get; set; }
		public int? Age { get; set; }
		public string DOB { get; set; }
		public bool IsNotification { get; set; }
		public bool IsAuthorized { get; set; }
		public bool Permission { get; set; }
		public bool? IsSecretoryId { get; set; }
		public string SignaturePath { get; set; }
		public DateTime? FirstWrongAttemptTime { get; set; }
		public int WrongAttemptCount { get; set; }
		public DateTime? BlockUntil { get; set; }
		public bool? IsHOD { get; set; }
		public bool? IsOther { get; set; }
		public int? UserType { get; set; }
		public bool IsSecChange { get; set; }
		public string AssemblyPassword { get; set; }
	}
}
