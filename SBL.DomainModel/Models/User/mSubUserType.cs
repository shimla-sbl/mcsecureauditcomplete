﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.User
{
    [Serializable]
    [Table("mSubUserType")]
   public partial class mSubUserType
   {
        public mSubUserType()
        {

            this.objList = new List<mSubUserType>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SubUserTypeID { get; set; }

        public int UserTypeID { get; set; }

        [Required]
        public string SubUserTypeName { get; set; }

        public string SubUserTypeNameLocal { get; set; }

        [NotMapped]
        public string UserTypeName { get; set; }

        [StringLength(500)]
        public string SubUserTypeDescription { get; set; }

        public string CreatedBy { get; set; }

        public bool IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }

        [NotMapped]
        public List<mSubUserType> objList { get; set; }

        [NotMapped]
        public List<mUserType> ListUserType { get; set; }


        [NotMapped]
        public List<mSubUserType> UserTypeList { get; set; }

        [NotMapped]
        public string Mode { get; set; }

         [NotMapped]
        public string iSubUserTypeID
        {
            get
            {
                return SubUserTypeID.ToString();
            }
            set { }
        }

        [NotMapped]
        public virtual ICollection<mUsers> Users { get; set; }

   }
}
