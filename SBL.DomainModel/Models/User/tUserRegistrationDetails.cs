﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.User
{

    [Table("tUserRegistrationDetails")]
    [Serializable]
    public class tUserRegistrationDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
      
        public int AssemblyId { get; set; }
        public int SessionId { get; set; }
        [StringLength(12)]
        public string MobileNumber { get; set; }
        public int DocumentTypeId { get; set; }
        [StringLength(150)]
        public string DiaryNumber { get; set; }
        public DateTime? IsSubmittedDate { get; set; }
        public DateTime? IsAssignDate { get; set; }
        public bool? IsLocked { get; set; }
        public int? RuleId { get; set; }
        [NotMapped]
        public virtual ICollection<OneTimeUserRegistrationModel> OneTimeUserRegistrationModel { get; set; }


        [NotMapped]
        public int RadioNewRequest { get; set; }
        [NotMapped]
        public int RadioExistingRequest { get; set; }
        
        [NotMapped]
        public string RecommendationType { get; set; }

       
        [NotMapped]
        public string CustomMessage { get; set; }

        [NotMapped]
        public string Subject { get; set; }

        [NotMapped]
        public string AssemblyName { get; set; }

        [NotMapped]
        public string SessionName { get; set; }

        [NotMapped]
        public int PaperTypeId { get; set; }

          [NotMapped]
        public string UserName { get; set; }

          [NotMapped]
        public string AddressLocations { get; set; }

          [NotMapped]
          public string Email { get; set; }

          [NotMapped]

          public virtual ICollection<tUserRegistrationDetails> UserRegistrationDetailsModel { get; set; }
          [NotMapped]
          public string UserId { get; set; }

        
          public string AadharId { get; set; }
    }
}
