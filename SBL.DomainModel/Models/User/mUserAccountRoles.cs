﻿using SBL.DomainModel.Models.Role;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.User
{
    //By robin---for using accounts users with roles
    [Serializable]
    public class mUserAccountRoles
    {       
        public Guid userID { get; set; }
        public string userName { get; set; }
        public string roleName { get; set; }
        public string roleId { get; set; }
        [NotMapped]
        public int? SubUserTypeID { get; set; }
        [NotMapped]
        public List<RoleList> RolesList { get; set; }
        //[NotMapped]        
        //public virtual SelectList AccountsRoles { get; set; }
    }

    [Serializable]
    public class RoleList
    {
        public string SubUserTypeName { get; set; }
        public int SubUserTypeID { get; set; }

    }
}
