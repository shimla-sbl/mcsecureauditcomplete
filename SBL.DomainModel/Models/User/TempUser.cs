﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.User
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    [Table("TempUsers")]
    [Serializable]
   public partial class TempUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKey { get; set; }
        public Guid userGuid { get; set; }
        public string UserId { get; set; }
        public string password { get; set; }
    }
}
