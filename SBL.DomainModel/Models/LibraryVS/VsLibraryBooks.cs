﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.LibraryVS
{
    [Serializable]
    [Table("VsLibraryBooks")]
    public class VsLibraryBooks
    {
        public VsLibraryBooks()
        {
            //contructor logic
          
        }
       // public string HAN;
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SerialNumber { get; set; }

        [Required]
        [Display(Name="Accession Number")]
        public string AccessionNumber { get; set; }

        //for updating accession number incase
        //[NotMapped]
       // public string HiddenAccessionNumber { get{return AccessionNumber;} set{HAN=AccessionNumber;} }
        
        [Required]
        [Display(Name="Entry Date")]
        public string DateOfEntry { get; set; }

        [Required]
        [Display(Name = "Author Name")]
        public string AuthorName { get; set; }

        [Display(Name = "Editor Name")]
        public string EditorName { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string TitleOfBook { get; set; }

        [Required]
        [Display(Name = "Edition")]
        public string Edition { get; set; }

        [Display(Name = "Volume")]
        public string Volume { get; set; }

        [Required]
        [Display(Name = "Publisher Name")]
        public string PublisherName { get; set; }

        [Required]
        [Display(Name = "Publication Place")]
        public string PublicationPlace { get; set; }

        [Required]
        [Display(Name = "Publication Year")]
        public string PublicationYear { get; set; }

        [Required]
        public string Pages { get; set; }

        [Required]
        [Display(Name = "Source Or Suplier Name")]
        public string SourceOrSuplierName { get; set; }

        [Required]
        [Display(Name = "Supplier Place")]
        public string SupplierPlace { get; set; }

        [Required]
        public string Cost { get; set; }

        [Display(Name = "Call Number")]
        public string CallNumber { get; set; }

        [Required]
        [Display(Name = "Book Number")]
        public string BookNumber { get; set; }

        [Required]
        [Display(Name = "Bill Number")]
        public string BillNumber { get; set; }

        [Required]
        [Display(Name = "Bill Date")]
        public string BillDate { get; set; }

        [Required]
        [Display(Name = "Language Option")]
        public string LanguageOption { get; set; }

        [Display(Name = "PO Number")]
        public string PONumber { get; set; }

        [Display(Name = "PO Date")]
        public string PODate { get; set; }

        public string Remarks { get; set; }

        [Required]
        public string Building { get; set; }

        [Required]
        public string Floor { get; set; }

        [Required]
        public string Almirah { get; set; }
        
        public string Rack { get; set; }

        [Required]
        public string Status { get; set; }

        public string Verfication { get; set; }

        public string VerficationYear { get; set; }

        public string WriteOff { get; set; }

        public string WriteOffDate { get; set; }

        public string WriteOffRemarks { get; set; }

        public string WriteOffValue { get; set; }

        [NotMapped]
        public bool checkStatus { get; set; }
    }
}
