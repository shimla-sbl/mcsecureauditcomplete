﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Bill;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.LibraryVS
{
    [Serializable]
    [Table("VsLibraryBillInfo")]
    public class VsLibraryBillInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SerialNumber { get; set; }

        [Required]
        public string Title { get; set; }

        //[Required]
        //public string Year { get; set; }

        public int BillId { get; set; }

        [Required]
        public string BillNumber { get; set; }
        
        public string status { get; set; }

        [Required]
        public string Building { get; set; }

        [Required]
        public string Floor { get; set; }

        [Required]
        public string Almirah { get; set; }

        public string Rack { get; set; }

        [NotMapped]
        public List<fillListGenric> BillNoList { get; set; }

        [NotMapped]
        public List<mBills> mbills { get; set; }


    }
}
