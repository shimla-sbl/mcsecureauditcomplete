﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.LibraryVS
{
    [Serializable]
    [Table("VsLibraryBulletin")]
    public class VsLibraryBulletin
    {
        public VsLibraryBulletin() 
        {
            this.mAssembly = new List<mAssembly>();
            this.mSession = new List<mSession>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SerialNumber { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }
        
        [Required]
        public int Assembly { get; set; }

        [Required]
        public int Session { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        //[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string StartDate { get; set; }

        [Required]
        [Display(Name = "End Date")]
        //[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string EndDate { get; set; }

        [Required]
        public string Building { get; set; }

        [Required]
        public string Floor { get; set; }

        [Required]
        public string Almirah { get; set; }

        public string Rack { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mSession> mSession { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mAssembly> mAssembly { get; set; }

        [NotMapped]
        public List<mAssembly> assemblylist { get; set; }

        [NotMapped]
        public List<mSession> sessionlist { get; set; }
    
    }
}
