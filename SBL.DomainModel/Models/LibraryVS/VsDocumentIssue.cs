﻿using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Member;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.LibraryVS
{
    [Table("VsDocumentIssue")]
    [Serializable]
    public class VsDocumentIssue
    {
        public VsDocumentIssue()
        {
            //this.DesignationCol = new List<mMemberDesignation>();        
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IssueID { get; set; }

        [DefaultValue(" ")]
        public string IssueDocumentName { get; set; }

        public DateTime DateOfIssue { get; set; }

        [NotMapped]
        public string DisplayedDOI { get; set; }

        [Required]
        public string TimeOfIssue { get; set; }

        [Required]
        public string SubscriberDesignationId { get; set; }

        [Required]
        public int SubscriberId { get; set; }

        [NotMapped]
        public string SubscriberDesignation { get; set; }

        [NotMapped]
        public string SubscriberName { get; set; }

        [NotMapped]
        public ICollection<mMemberDesignation> DesignationCol { get; set; }

        [NotMapped]
        public ICollection<mMember> MemberCol { get; set; }
        
        [DefaultValue(false)]
        public bool IsReturned { get; set; }

        [DefaultValue(true)]
        public bool IsMember { get; set; }

        public DateTime? DateOfReturned { get; set; }

        public string TimeOfReturn { get; set; }

        [NotMapped]
        public DateTime? IssueStartRange { get; set;}

        [NotMapped]
        public DateTime? IssueEndRange {get;set;}

        [NotMapped]
        public DateTime? ReturnStartRange{get;set;}

        [NotMapped]
        public DateTime? ReturnEndRange{get;set;}
 
    }
}
