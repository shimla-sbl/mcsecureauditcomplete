﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Adhaar
{
    [Serializable]
    public partial class AdhaarDetails
    {
        public AdhaarDetails()
        { }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AdhaarDetailsId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string AdhaarID { get; set; }
        public string Gender { get; set; }
        public string FatherName { get; set; }
        public string DOB { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string District {get; set;}
        public string PinCode { get; set; }
        public string Photo { get; set; }
    }
}
