﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.MemberAssemblyRemarks
{
    [Serializable]
    [Table("mMemberAssemblyRemarks")]
    public class mMemberAssemblyRemarks
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MemberAssemblyRemarksID { get; set; }

        public string MemberAssemblyRemarks { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool? Active { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
