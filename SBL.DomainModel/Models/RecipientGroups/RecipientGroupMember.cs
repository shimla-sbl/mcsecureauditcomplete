﻿namespace SBL.DomainModel.Models.RecipientGroups
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    #endregion

    [Serializable]
    [Table("tRecipientGroupMember")]
    public class RecipientGroupMember
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 GroupMemberID { get; set; }
        public int GroupID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public string WardName { get; set; }
        public string AddressVS { get; set; }
        public string RoomNoVS { get; set; }

        public string PinCode { get; set; }
        public bool IsActive { get; set; }

        public string DepartmentCode { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
		public int? RankingOrder { get; set; }
		public string LandLineNumber { get; set; }
        public string LandLineNumberVS { get; set; }
        [NotMapped]
        public string AssociatedContactGroups { get; set; }


		[NotMapped]
		public int? ConstituencyCode { get; set; }
		[NotMapped]
		public int? PancayatCode { get; set; }
		[NotMapped]
		public string Officecode { get; set; }
        public string AadharId { get; set; }
        public string CommitteeId { get; set; }
    }
}
