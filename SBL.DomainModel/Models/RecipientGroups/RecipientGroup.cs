﻿namespace SBL.DomainModel.Models.RecipientGroups
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SBL.DomainModel.Models.Office;
    #endregion

    [Serializable]
    [Table("mRecipientGroups")]
    public class RecipientGroup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsPublic { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }

        public int GroupTypeID { get; set; }

        public string DepartmentCode { get; set; }
        public int? PancayatCode { get; set; }
        public int? ConstituencyCode { get; set; }
		public bool IsGeneric { get; set; }
		public int? RankingOrder { get; set; }
		public bool? IsConstituencyOffice { get; set; }
        [NotMapped]
        public Guid UserID { get; set; }
		 
		public int OfficeId { get; set; }
        [NotMapped]
        public int MemberCode { get; set; }

		[NotMapped]
		public string PanchayatName { get; set; }

		[NotMapped]
		public string ConstituencyName { get; set; }
        [NotMapped]
        public string DistId { get; set; }
        [NotMapped]
        public string DeptId { get; set; }
        [NotMapped]
        public int AssId { get; set; }
        [NotMapped]
        public string ConstituencyCodeForSubdivision { get; set; }
          [NotMapped]
        public List<mOffice> OfficeList { get; set; } 
              [NotMapped]
          public List<mOffice> OfficeList_search { get; set; } 

    }
    [Serializable]
    public class fillListGenric1
    {

        public string Text { get; set; }
        public string value { get; set; }

    }
}
