﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Districtconsistency
{
    [Table("tDistrictconsistency")]
    [Serializable]
    public class Districtconsistency
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int consistencyCode { get; set; }
        public int DistrictCode { get; set; }

    }
}
