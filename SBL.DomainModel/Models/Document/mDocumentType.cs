﻿namespace SBL.DomainModel.Models.Document
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    #endregion

    [Table("mDocumentType")]
    [Serializable]
    public partial class mDocumentType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocumentTypeID { get; set; }

        [Required]
        [StringLength(250)]
        public string DocumentType { get; set; }

        [StringLength(250)]
        public string DocumentTypeLocal { get; set; }

        public bool? IsActive { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
