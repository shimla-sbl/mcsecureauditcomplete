﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.ConstituencyVS
{
    [Serializable]
    [Table("tWorkTrans")]
    public class workTrans
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int autoID { get; set; }

        public long schemeID { get; set; }

        public int memberCode { get; set; }

        public Guid userID { get; set; }

        public string Remarks { get; set; }

        public bool read { get; set; }

        public bool isMember { get; set; }

        public DateTime SubmitTime { get; set; }
        public string SubmitTimeString { get; set; }

        [NotMapped]
        public string imagePath { get; set; }

        [NotMapped]
        public string OfficerName { get; set; }
        [NotMapped]
        public string workName { get; set; }


    }
}
