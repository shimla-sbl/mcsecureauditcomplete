﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.ConstituencyVS
{
    [Serializable]
    [Table("tConstituencyWorkProgress")]
    public class ConstituencyWorkProgress : userInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int autoID { get; set; }
        public long schemeCode { get; set; }
        public string workCode { get; set; }
        public long constituencyCode { get; set; }
        public string RemarkProgress { get; set; }
        public double? toalExpenditure { get; set; }
        public int? toalProgress { get; set; }
        public DateTime? submitDate { get; set; }
        public double? totalfundreleased { get; set; }
    }

    [Serializable]
    [Table("tConstituencyWorkProgressImages")]
    public class ConstituencyWorkProgressImages : userInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int autoID { get; set; }
        public long schemeCode { get; set; }
        public string workCode { get; set; }
        public long constituencyCode { get; set; }
        public string filePath { get; set; }
        public string tempPath { get; set; }
        public string thumbFilePath { get; set; }
        public string fileName { get; set; }
        public string tFileCaption { get; set; }
        public DateTime? UploadedDate { get; set; }
        [NotMapped]
        public string ProjectName { get; set; }
        [NotMapped]
        public string ConstituencyName { get; set; }
        [NotMapped]
        public string ControllingAuthoritydept { get; set; }
        [NotMapped]
        public string DepartmentIdOwner { get; set; }
        [NotMapped]
        public string financialYear { get; set; }
        [NotMapped]
        public string workStatus
        {
            get;
            set;
        }
    }
}
