﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.ConstituencyVS
{
    [Serializable]
    [Table("mWorkStatus")]
    public class mworkStatus
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long workStatusID { get; set; }
        public int workStatusCode { get; set; }
        public string workStatus { get; set; }
        public int? SequenceOrder { get; set; }
        public string workStatusLocal { get; set; }
        public string WorkIconPath { get; set; }
    }

    [Serializable]
    [Table("mWorkType")]
    public class workType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long workID { get; set; }

        public string deptID { get; set; }

        public long WorkTypeCode { get; set; }

        public string WorkTypeName { get; set; }

        public string WorkTypeNameLocal { get; set; }
        [NotMapped]
        public int ClickCount { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }
        public List<workType> AllList { get; set; }

        public bool IsDeleted { get; set; }
    }

    [Serializable]
    [Table("mWorkNature")]
    public class workNature
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long WorkNatureCode { get; set; }

        public long WorkNatureID { get; set; }
        public string WorkNatureName { get; set; }
    }

    [Serializable]
    [Table("mProgramme")]
    public class mProgramme
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProgramID { get; set; }

        public string deptID { get; set; }

        public int programmeCode { get; set; }

        public string programmeName { get; set; }

        [NotMapped]
        public int ClickCount { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public bool? IsDeleted { get; set; }
        [NotMapped]
        public List<mProgramme> AllList { get; set; }

        [NotMapped]
        public string SearchText { get; set; }

        [NotMapped]
        public List<mProgramme> ProgrammeList { get; set; }

        [NotMapped]
        public int PAGE_SIZE { get; set; }

        [NotMapped]
        public int PageIndex { get; set; }

        [NotMapped]
        public int ResultCount { get; set; }

        [NotMapped]
        public int loopStart { get; set; }

        [NotMapped]
        public int loopEnd { get; set; }

        [NotMapped]
        public int selectedPage { get; set; }

        [NotMapped]
        public int PageNumber { get; set; }

        [NotMapped]
        public int RowsPerPage { get; set; }

        [NotMapped]
        public bool HasPreviousPage { get; set; }

        [NotMapped]
        public bool HasNextPage { get; set; }

        [NotMapped]
        public int TempLoop { get; set; }
       
    }

    [Serializable]
    [Table("mControllingAuthority")]
    public class mControllingAuthority
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ControllingAuthorityAutoID { get; set; }

        public string ControllingAuthorityID { get; set; }

        public string ControllingAuthorityName { get; set; }

        public long districtCode { get; set; }
    }
}