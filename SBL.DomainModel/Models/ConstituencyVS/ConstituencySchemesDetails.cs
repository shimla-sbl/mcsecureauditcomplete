﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.ConstituencyVS
{
    [Serializable]
    [Table("tConstituencySchemesBasicDetails")]
    public class ConstituencySchemesBasicDetails
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SchemesBasicDetails { get; set; }

        public long mySchemeID { get; set; }

        public string programName { get; set; }

        public String Owner_deptid { get; set; }

        public String ExecutingDepartmentId { get; set; }

        public string Owner_deptName { get; set; }

        public string ExecutingDepartmentName { get; set; }

        public String ExecutingAgencyID { get; set; }

        public long? ExecutingOfficeID { get; set; }

        public long? Panchayat { get; set; }

        [NotMapped]
        public string Mode { get; set; }
        [NotMapped]
        public string schemeList { get; set; }

        public string ContractorName { get; set; }

        public string ContractorAddress { get; set; }

        public string completionDate { get; set; }

        public DateTime? CompletedDate { get; set; }

        public string completionFinancialYear { get; set; }

        [NotMapped]
        public double? EstimatedAmount { get; set; }


        public Int32? SchemeTypeId { get; set; }
    }

    [Serializable]
    [Table("tConstituencySchemesFinancialDetails")]
    public class ConstituencySchemesFinancialDetails
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SchemesFinancialDetails { get; set; }
    }

    [Serializable]
    public class workBasicView
    {

        public string financialYear { get; set; }
        public string workName { get; set; }
        public string workCode { get; set; }
        public string workStatus { get; set; }
    }
}



