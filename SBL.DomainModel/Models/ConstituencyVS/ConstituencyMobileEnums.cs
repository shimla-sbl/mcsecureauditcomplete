﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.ConstituencyVS
{
    [Serializable]
    [Table("tMenuMasterForContituencyApp")]
    public class tMenuMasterForContituencyApp
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string MenuName { get; set; }
        public string MenuNameLocal { get; set; }
        public string MenuIconPath { get; set; }
        public bool IsActive { get; set; }
    }

    [Serializable]
    [Table("tCopyRights")]
    public class tCopyRights
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string CopyrightText { get; set; }
        public string CopyrightTextLocal { get; set; }
        public string CopyrightText1 { get; set; }
        public string CopyrightTextLocal1 { get; set; }
        public bool IsActive { get; set; }
    }

    [Serializable]
    [Table("tUnitLanguage")]
    public class tUnitLanguage
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string DefaultLanguage { get; set; }
        public string LocalLanguage { get; set; }
        public bool IsActive { get; set; }
    }
   
    [Serializable]
    [Table("tLoginLabels")]
    public class tLoginLabels
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string LabelName { get; set; }
        public string LabelNameValue { get; set; }
        public string LabelNameValueLocal { get; set; }
    } 
      [Serializable]
      [Table("tDiaryLabels")]
    public class tDiaryLabels
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string LabelName { get; set; }
        public string LabelNameValue { get; set; }
        public string LabelNameValueLocal { get; set; }
    }

      [Serializable]
      [Table("tWorksLabels")]
      public class tWorksLabels
      {
          [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
          public long Id { get; set; }
          public string LabelName { get; set; }
          public string LabelNameValue { get; set; }
          public string LabelNameValueLocal { get; set; }
      }
      [Serializable]
      [Table("tVacancyLabels")]
      public class tVacancyLabels
      {
          [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
          public long Id { get; set; }
          public string LabelName { get; set; }
          public string LabelNameValue { get; set; }
          public string LabelNameValueLocal { get; set; }
      }
    
    [Serializable]
      [Table("tMenuMasterForAssemblyApp")]
      public class tMenuMasterForAssemblyApp
      {
          [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
          public long Id { get; set; }
          public string MenuName { get; set; }
          public string MenuNameLocal { get; set; }
          public string MenuIconPath { get; set; }
          public bool IsActive { get; set; }
      }

    [Serializable]
    [Table("tInnerMenuMasterForContituencyApp")]
    public class tInnerMenuMasterForContituencyApp
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string MenuName { get; set; }
        public string MenuNameLocal { get; set; }
        public string MenuIconPath { get; set; }
        public bool IsActive { get; set; }
    }
    [Table("tNewsLabels")]
    public class tNewsLabels
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string LabelName { get; set; }
        public string LabelNameValue { get; set; }
        public string LabelNameValueLocal { get; set; }
    }
}
