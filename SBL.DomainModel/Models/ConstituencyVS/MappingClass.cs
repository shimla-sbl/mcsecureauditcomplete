﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.ConstituencyVS
{
    [Serializable]
    public abstract class userInfo
    {
        public string createdBY { get; set; }

        // [DefaultValue(DateTime.Now)]
        public DateTime createdDate { get; set; }

        public string modifiedBY { get; set; }

        //[DefaultValue(DateTime.Now)]
        public DateTime? ModifiedDate { get; set; }

        [DefaultValue(true)]
        public bool isActive { get; set; }

        [DefaultValue(true)]
        public bool isDeleted { get; set; }
    }

    [Serializable]
    [Table("mConstituencyOffice")]
    public class ConstituencyOffice : userInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long cOfficeID { get; set; }

        public long AssemblyId { get; set; }

        public long ConstituencyCode { get; set; }

        public long MemberCode { get; set; }

        public long OfficerId { get; set; }

        public long DeparmtnetId { get; set; }

        public long OfficeId { get; set; }
    }

    [Serializable]
    [Table("mConstituencyMapping")]
    public class ConstituencyMapping : userInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long autoID { get; set; }

        public long vsConstituencyCode { get; set; }

        public long dcConstituencyCode { get; set; }
    }

    [Serializable]
    [Table("mDepartmentMapping")]
    public class DepartmentMapping : userInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long autoID { get; set; }

        public string departmentCode { get; set; }

        public long budgetDepartmentCode { get; set; }
    }

    [Serializable]
    [Table("mOfficeMapping")]
    public class OfficeMapping : userInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long autoID { get; set; }

        public long vOfficCode { get; set; }

        public long dcOfficCode { get; set; }

        public long OfficCode { get; set; }

        public string OffictName { get; set; }
    }

    [Serializable]
    [Table("mOfficerMapping")]
    public class mOfficerMapping : userInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long autoID { get; set; }

        public long vOfficerCode { get; set; }

        public long dOfficerCode { get; set; }

        public long OfficCode { get; set; }

        public long officerID { get; set; }

        public string OfficertName { get; set; }

        public string mobileNo { get; set; }

        public string emailID { get; set; }
    }

    [Serializable]
    [Table("mDistrictsMapping")]
    public class DistrictsMapping : userInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long autoID { get; set; }

        public long? PMISDistrictsCode { get; set; }

        public long? dcDistrictsCode { get; set; }

        public long? eDistrictsCode { get; set; }
    }
}