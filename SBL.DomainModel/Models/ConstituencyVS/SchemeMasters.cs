﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SBL.DomainModel.Models.ConstituencyVS
{
    [Serializable]
    public abstract class SchemeMaster
    {
        public string ControllingAuthorityId { get; set; }
        public long DistrictId { get; set; }
        public string DepartmentIdOwner { get; set; }
        public string financialYear { get; set; }
        public int financialYearID { get; set; }
        public double? AmountEstimate { get; set; }
        public double? SanctionedAmount { get; set; }
        public double? revisedAmount { get; set; }
        public DateTime? SanctionedDate { get; set; }
        public string WorkCode { get; set; }
        public string workName { get; set; }
        public string WorkNameLocal { get; set; }
        public string ProgrammeName { get; set; }
        public double? TotalAmountReleased { get; set; }
        public long DemandCode { get; set; }
        public string EstimatedCompletionDate { get; set; }
    }
    [Serializable]
    [Table("mSchemeMasterVS")]
    public class SchemeMastersVS : SchemeMaster
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long schemeVSID { get; set; }
        public string syncBY { get; set; }
        public DateTime? syncDate { get; set; }
        public Guid syncCode { get; set; }
    }
    [Serializable]
    [Table("mSchemeMasterDC")]
    public class SchemeMastersDC : SchemeMaster
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long schemeDCID { get; set; }
        public long WorkTypeCode { get; set; }
        public long PanchayatID { get; set; }
        public string Panchayat { get; set; }
        public string syncBY { get; set; }
        public DateTime? syncDate { get; set; }
        public Guid syncCode { get; set; }
    }
    [Serializable]
    [Table("mSchemeMasterConstituency")]
    public class SchemeMastersConstituency : SchemeMaster
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long schemeConstituencyID { get; set; }
        public long constituencyID { get; set; }
        public string createdBY { get; set; }
        public DateTime createdDate { get; set; }
        public string modifiedBY { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? Panchayat { get; set; }
        public string workNature { get; set; }
        public string workType { get; set; }
        public int? workStatus { get; set; }
        public int? memberCode { get; set; }
        public string dprFileName { get; set; }
        public string SalientFeatures { get; set; }
        public int? SubdevisionId { get; set; }
        public int? AssemblyId { get; set; }
        public int? StateCode { get; set; }
    }
    [Serializable]
    [Table("mDemands")]
    public class Demands : userInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DemandNo { get; set; }
        public string demandDesc { get; set; }
    }
}