﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.WorksProposal
{
    [Serializable]
    [Table("mWorkProposal")]
    public class mWorkProposal
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long WorkProposalCode { get; set; }

        public string ControllingAuthorityId { get; set; }

        public long DistrictId { get; set; }

        public string financialYear { get; set; }

        public string workName { get; set; }

        public string ProgrammeName { get; set; }

        public long DemandCode { get; set; }

        public long WorkTypeCode { get; set; }

        public long PanchayatID { get; set; }

        public long constituencyID { get; set; }

        public string createdBY { get; set; }

        public DateTime createdDate { get; set; }

        public string modifiedBY { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public long? Panchayat { get; set; }

        public string workNature { get; set; }

        public string workType { get; set; }

        public int? workStatus { get; set; }

        public int? memberCode { get; set; }

        public string dprFileName { get; set; }

        public string programName { get; set; }

        public String Owner_deptid { get; set; }

        public String ExecutingDepartmentId { get; set; }

        public long? ExecutingOfficeID { get; set; }

    }
}