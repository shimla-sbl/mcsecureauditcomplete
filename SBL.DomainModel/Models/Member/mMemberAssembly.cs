﻿namespace SBL.DomainModel.Models.Member
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("mMemberAssembly")]
    public partial class mMemberAssembly
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MemberAssemblyID { get; set; }
        //[Key]
        //[Column(Order = 0)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AssemblyID { get; set; }

        //[Key]
        //[Column(Order = 1)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MemberID { get; set; }

        //[Key]
        //[Column(Order = 2)]
        ////[StringLength(4)]
        public int ConstituencyCode { get; set; }

        [Column(TypeName = "date")]
        public DateTime? MemberstartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? MemberEndDate { get; set; }

        public bool? Active { get; set; }

        public bool? IsDeleted { get; set; }

        [StringLength(150)]
        public string Remarks { get; set; }

        [StringLength(260)]
        public string DeptAssigned { get; set; }

        public byte? LevelCode { get; set; }

        public bool? IsWomenReservation { get; set; }

        public bool? IsSCSTReservation { get; set; }

        [StringLength(4)]
        public string Category { get; set; }

        public int? AuthorityOrder { get; set; }

        [StringLength(100)]
        public string Designation { get; set; }

        public int? DesignationID { get; set; }

        public int? PartyID { get; set; }

        [StringLength(15)]
        public string PartyCode { get; set; }

        [StringLength(150)]
        public string ProfiePicPath { get; set; }

        [Column(TypeName = "ntext")]
        public string Para1 { get; set; }

        [Column(TypeName = "ntext")]
        public string Para2 { get; set; }

        [Column(TypeName = "ntext")]
        public string Para3 { get; set; }

        [Column(TypeName = "ntext")]
        public string Para4 { get; set; }

        [Column(TypeName = "ntext")]
        public string Para5 { get; set; }

        [Column(TypeName = "ntext")]
        public string sinterest { get; set; }

        [StringLength(260)]
        public string languagesknown { get; set; }

        [Column(TypeName = "ntext")]
        public string travels { get; set; }

        [Column(TypeName = "ntext")]
        public string timepass { get; set; }

        [StringLength(260)]
        public string sports { get; set; }

        [StringLength(260)]
        public string Awards { get; set; }

        [StringLength(260)]
        public string SocialActivities { get; set; }

        [StringLength(260)]
        public string PublishedWork { get; set; }

        [Column(TypeName = "ntext")]
        public string ConferencesAttended { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        [StringLength(50)]
        public string mMemberOldID { get; set; }

        //added by durgesh for pension  
        public int? noOfYears { get; set; }

        [NotMapped]
        public string GetAssemblyName { get; set; }

        [NotMapped]
        public string GetMemberName { get; set; }


        [NotMapped]
        public string GetConstituencyName { get; set; }

        [NotMapped]
        public string GetpartyName { get; set; }


        [NotMapped]
        public string GetMemberDesignationName { get; set; }

        //For Paging

        [NotMapped]
        public int ClickCount { get; set; }

        [NotMapped]
        public List<mMemberAssembly> AllList { get; set; }
        [NotMapped]
        public string SearchText { get; set; }

        [NotMapped]
        public List<mMemberAssembly> MemberAssemblyList { get; set; }

        [NotMapped]
        public int PAGE_SIZE { get; set; }

        [NotMapped]
        public int PageIndex { get; set; }

        [NotMapped]
        public int ResultCount { get; set; }

        [NotMapped]
        public int loopStart { get; set; }

        [NotMapped]
        public int loopEnd { get; set; }

        [NotMapped]
        public int selectedPage { get; set; }

        [NotMapped]
        public int PageNumber { get; set; }

        [NotMapped]
        public int RowsPerPage { get; set; }
        [NotMapped]
        public bool HasPreviousPage { get; set; }

        [NotMapped]
        public bool HasNextPage { get; set; }
        [NotMapped]
        public string MemberName { get; set; }
        [NotMapped]
        public string ConstituencyName { get; set; }
        //[NotMapped]
        //public int AssemblyId { get; set; }


    }
}
