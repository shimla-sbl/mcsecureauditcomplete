﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Member
{
    [Serializable]
    [Table("mReferenceMaterialType")]
    public class mReferenceMaterialType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string TypeText { get; set; }
        public string TypeTextLocal { get; set; }
        public bool? IsActive { get; set; }
    }
}
