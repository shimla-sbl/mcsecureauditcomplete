﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Member
{
    [Serializable]
    [Table("mMemberAccountsDetails")]
    public class mMemberAccountsDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int MemberCode { get; set; }
        [Display(Name = "PAN Number")]
        public string PanNumber { get; set; }

        [NotMapped]
        [Display(Name = "Name")]
        public string MemberName { get; set; }

        [NotMapped]
        public string ConstituencyName { get; set; }

        [NotMapped]
        public int constituencyCode { get; set; }

        [NotMapped]
        public string MemberAddress { get; set; }

        [NotMapped]
        public string MobileNumber { get; set; }

        public int? PhotoCode { get; set; }

        [NotMapped]
        public string MemberDetail { get; set; }

        public int? RoomNo { get; set; }

        //[Required(ErrorMessage = "Please Enter EPABX Number")]
        public long? EPABXNo { get; set; }

        //[Required(ErrorMessage = "Please Enter HBA Account Number")]
        public long? HBAAccountNo { get; set; }

        //[Required(ErrorMessage = "Please Enter MCA Account Number")]
        public long? MCAAccountNo { get; set; }

        //[Required(ErrorMessage = "Please Enter Bank Name")]
        //[RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$", ErrorMessage = "Numbers and special characters are not allowed in the Bank Name.")]
        public string BankName { get; set; }

        //[Required(ErrorMessage = "Please Enter Bank Account Number")]
        public long? AccountNo { get; set; }

        public string AccountNoS { get; set; }

        //[Required(ErrorMessage = "Please Enter IFSC Code")]
        public string IFSCCode { get; set; }

        public bool NomineeActive { get; set; }

        //[Required(ErrorMessage = "Please Enter Nominee Name")]

        public string NomineeName { get; set; }

        public string nameOfTreasury { get; set; }

  //      public string PPONO { get; set; }

        [Display(Name = "Nominee's RelationShip")]
        //[Required(ErrorMessage = "Please Select Relationship with Nominee")]
        public string NomineeRelationShip { get; set; }

        //[Required(ErrorMessage = "Please Enter Nominee Bank Name")]

        public string NomineeBankName { get; set; }

        //[Required(ErrorMessage = "Please Enter Nominee's Account No")]
        public long? NomineeAccountNo { get; set; }
       public string NomineeAccountNoS { get; set; }
        //[Required(ErrorMessage = "Please Enter Nominee's Bank IFSC Code")]
        public string NomineeIFSCCode { get; set; }
         [Display(Name = "PPO Number")]
        public string PPONum { get; set; }

         [Display(Name = "Payee Code")]
         public string MemberPayeeCode { get; set; }

        [NotMapped]
        public string designation { get; set; }

        [NotMapped]
        public IEnumerable<mMemberNominee> mMemberNominee { get; set; }
    }
}