﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Member
{
    [Serializable]
    [Table("tReferenceMaterial")]
    public class tReferenceMaterial
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int? AssemblyID { get; set; }
        public int? SessionID { get; set; }
        public int? MemberCode { get; set; }
        public string ReferenceMaterialText { get; set; }
        public string ReferenceMaterialTextLocal { get; set; }
        public int? ReferenceMaterialTypeId { get; set; }
        public string ReferenceMaterialAttachment { get; set; }
        public string ReferenceMaterialDescription { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateUser { get; set; }
        public bool? IsActive { get; set; }
          [NotMapped]
        public string MaterialTypeName { get; set; }
          [NotMapped]
          public string MemberName { get; set; }
          [NotMapped]
          public string Mode { get; set; }
          [NotMapped]
          public string IsAttachment { get; set; }
        
        [NotMapped]
          public List<tReferenceMaterial> ReferenceMaterialList { get; set; }
        [NotMapped]
        public List<mReferenceMaterialType> MaterialTypeList { get; set; }
        [NotMapped]
        public List<SBL.DomainModel.ComplexModel.QuestionModelCustom> memberList { get; set; }
    }
}
