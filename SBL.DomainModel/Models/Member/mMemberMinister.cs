﻿namespace SBL.DomainModel.Models.Member
{
    using SBL.DomainModel.Models.Assembly;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("mMemberMinister")]
    [Serializable]
    public partial class mMemberMinister
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AssemblyID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MemberID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MinisteryID { get; set; }

        [StringLength(7)]
        public string DepartmentID { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public virtual mAssembly mAssembly { get; set; }
    }
}
