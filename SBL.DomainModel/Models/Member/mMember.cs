namespace SBL.DomainModel.Models.Member
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;

    #endregion

    [Serializable]
    [Table("mMembers")]
    public partial class mMember
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MemberID { get; set; }
        public int MemberCode { get; set; }
        public bool? Active { get; set; }
        public bool? IsDeleted { get; set; }
        public string LoginID { get; set; }
        public string CmRefnicVipCode { get; set; }
        public string Prefix { get; set; }
        public string Name { get; set; }
        public string NameLocal { get; set; }

        public string District { get; set; }

        public int? DistrictID { get; set; }

        public string ShimlaAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string Sex { get; set; }
        public string TelOffice { get; set; }
        public string TelResidence { get; set; }
        public string Mobile { get; set; }

        public string AlternateMobile { get; set; }
        public string Email { get; set; }
        public string FatherName { get; set; }
        public string Description { get; set; }
        public string AadhaarNo { get; set; }

        public string StateName { get; set; }

        public int StateNameID { get; set; }

        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

        [NotMapped]
        public string GetStateName { get; set; }

        [NotMapped]
        public string GetDistrictName { get; set; }

        [NotMapped]
        public string GetHighestQualificationName { get; set; }
        //added by durgesh for loan module 

        [DefaultValue(true)]
        public bool isAlive { get; set; }

        //Added by umang
        public DateTime? memdob { get; set; }

        public int? HighestQualificationID { get; set; }
        public string PreviousQualification { get; set; }
        public string DeactivationReason { get; set; }
        public string Remarks { get; set; }


        public string FileName { get; set; }
        public string ThumbName { get; set; }

        public string FilePath { get; set; }
        [NotMapped]
        public string ImageAcessurl { get; set; }

        public int MonthlySMSQuota { get; set; }

        //added by durgesh for pension  


        public string PPONO { get; set; }
        public string NameofTreasury { get; set; }
    
        public bool? IsAuthorty { get; set; }
          public int? Order { get; set; }
          public string MlaSecretCode_ForUReg { get; set; }
          [NotMapped]
          public string ConsistuencyCode { get; set; }
          [NotMapped]
          public string ConsistuencyName { get; set; }
          [NotMapped]
          public string PartyName { get; set; }

    }
    [Serializable]
    public class DepartmentMembers
    {
        public int MemberID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }

    }
}
