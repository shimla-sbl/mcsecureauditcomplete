﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SBL.DomainModel.Models.Member
{
    [Serializable]
    [Table("tLokSabhaandRajyaSabhaMembers")]
    public partial class tLokSabhaandRajyaSabhaMember
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int MemberCode { get; set; }
        public int PartyCode { get; set; }
        public string AssemblyCode { get; set; }
        public string Name { get; set; }
        public string Period { get; set; }
        public string ElectedTo { get; set; }
    }
}
