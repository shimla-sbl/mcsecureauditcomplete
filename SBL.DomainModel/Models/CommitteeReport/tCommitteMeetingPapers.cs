﻿using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.eFile;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.CommitteeReport
{
    [Table("tCommitteMeetingPapers")]
    [Serializable]
    public class tCommitteMeetingPapers
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }       

        public int? PapersId { get; set; }

        public DateTime? AssignedDate { get; set; }

        public string Subject { get; set; }

        public string RefNo { get; set; }

        public int? BranchId { get; set; }

        public string PdfPath { get; set; }       

        public string CreatedBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string PaperType { get; set; }        

        [NotMapped]
        public List<tCommitteMeetingPapers> tCommitteMeetingPapersList { get; set; }        

    }

    [Serializable]
    public class CommitteMeetingPapersModel
    {
        public int Id { get; set; }
        public int? PapersId { get; set; }
        public DateTime? AssignedDate { get; set; }
        public string StrAssignedDate { get; set; }
        public string Subject { get; set; }
        public string RefNo { get; set; }
        public int? BranchId { get; set; }
        public string PdfPath { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string PaperType { get; set; }
        public string eFileNumber { get; set; }
        public string deptID { get; set; }
        public string DepartmentName { get; set; }
        public string RecvDetails { get; set; }
        public string DeptAbbr { get; set; }
        public int? ReFileID { get; set; }
        public int? eFileID { get; set; }
        public string PType { get; set; }
        public string ReplyStatus { get; set; }
        public string PaperRefNo { get; set; }
        public string LinkedRefNo { get; set; }
        public string DocuemntPaperType { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public string eFilePath { get; set; }
        public string eFilePathWord { get; set; }
        public string PaperStatus { get; set; }
        public string MainEFileName { get; set; }
        public string eFileName { get; set; }
        public int? eFileAttachmentId { get; set; }

        public string Name { get; set; }
        public int Length { get; set; }
        public string Type { get; set; }
        public string AssignAllDates { get; set; }


    }


    [Serializable]
    public class CommittePapersLaidModel
    {
        public Int64 PaperLaidId { get; set; }
        public string Title { get; set; }
        public int? AssemblyId { get; set; }
        public int? SessionId { get; set; }
        public int? EventId { get; set; }      
        public int? BranchId { get; set; }
        public int? MinistryId { get; set; }
        public int? PaperTypeID { get; set; }
        public DateTime? DesireLayingDate { get; set; }
        public string Description { get; set; }
        public string DepartmentId { get; set; }
        public int? CommitteeId { get; set; }
        public string ProvisionUnderWhich { get; set; }
        public string Remark { get; set; }
        public int? CommitteeActivePaeperId { get; set; }
        public int? IsLaid { get; set; }
        public int? Version { get; set; }
        public string FilePath { get; set; }
        public string SignedFilePath { get; set; }
        public string CommtSubmittedDate { get; set; }
        public int? DocFileVersion { get; set; }
        public string DocFileName { get; set; }
        public string evidhanReferenceNumber { get; set; }
        

        public string Name { get; set; }
        public int Length { get; set; }
        public string Type { get; set; }


    }




}
