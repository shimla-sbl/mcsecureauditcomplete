﻿using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.eFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.CommitteeReport
{
    public class npHouseCommitteeFiles
    {
        public int Id { get; set; }
        public int eFileID { get; set; }
        public string eFileName { get; set; }
        public int? DocumentTypeId { get; set; }

        public string Description { get; set; }
        public string Title { get; set; }
        public bool SendStatus { get; set; }
        public string ByDepartmentId { get; set; }
        public string ToDepartmentID { get; set; }
        public int? ParentId { get; set; }
        public int? ByOfficeCode { get; set; }
        public int? ToOfficeCode { get; set; }
        public int? PaperNature { get; set; }
        public DateTime? SendDateToDept { get; set; }
        public DateTime? ReceivedDateByDept { get; set; }
        public string ItemName { get; set; }
        public int? ItemId { get; set; }
        public int? CommitteeId { get; set; }
        public string CommitteeName { get; set; }
        public string eFilePath { get; set; }
        public string ReplyPdfPath { get; set; }
        public string AnnexPdfPath { get; set; }
        public string AnnexFilesNameByUser { get; set; }
        public string DocFilePath { get; set; }
        public string DocFilesNameByUser { get; set; }
        public string PaperRefNo { get; set; }
        public string LinkedRefNo { get; set; }
        public bool IsDeleted { get; set; }
        public int? CurYear { get; set; }
        public int? CurrentBranchId { get; set; }
        public string CurrentEmpAadharID { get; set; }
        public string SelectedPaperDate { get; set; }
        public int? LetterStatusType { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string DescriptionRO { get; set; }
        public List<tCommittee> tCommitteeList { get; set; }

        public bool? IsVerified { get; set; }

        public string RejectReason { get; set; }

        public int RecordId { get; set; }

        public string FileAccessingUrlPath { get; set; }
      
        public string ToDepartmentNameAbbrev { get; set; }
     
        public string CommitteeNameAbbrev { get; set; }  
        

        public string eFileNumber { get; set; }

        public string eFileFromYear { get; set; }

        public string eFileToYear { get; set; }

        public string eFileSubject { get; set; }

        public int? PreviousLinkId { get; set; }

        public int? FutureLinkId { get; set; }

        public string PreviousFileNumber { get; set; }

        public string FutureFileNumber { get; set; }

        public List<pHouseCommitteeFiles> HouseCommAllFilesList { get; set; }

        public string CurrentDeptId { get; set; }

        public string ToDepartmentName { get; set; }

        public string ByDepartmentName { get; set; }

        public List<tCommittee> CommitteeList = new List<tCommittee>();

        public List<eFileList> eFileLists = new List<eFileList>();

        public List<mDepartment> DepartmentList = new List<mDepartment>();

        public List<eFilePaperType> eFilePaperTypeList = new List<eFilePaperType>();

        public List<mCommitteeReplyItemType> mCommitteeReplyItemTypeList = new List<mCommitteeReplyItemType>();

        public int AssemblyID { get; set; }

        public int? SessionID { get; set; }

        public string PendencyIDs { get; set; }
    }
}
