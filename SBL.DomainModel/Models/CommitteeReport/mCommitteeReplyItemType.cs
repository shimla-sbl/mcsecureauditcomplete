﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.CommitteeReport
{
    [Table("mCommitteeReplyItemType")]
    [Serializable]
    public class mCommitteeReplyItemType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string ItemTypeName { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreationDate { get; set; }

        public string CreationBy { get; set; }
    }
}
