﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.CommitteeReport
{
    [Table("mCommitteeReplySubItemType")]
    [Serializable]
    public class mCommitteeReplySubItemType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string SubItemTypeName { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreationDate { get; set; }

        public string CreationBy { get; set; }
    }
}
