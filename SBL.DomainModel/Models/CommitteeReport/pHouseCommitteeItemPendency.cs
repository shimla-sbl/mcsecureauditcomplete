﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.eFile;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Trirand.Web.Mvc;

namespace SBL.DomainModel.Models.CommitteeReport
{
    [Table("pHouseCommitteeItemPendency")]
    [Serializable]
    public class pHouseCommitteeItemPendency
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PendencyId { get; set; }
        public int? ParentId { get; set; }
        

        public int? CommitteeId { get; set; }

        public string SentByDepartmentId { get; set; }

        //public string SentToDepartmentId { get; set; }

        public int? ItemTypeId { get; set; } //Assurance or Audit para

        public string ItemTypeName { get; set; } //Assurance or Audit para

        public int? SubItemTypeId { get; set; } // Audit para

        public string SubItemTypeName { get; set; } // Audit para

        public string ItemNumber { get; set; } //Assurance or Audit No.

        public int? ItemNatureId { get; set; }//1/2/3/4

        public string ItemNature { get; set; } //Ori/Rem1/Rem2
        public string CurrentDeptId { get; set; }

        //public string TypeNo { get; set; }//report or type no

        //[UIHint("tinymce_jquery_full_LOB"), AllowHtml]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string ItemDescription { get; set; }


        public string FinancialYear { get; set; }
        public string CAGYear { get; set; }

        public DateTime? SentOnDate { get; set; }
        //public string SentOnDateByVS { get; set; }
        public string AnnexPdfPath { get; set; }
        public string AnnexFilesNameByUser { get; set; }
        public string ItemReplyPdfPath { get; set; }
        public string ItemReplyPdfNameByUser { get; set; }
        public string DocFilePath { get; set; }
        public string DocFileName { get; set; }
        //public DateTime? LaidInTheHouse { get; set; }

        //public string CagPageNo { get; set; }

        // [NotMapped]
        //public string mode { get; set; }

        [NotMapped]
        public string SentOnDateVIew { get; set; }
        [NotMapped]
        public int MainPendencyId { get; set; }
        [NotMapped]
        public string FileAccessingUrlPath { get; set; }
        //public DateTime? LastReminderSentDate { get; set; }

        [NotMapped]
        public string LastReminderSentDateView { get; set; }

        //public int? LatestReplyId { get; set; }

        public int? eFileID { get; set; }



        [NotMapped]
        public string eFileSubject { get; set; }

        [NotMapped]
        public string eFileNumber { get; set; }
        [NotMapped]
        public int SubPendencyId { get; set; }
        [NotMapped]
        public string CommitteeName { get; set; }
        //public string CoverLetterPdfPath { get; set; }

        public bool IsFreeze { get; set; }
        public bool IsLocked { get; set; }
        public bool IsLinked { get; set; }
        public int? Linked_RecordId { get; set; }

        public int Status { get; set; }
        public Guid? CreatedBy { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy HH:mm:ss}")]
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        

        [NotMapped]
        public bool ReplyPdfIsChange { get; set; }
        [NotMapped]
        public bool AnnexPdfIsChange { get; set; }
        [NotMapped]
        public bool DocFileIsChange { get; set; }


        [NotMapped]
        public string StatusName { get; set; }

        [NotMapped]
        public string DepartmentName { get; set; }

        [NotMapped]
        public string AbbrDeptName { get; set; }

        [NotMapped]
        public List<tCommitteReplyPendency> tCommittePendencyList { get; set; }

        [NotMapped]
        public List<pHouseCommitteeItemPendency> HouseCommitteeItemPendency { get; set; }
        [NotMapped]
        public List<mSession> SessionList { get; set; }
        [NotMapped]
        public List<mAssembly> AssmeblyList { get; set; }
        [NotMapped]
        public List<mSessionDate> SessionDateList { get; set; }

        [NotMapped]
        public List<mDepartment> mDepartmentList { get; set; }

        [NotMapped]
        public List<tCommittee> tCommitteeList { get; set; }

        [NotMapped]
        public List<mCommitteeReplyItemType> mCommitteeReplyItemTypeList { get; set; }

        [NotMapped]
        public List<mCommitteeReplySubItemType> mCommitteeReplySubItemTypeList { get; set; }

        [NotMapped]
        public List<eFilePaperType> eFilePaperTypeList { get; set; }

        [NotMapped]
        public List<eFilePaperNature> eFilePaperNatureList { get; set; }

        [NotMapped]
        public List<mCommitteeReplyStatus> mCommitteeReplyStatusList { get; set; }

        [NotMapped]
        public List<tCommitteReplyByMaster> mCommitteeReplyMasterList { get; set; }

        [NotMapped]
        public string CheckValue { get; set; }

        [NotMapped]
        public string CheckValueDept { get; set; }

        [NotMapped]
        public string CheckValueAssemblyId { get; set; }

        [NotMapped]
        public string CheckValueSessionId { get; set; }

        [NotMapped]
        public DateTime? CheckValueSessionDate { get; set; }

        [NotMapped]
        public string CheckFinancialYear { get; set; }

        [NotMapped]
        public int? CheckItemTypeId { get; set; }

        [NotMapped]
        public int page { get; set; }

        [NotMapped]
        public int rows { get; set; }

        [NotMapped]
        public string sidx { get; set; }

        [NotMapped]
        public string sord { get; set; }

        [NotMapped]
        public int totalRecordsCount { get; set; }

        //For new eFile
        [NotMapped]
        public string FromYear { get; set; }
        [NotMapped]
        public string ToYear { get; set; }
        [NotMapped]
        public int eFileAttachmentID { get; set; }
        [NotMapped]
        public List<SBL.DomainModel.Models.eFile.eFileList> eFileList { get; set; }
        [NotMapped]
        public List<SBL.DomainModel.Models.eFile.eFileListAll> eFileListAll { get; set; }

    }


}
