﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Committee
{
    public class npHouseCommitteItemPendency
    {
        [Key]
        public int PendencyId { get; set; }
        public int? ParentId { get; set; }
        public int? CommitteeId { get; set; }
        public string SentByDepartmentId { get; set; }

        public int? ItemTypeId { get; set; } //Assurance or Audit para

        public string ItemTypeName { get; set; } //Assurance or Audit para

        public int? SubItemTypeId { get; set; } // Audit para

        public string SubItemTypeName { get; set; } // Audit para

        public string ItemNumber { get; set; } //Assurance or Audit No.

        public int? ItemNatureId { get; set; }//1/2/3/4

        public string ItemNature { get; set; } //Ori/Rem1/Rem2
        public string CurrentDeptId { get; set; }


        public string ItemDescription { get; set; }


        public string FinancialYear { get; set; }
        public string CAGYear { get; set; }

        public DateTime? SentOnDate { get; set; }
        //public string SentOnDateByVS { get; set; }
        public string AnnexPdfPath { get; set; }
        public string AnnexFilesNameByUser { get; set; }
        public string ItemReplyPdfPath { get; set; }
        public string ItemReplyPdfNameByUser { get; set; }
        public string DocFilePath { get; set; }
        public string DocFileName { get; set; }

        public string SentOnDateVIew { get; set; }

        public int MainPendencyId { get; set; }

        public string FileAccessingUrlPath { get; set; }



        public string LastReminderSentDateView { get; set; }

        //public int? LatestReplyId { get; set; }

        public int? eFileID { get; set; }




        public string eFileSubject { get; set; }


        public string eFileNumber { get; set; }

        public int SubPendencyId { get; set; }

        public string CommitteeName { get; set; }
        //public string CoverLetterPdfPath { get; set; }

        public bool IsFreeze { get; set; }
        public bool IsLocked { get; set; }
        public bool IsLinked { get; set; }
        public int? Linked_RecordId { get; set; }

        public int Status { get; set; }
        public Guid? CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }


        public bool ReplyPdfIsChange { get; set; }

        public bool AnnexPdfIsChange { get; set; }

        public bool DocFileIsChange { get; set; }



        public string StatusName { get; set; }


        public string DepartmentName { get; set; }


        public string AbbrDeptName { get; set; }









        public string CheckValue { get; set; }


        public string CheckValueDept { get; set; }


        public string CheckValueAssemblyId { get; set; }


        public string CheckValueSessionId { get; set; }

        public DateTime? CheckValueSessionDate { get; set; }


        public string CheckFinancialYear { get; set; }

        public int? CheckItemTypeId { get; set; }


        public int page { get; set; }


        public int rows { get; set; }


        public string sidx { get; set; }


        public string sord { get; set; }


        public int totalRecordsCount { get; set; }

        //For new eFile

        public string FromYear { get; set; }

        public string ToYear { get; set; }

        public int eFileAttachmentID { get; set; }

        public List<SBL.DomainModel.Models.eFile.eFileList> eFileList { get; set; }

        public List<SBL.DomainModel.Models.eFile.eFileListAll> eFileListAll { get; set; }





    }
}
