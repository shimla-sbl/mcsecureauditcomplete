﻿using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.eFile;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Trirand.Web.Mvc;

namespace SBL.DomainModel.Models.CommitteeReport
{
    [Table("tCommitteReplyPendency")]
    [Serializable]
    public class tCommitteReplyPendency
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PendencyId { get; set; }

        //public string PendencyBy { get; set; }//report or item ///this is type

        public int AssemblyID { get; set; }

        public int? SessionID { get; set; }

        public DateTime? SessionDate { get; set; }

        public int CommitteeId { get; set; }

        public string DepartmentId { get; set; }

        public int? ItemTypeId { get; set; }

        public string TypeNo { get; set; }//report or type no

        //[UIHint("tinymce_jquery_full_LOB"), AllowHtml]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string ItemDescription { get; set; }

        public int? ReportTypeId { get; set; }

        public int? ReportNatureId { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Subject { get; set; }

        //[NotMapped]//was confliction so made new property
        //[UIHint("tinymce_jquery_Subject"), AllowHtml]
        //public string SubjectDeptEdit { get; set; }

        public string FinancialYear { get; set; }

        public DateTime? SentOnDate { get; set; }

        public DateTime? LaidInTheHouse { get; set; }

        public string CagPageNo { get; set; }

        // [NotMapped]
        //public string mode { get; set; }

        [NotMapped]
        public string SentOnDateVIew { get; set; }

        public DateTime? LastReminderSentDate { get; set; }

        [NotMapped]
        public string LastReminderSentDateView { get; set; }

        public int? LatestReplyId { get; set; }

        public int? FileNumber { get; set; }

        [NotMapped]
        public int eFileID { get; set; }

        [NotMapped]
        public string eFileSubject { get; set; }

        [NotMapped]
        public string eFileNumber { get; set; }

        public string CoverLetterPdfPath { get; set; }

        public bool IsFreeze { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedDate { get; set; }

        public int Status { get; set; }

        public int? OrderString1 { get; set; }

        public int? OrderString2 { get; set; }

        public int? OrderString3 { get; set; }

        public int? OrderString4 { get; set; }

        public int? OrderString5 { get; set; }

        public int? OrderString6 { get; set; }

        public int? OrderString7 { get; set; }

        public int? OrderString8 { get; set; }

        [NotMapped]
        public string StatusName { get; set; }

        [NotMapped]
        public string DepartmentName { get; set; }

        [NotMapped]
        public string AbbrDeptName { get; set; }

        [NotMapped]
        public List<tCommitteReplyPendency> tCommittePendencyList { get; set; }

        [NotMapped]
        public List<mDepartment> mDepartmentList { get; set; }

        [NotMapped]
        public List<tCommittee> tCommitteeList { get; set; }

        [NotMapped]
        public List<mCommitteeReplyItemType> mCommitteeReplyItemTypeList { get; set; }

        [NotMapped]
        public List<eFilePaperType> eFilePaperTypeList { get; set; }

        [NotMapped]
        public List<eFilePaperNature> eFilePaperNatureList { get; set; }

        [NotMapped]
        public List<mCommitteeReplyStatus> mCommitteeReplyStatusList { get; set; }

        [NotMapped]
        public List<tCommitteReplyByMaster> mCommitteeReplyMasterList { get; set; }

        [NotMapped]
        public string CheckValue { get; set; }

        [NotMapped]
        public string CheckValueDept { get; set; }

        [NotMapped]
        public string CheckValueAssemblyId { get; set; }

        [NotMapped]
        public string CheckValueSessionId { get; set; }

        [NotMapped]
        public DateTime? CheckValueSessionDate { get; set; }

        [NotMapped]
        public string CheckFinancialYear { get; set; }

        [NotMapped]
        public int? CheckItemTypeId { get; set; }

        [NotMapped]
        public int page { get; set; }

        [NotMapped]
        public int rows { get; set; }

        [NotMapped]
        public string sidx { get; set; }

        [NotMapped]
        public string sord { get; set; }

        [NotMapped]
        public int totalRecordsCount { get; set; }

        //For new eFile
        [NotMapped]
        public string FromYear { get; set; }
        [NotMapped]
        public string ToYear { get; set; }
        [NotMapped]
        public int eFileAttachmentID { get; set; }
        [NotMapped]
        public List<SBL.DomainModel.Models.eFile.eFileList> eFileList { get; set; }
        [NotMapped]
        public List<SBL.DomainModel.Models.eFile.eFileListAll> eFileListAll { get; set; }

    }

    public class ComplexReplyPendency
    {
        public JQGrid PostGrid;
        public ComplexReplyPendency()
        {
            PostGrid = new JQGrid
            {
                Columns = new List<JQGridColumn>()
                {
                      new JQGridColumn { DataField = "FinancialYear" },                   
                    new JQGridColumn { DataField = "AbbrDeptName" }, 
                    new JQGridColumn { DataField = "TypeNo" },                   
                     new JQGridColumn { DataField = "Subject" },
                       new JQGridColumn { DataField = "StatusName" },
                         new JQGridColumn { DataField = "eFileNumber" }, 
                    new JQGridColumn { DataField = "eFileSubject" },          
                   new JQGridColumn { DataField = "eFileID" }, 
                   new JQGridColumn { DataField = "ItemTypeId" },
                        new JQGridColumn { DataField = "DepartmentName" },
                        new JQGridColumn { DataField = "PendencyId" },
                }
            };
        }
    }

    [Serializable]
    public class tCommitteReplyPendency1
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PendencyId { get; set; }

        //public string PendencyBy { get; set; }//report or item ///this is type

        public int AssemblyID { get; set; }

        public int? SessionID { get; set; }

        public DateTime? SessionDate { get; set; }

        public int CommitteeId { get; set; }

        public string DepartmentId { get; set; }

        public int? ItemTypeId { get; set; }

        public string TypeNo { get; set; }//report or type no

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string ItemDescription { get; set; }

        public int? ReportTypeId { get; set; }

        public int? ReportNatureId { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Subject { get; set; }

        //[NotMapped]//was confliction so made new property
        //[UIHint("tinymce_jquery_Subject"), AllowHtml]
        //public string SubjectDeptEdit { get; set; }

        public string FinancialYear { get; set; }

        public DateTime? SentOnDate { get; set; }

        public DateTime? LaidInTheHouse { get; set; }

        public string CagPageNo { get; set; }

        // [NotMapped]
        //public string mode { get; set; }

        [NotMapped]
        public string SentOnDateVIew { get; set; }

        public DateTime? LastReminderSentDate { get; set; }

        [NotMapped]
        public string LastReminderSentDateView { get; set; }

        public int? LatestReplyId { get; set; }

        public int? FileNumber { get; set; }

        [NotMapped]
        public int eFileID { get; set; }

        [NotMapped]
        public string eFileSubject { get; set; }

        [NotMapped]
        public string eFileNumber { get; set; }

        public string CoverLetterPdfPath { get; set; }

        public bool IsFreeze { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedDate { get; set; }

        public int Status { get; set; }

        public int? OrderString1 { get; set; }

        public int? OrderString2 { get; set; }

        public int? OrderString3 { get; set; }

        public int? OrderString4 { get; set; }

        public int? OrderString5 { get; set; }

        public int? OrderString6 { get; set; }

        public int? OrderString7 { get; set; }

        public int? OrderString8 { get; set; }

        [NotMapped]
        public string StatusName { get; set; }

        [NotMapped]
        public string DepartmentName { get; set; }

        [NotMapped]
        public string AbbrDeptName { get; set; }

        [NotMapped]
        public List<tCommitteReplyPendency> tCommittePendencyList { get; set; }

        [NotMapped]
        public List<mDepartment> mDepartmentList { get; set; }

        [NotMapped]
        public List<tCommittee> tCommitteeList { get; set; }

        [NotMapped]
        public List<mCommitteeReplyItemType> mCommitteeReplyItemTypeList { get; set; }

        [NotMapped]
        public List<eFilePaperType> eFilePaperTypeList { get; set; }

        [NotMapped]
        public List<eFilePaperNature> eFilePaperNatureList { get; set; }

        [NotMapped]
        public List<mCommitteeReplyStatus> mCommitteeReplyStatusList { get; set; }

        [NotMapped]
        public List<tCommitteReplyByMaster> mCommitteeReplyMasterList { get; set; }

        [NotMapped]
        public string CheckValue { get; set; }

        [NotMapped]
        public string CheckValueDept { get; set; }

        [NotMapped]
        public string CheckValueAssemblyId { get; set; }

        [NotMapped]
        public string CheckValueSessionId { get; set; }

        [NotMapped]
        public DateTime? CheckValueSessionDate { get; set; }

        [NotMapped]
        public string CheckFinancialYear { get; set; }

        [NotMapped]
        public int? CheckItemTypeId { get; set; }

        [NotMapped]
        public int page { get; set; }

        [NotMapped]
        public int rows { get; set; }

        [NotMapped]
        public string sidx { get; set; }

        [NotMapped]
        public string sord { get; set; }

        [NotMapped]
        public int totalRecordsCount { get; set; }

    }
}
