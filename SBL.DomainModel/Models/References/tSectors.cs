﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.References
{
    
    [Serializable]
    [Table("tSectors")]
    public class tSectors
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

       // public string SectorType { get; set; }

        public string Title { get; set; }
        public string IconPath { get; set; }
        public bool Active { get; set; }
    }
}

