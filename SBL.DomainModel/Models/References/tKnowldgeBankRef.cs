﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.References
{
    [Serializable]
    [Table("tKnowldgeBankRef")]
    public class tKnowldgeBankRef
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int? SectorId { get; set; }

        public int CollectionTypeId { get; set; }

        public string Description { get; set; }

        public string FileLocation { get; set; }

        public DateTime SubmittedDate { get; set; }

        public int RegionId { get; set; }
    }
}
