namespace SBL.DomainModel.Models.Rule
{
    #region Namespace Reffrences
    #endregion

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("mRules")]
    public partial class mRule
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RuleId { get; set; }

        [StringLength(250)]
        public string RuleName { get; set; }

        [StringLength(250)]
        public string RuleNameLocal { get; set; }

        public string RuleDescription { get; set; }

        public bool IsActive { get; set; }

        public string PDFPath { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
