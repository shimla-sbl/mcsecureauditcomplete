﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.ItemSubscription
{
    [Table("mItemSubsDelivery")]
    [Serializable]
    public class mItemSubsDelivery
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemSubsDeliveryID { get; set; }
        [Required]
        public int ItemSubscriptionId { get; set; }
        [Required]
        public int QuantityReceived { get; set; }
        [Required]
        public string Year { get; set; }
        [Required]
        public DateTime DateOfReceiving { get; set; }
        [Required]
        public string Building { get; set; }
        [Required]
        public string Floor { get; set; }
        [Required]
        public string Almirah { get; set; }
      
        public string Rack { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public int Month { get; set; }

        public string Volume { get; set; }

        public string Number { get; set; }

        [NotMapped]
        public string ItemName { get; set; }
        [NotMapped]
        public string ItemType { get; set; }
        [NotMapped]
        public string Language { get; set; }
        [NotMapped]
        public string Quantity { get; set; }
        [NotMapped]
        public string Frequancy { get; set; }
    }
}
