﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.ItemSubscription
{
    [Table("mItemSubscription")]
    [Serializable]
    public class mItemSubscription
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemSubscriptionID { get; set; }

        [Required]
        public string ItemName { get; set; }

        [Required]
        public string ItemType { get; set; }

        [Required]
        public string Language { get; set; }

        [Required]
        public string Frequancy { get; set; }
        [Required]
        public string Quantity { get; set; }
        [Required]
        public DateTime? SubsStartDate { get; set; }
        [Required]
        public DateTime? SubsEndDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
    }
}
