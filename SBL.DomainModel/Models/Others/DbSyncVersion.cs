﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Others
{
    [Table("ct_track")]
    [Serializable]
    public class DbSyncVersion
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int cttrackID { get; set; }
        public int? clientID { get; set; }
        public int? ClientLastUpdateCTID { get; set; }
        public DateTime? DateTime { get; set; } 
    }
}
