﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Passes
{
    [Serializable]
    [Table("mPasses")]
    public class mPasses : PhotoCaptureGenericModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PassID { get; set; }

        public Int32? PassCode { get; set; }

        public int RequestedPassCategoryID { get; set; }

        public int ApprovedPassCategoryID { get; set; }

        public int AssemblyCode { get; set; }

        public int SessionCode { get; set; }

        public string Prefix { get; set; }

        public string AadharID { get; set; }

        [Required]
        public string Name { get; set; }

        public string Gender { get; set; }

        public int? Age { get; set; }

        public string FatherName { get; set; }

        public int? NoOfPersions { get; set; }

        public string RecommendationType { get; set; }

        public string RecommendationBy { get; set; }

        public string RecommendationDescription { get; set; }

        public string MobileNo { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string OrganizationName { get; set; }

        public string Designation { get; set; }

        public string DayTime { get; set; }

        public TimeSpan Time { get; set; }

        public string DepartmentID { get; set; }

        public string SessionDateFrom { get; set; }

        public string SessionDateTo { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public string ApprovedBy { get; set; }

        public string VehicleNumber { get; set; }

        public string GateNumber { get; set; }

        public bool IsRequested { get; set; }
        public bool IsLocal { get; set; }
        public DateTime? RequestedDate { get; set; }

        [NotMapped]
        public int passStatus { get; set; }

        public int Status { get; set; }

        public string RequestedBy { get; set; }

        public bool IsActive { get; set; }

        public bool IsApproved { get; set; }

        public DateTime? RejectionDate { get; set; }

        public string RejectionReason { get; set; }

        /// <summary>
        /// Create for Dropdown List value --sanjay
        /// </summary>
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mAssembly> mAssemblyList { get; set; }
        [NotMapped]
        public DateTime? SessionDateF { get; set; }
        [NotMapped]
        public DateTime? SessionDateT { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }

        [NotMapped]
        public string PassTypeName { get; set; }
        [NotMapped]
        public int TotalOfTotalcount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AssesmblyName { get; set; }
        [NotMapped]
        public int TotalCount { get; set; }
        public string RequestID { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string IsRequestUserID { get; set; }

    }


    [Serializable]
    [Table("mPassesDeleted")]
    public class mPassesDeleted : PhotoCaptureGenericModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PassID { get; set; }
        public Int32? OrgPassCode { get; set; }
        public Int32? PassCode { get; set; }

        public int RequestedPassCategoryID { get; set; }

        public int ApprovedPassCategoryID { get; set; }

        public int AssemblyCode { get; set; }

        public int SessionCode { get; set; }

        public string Prefix { get; set; }

        public string AadharID { get; set; }

        [Required]
        public string Name { get; set; }

        public string Gender { get; set; }

        public int? Age { get; set; }

        public string FatherName { get; set; }

        public int? NoOfPersions { get; set; }

        public string RecommendationType { get; set; }

        public string RecommendationBy { get; set; }

        public string RecommendationDescription { get; set; }

        public string MobileNo { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string OrganizationName { get; set; }

        public string Designation { get; set; }

        public string DayTime { get; set; }

        public TimeSpan Time { get; set; }

        public string DepartmentID { get; set; }

        public string SessionDateFrom { get; set; }

        public string SessionDateTo { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public string ApprovedBy { get; set; }

        public string VehicleNumber { get; set; }

        public string GateNumber { get; set; }

        public bool IsRequested { get; set; }

        public DateTime? RequestedDate { get; set; }

        [NotMapped]
        public int passStatus { get; set; }

        public int Status { get; set; }

        public string RequestedBy { get; set; }

        public bool IsActive { get; set; }

        public bool IsApproved { get; set; }

        public DateTime? RejectionDate { get; set; }

        public string RejectionReason { get; set; }

        /// <summary>
        /// Create for Dropdown List value --sanjay
        /// </summary>
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mAssembly> mAssemblyList { get; set; }
        [NotMapped]
        public DateTime? SessionDateF { get; set; }
        [NotMapped]
        public DateTime? SessionDateT { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }

        [NotMapped]
        public string PassTypeName { get; set; }
        [NotMapped]
        public int TotalOfTotalcount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AssesmblyName { get; set; }
        [NotMapped]
        public int TotalCount { get; set; }
        public string RequestID { get; set; }
    }
}
