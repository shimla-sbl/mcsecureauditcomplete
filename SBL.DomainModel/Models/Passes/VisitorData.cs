﻿namespace SBL.DomainModel.Models.Passes
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    #endregion

    [Serializable]
    [Table("VisitorData")]
    public class VisitorData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 VerifiedPassID { get; set; }

        public int AssemblyCode { get; set; }

        public int SessionCode { get; set; }

        public DateTime SessionDate { get; set; }

        public int PassCode { get; set; }

        public TimeSpan EntryTime { get; set; }

        public string GateNumber { get; set; }

        public Guid PoliceID { get; set; }

        public string VerificationResult { get; set; }

        public string BlockingReason { get; set; }

        public bool IsLocal { get; set; }

        [NotMapped]
        public string PassCategoryID { get; set; }
        [NotMapped]
        public string PassCategory { get; set; }
        [NotMapped]
        public string AllowPassCount { get; set; }
        [NotMapped]
        public string InvalidPassCount { get; set; }
        [NotMapped]
        public string BlockedPassCount { get; set; }
        [NotMapped]
        public string ApprovedPassCount { get; set; }
        [NotMapped]
        public string ApproveRequestedCount { get; set; }

        [NotMapped]
        public string MobileNo { get; set; }
        [NotMapped]
        public string AadharID { get; set; }
        [NotMapped]
        public string OrganizationName { get; set; }
        [NotMapped]
        public string Address { get; set; }
        [NotMapped]
        public string SessionDateFrom { get; set; }
        [NotMapped]
        public string SessionDateTo { get; set; }
        [NotMapped]
        public string ApprovedDate { get; set; }
        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string EntryTimeSl { get; set; }
        [NotMapped]
        public string EntryDate { get; set; }



    }
}
