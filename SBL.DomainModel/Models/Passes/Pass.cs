﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Media;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Passes
{
    [Serializable]
    public class Pass
    {
        [Key]
        public int PassID { get; set; }

        [ForeignKey("PassCategory")]
        public int PassCategoryID { get; set; }

      //  [ForeignKey("Assembly")]
        public int AssemblyID { get; set; }

     //   [ForeignKey("Session")]
        public int SessionID { get; set; }

        [Required]
        public string Prefix { get; set; }

        [Required]
        public string Name { get; set; }
        public string UID { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public string Address { get; set; }
        [Required]
        public string Photo { get; set; }

        [ForeignKey("Department")]
        public string DepartmentID { get; set; }

        [ForeignKey("Organization")]
        public int? OrganizationID { get; set; }
        public string OrgDeptName { get; set; }

        public DateTime ValidFrom { get; set; }
        public DateTime ValidUpto { get; set; }
        public DateTime IssueDate { get; set; }

        public string VehicleNumber { get; set; }
        public string GateNumber { get; set; }
        /// <summary>
        /// 1:Requested 2:Approved 3: Rejected
        /// </summary>
        public int Status { get; set; }

        public string RequestedBy { get; set; }
        public string ApprovedBy { get; set; }

        public bool IsActive { get; set; }

        public virtual PassCategory PassCategory { get; set; }

        public virtual mDepartment Department { get; set; }

        public virtual Organization Organization { get; set; }
       // public virtual mAssembly Assembly { get; set; }
      //  public virtual mSession Session { get; set; }
    }
}
