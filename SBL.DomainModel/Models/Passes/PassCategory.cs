﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Passes
{
    [Serializable]
    public class PassCategory
    {
        [Key]
        public int PassCategoryID { get; set; }

        //[Required]
        public string Name { get; set; }

        //[Required]
        public string Description { get; set; }

        //[Required]
        public string Template { get; set; }

        public bool ForVehicle { get; set; }

        public bool IsActive { get; set; }

        public string PassAllowsCreation { get; set; }

        //public string ModifiedBy { get; set; }

        //public DateTime? ModifiedDate { get; set; }

        //public string CreatedBy { get; set; }

        //public DateTime? CreatedDate { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
