﻿namespace SBL.DomainModel.Models.Passes
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    #endregion

    [Serializable]
    [Table("tVerifiedPasses")]
    public class tVerifiedPasses
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 VerifiedPassID { get; set; }

        public int AssemblyCode { get; set; }

        public int SessionCode { get; set; }

        public DateTime SessionDate { get; set; }

        public int PassCode { get; set; }

        public TimeSpan EntryTime { get; set; }

        public string GateNumber { get; set; }

        public Guid PoliceID { get; set; }

        public string VerificationResult { get; set; }

        public string BlockingReason { get; set; }
    }
}
