﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Passes
{
    [Serializable]
    public class PhotoCaptureGenericModel
    {
        [Required(ErrorMessage = "Photo is Required")]
        public string Photo { get; set; }

        [NotMapped]
        public string PhotoName { get; set; }

        // added by Umang
        [NotMapped]
        public string ImagePath { get; set; }
        [NotMapped]
        public string FullImage { get; set; }
        [NotMapped]
        public string ThumbImage { get; set; }
        [NotMapped]
        public string FileLocation { get; set; }
        [NotMapped]
        public string ImageLocation { get; set; }
        [NotMapped]
        public string ThumbName { get; set; }
    }
}