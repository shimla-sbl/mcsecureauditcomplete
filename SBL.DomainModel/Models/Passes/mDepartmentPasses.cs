﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Passes
{
    [Serializable]
    [Table("mDepartmentPasses")]
    public class mDepartmentPasses : PhotoCaptureGenericModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PassID { get; set; }

        public Int32? PassCode { get; set; }

        public int PassCategoryID { get; set; }

        //  [ForeignKey("Assembly")]
        public int AssemblyCode { get; set; }

        //   [ForeignKey("Session")]
        public int SessionCode { get; set; }

        public string AadharID { get; set; }

        public string Prefix { get; set; }

        [Required]
        public string Name { get; set; }

        public string Gender { get; set; }

        public int? Age { get; set; }

        public string FatherName { get; set; }

        public int? NoOfPersions { get; set; }

        public string RecommendationType { get; set; }

        public string RecommendationBy { get; set; }

        public string RecommendationDescription { get; set; }

        public string MobileNo { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        //[Required]
        //public string Photo { get; set; }

        public bool IsVehiclaPass { get; set; }

        public string OrganizationName { get; set; }

        public string Designation { get; set; }

        public string DayTime { get; set; }

        public TimeSpan? Time { get; set; }

        public string DepartmentID { get; set; }

        public String SessionDateFrom { get; set; }

        public String SessionDateTo { get; set; }

        public bool IsApproved { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public string ApprovedBy { get; set; }

        public string VehicleNumber { get; set; }

        public string GateNumber { get; set; }

        public bool IsRequested { get; set; }

        public DateTime? RequestedDate { get; set; }

        public int? Status { get; set; }

        public string RequestedBy { get; set; }

        public bool IsActive { get; set; }

        public DateTime? RejectionDate { get; set; }

        public string RejectionReason { get; set; }

        public int ApprovedPassCategoryID { get; set; }

        //Add for transfer session to Next Session - sanjay
        public bool IsSessionTransferID { get; set; }

        public string RequestdID { get; set; }
        /// <summary>
        /// Use For Next Session- 
        /// </summary>
        public string IsRequestUserID { get; set; }
        [NotMapped]
        public int IsCurrentSessionID { get; set; }
        [NotMapped]
        public int IsCurrentAssemblyID { get; set; }
    }
}
