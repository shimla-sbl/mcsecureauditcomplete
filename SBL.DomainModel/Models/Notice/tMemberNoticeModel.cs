﻿namespace SBL.DomainModel.Models.Notice
{
    #region Namespace Reffrences
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.Assembly;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;
    using SBL.DomainModel.Models.Ministery;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Event;

    #endregion

    [Serializable]
    public class tMemberNoticeModel
    {
        public int NoticeId { get; set; }

        public long? PaperLaidId { get; set; }

        public string NoticeNumber { get; set; }

        public string Notice { get; set; }

        public string NoticeLocal { get; set; }

        public int NoticeTypeID { get; set; }

        public string Subject { get; set; }

        public DateTime NoticeDate { get; set; }

        public TimeSpan NoticeTime { get; set; }

        public DateTime? FixedDate { get; set; }

        public string NoticeReply { get; set; }

        public string NoticeReplyPath { get; set; }

        public string DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public string DepartmentNameLocal { get; set; }

        public int? MinistryId { get; set; }

        public int? MinsiterId { get; set; }

        public string MinisterName { get; set; }

        public string MinisterNameLocal { get; set; }

        public int? MemberId { get; set; }

        public string MemberName { get; set; }

        public string MemberNameLocal { get; set; }

        public string BusinessType { get; set; }
        public DateTime? LaidOnDate { get; set; }

        #region New Variables
        //Location of the path as we will let the Notice department to create the notice.
        public string NoticePath { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public string SessionName { get; set; }
        public int? RecivedBy { get; set; }
        public int NoticeStatus { get; set; }
        public bool? Actice { get; set; }
        #endregion

        public virtual ICollection<mEvent> mEventList { get; set; }

    }
}
