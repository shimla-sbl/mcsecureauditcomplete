﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Notice
{
    [Serializable]
    [Table("OnlineNotices")]
    public class OnlineNotices
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OnlineNoticeId { get; set; }
        public int ManualNoticeId { get; set; }
        public int AssemblyId { get; set; }
        public int SessionId { get; set; }
        [StringLength(150)]
        public string NoticeNumber { get; set; }
        public int NoticeTypeId { get; set; }
        public string Subject { get; set; }
        public string NoticeDetail { get; set; }
        public int? MinistryId { get; set; }
        [StringLength(7)]
        public string DepartmentID { get; set; }
        public int MemberID { get; set; }
        public string OriMemFilePath { get; set; }
        public string OriMemFileName { get; set; }
        public bool? IsHindi { get; set; }
        public DateTime? MemberSubmitDate { get; set; }
        public TimeSpan? MemberSubmitTime { get; set; }
        public bool SubmitToVS { get; set; }
        public string AttachedFilePath { get; set; }
        public string AttachedFileName { get; set; }
        public int? DemandId { get; set; }
        public bool? IsMobile { get; set; }
        public int? DesiredQuestionDateID { get; set; } 

    }
}
