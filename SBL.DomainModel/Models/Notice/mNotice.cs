﻿using SBL.DomainModel.ComplexModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Notice
{
    [Serializable]
    [Table("tNoticeBoard")]
    public class mNotice
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NoticeID { get; set; }
        [Required]
        public string NoticeTitle { get; set; }
        
        public string deptId { get; set; }
        public string LocalNoticeTitle { get; set; }

        public string NoticeDescription { get; set; }

        public string LocalNoticeDescription { get; set; }
       
        public string Attachments { get; set; }

        public byte Status { get; set; }

        public int CategoryID { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public DateTime StartPublish { get; set; }

        public DateTime StopPublish { get; set; }

        public int AssemblyId { get; set; }

        public int SessionId { get; set; }
        
        [NotMapped]
        public string CategoryName { get; set; }

        public int Odering { get; set; }

        public int Hits { get; set; }
        public Int32? BulletinCode { get; set; }
        public int? GroupBy { get; set; }
        public int? BranchId { get; set; }
        [NotMapped]
        public List<FillBranch> BranchList { get; set; }
    }
}
