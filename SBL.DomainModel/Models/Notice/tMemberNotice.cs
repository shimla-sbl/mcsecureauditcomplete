﻿namespace SBL.DomainModel.Models.Notice
{
    #region Namespace Reffrences
    using SBL.DomainModel.ComplexModel;
    using SBL.DomainModel.Models.ActionButtons;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Event;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.Ministery;
    using SBL.DomainModel.Models.Question;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.UserModule;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;
    using SBL.DomainModel.Models.Diaries;


    #endregion

    [Table("tMemberNotice")]
    [Serializable]
    public partial class tMemberNotice
    {
        public tMemberNotice()
        {
            this.memberNoticeList = new List<tMemberNotice>();
            this.memberNoticeModelList = new List<tMemberNoticeModel>();
            //this.noticeTypeList = new List<mNoticeType>();
            this.sessionList = new List<mSession>();
            this.memberList = new List<mMember>();
            this.NewmemberList = new List<DiaryModel>();
            this.mAssemblyList = new List<mAssembly>();
            this.MinistryList = new List<mMinisteryMinisterModel>();
            this.DepartmentList = new List<mDepartment>();
            this.eventList = new List<mEvent>();
            this.tQuestionModel = new List<QuestionModelCustom>();
            this.Values = new List<QuestionModelCustom>();
        }
        public tMemberNotice(tMemberNotice memNotice)
        {
            this.NoticeId = memNotice.NoticeId;
            this.NoticeNumber = memNotice.NoticeNumber;
            this.MemberName = memNotice.MemberName;
            this.PaperLaidId = memNotice.PaperLaidId;
            this.MinistryId = memNotice.MinistryId;
            this.DepartmentId = memNotice.DepartmentId;
            this.MinistryName = memNotice.MinistryName;
            this.Subject = memNotice.Subject;
            this.Status = memNotice.Status;
            this.DataStatus = memNotice.DataStatus;
            this.BussinessType = memNotice.BussinessType;
            this.IsAcknowledgmentDate = memNotice.IsAcknowledgmentDate;
            this.AcknowledgmentDateString = memNotice.AcknowledgmentDateString;
            this.SubmittedDateString = memNotice.SubmittedDateString;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [HiddenInput(DisplayValue = false)]
        public int NoticeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? PaperLaidId { get; set; }

        [StringLength(250)]
        [DisplayName("Number")]
        public string NoticeNumber { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Notice { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string NoticeLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int NoticeTypeID { get; set; }

        [DisplayName("Subject")]
        public string Subject { get; set; }

        public bool IsPrintTaken { get; set; }

        public bool IsSecChecked { get; set; }

        //[Required]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [HiddenInput(DisplayValue = false)]
        public DateTime NoticeDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public TimeSpan NoticeTime { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? FixedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string NoticeReply { get; set; }

        [StringLength(50)]
        [HiddenInput(DisplayValue = false)]
        public string NoticeReplyPath { get; set; }

        [StringLength(50)]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentId { get; set; }

        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentName { get; set; }

        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public string BussinessType { get; set; }

        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MinistryId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MinsiterId { get; set; }

        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        public string MinisterName { get; set; }

        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        public string MinisterNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MemberId { get; set; }

        [StringLength(250)]
        [DisplayName("Asked By")]
        public string MemberName { get; set; }

        [StringLength(50)]
        [HiddenInput(DisplayValue = false)]
        public string MemberNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsOnlineSubmitted { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? IsOnlineSubmittedDate { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<tMemberNotice> memberNoticeList { get; set; }



        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mMember> memberList { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<DiaryModel> NewmemberList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mAssembly> mAssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mMinisteryMinisterModel> MinistryList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mDepartment> DepartmentList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<tMemberNoticeModel> memberNoticeModelList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mEvent> eventList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DateNotice { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string NoticeTypeName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFilePath { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFileName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsSubmitted { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime? SubmittedDate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string SubmittedBy { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string SubInCatchWord { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsNoticeFreeze { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime? IsNoticeFreezeDate { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual List<tMemberNotice> tSessionDateSignature { get; set; }

        #region New Variables
        //Location of the path as we will let the Notice department to create the notice.
        [HiddenInput(DisplayValue = false)]
        public string NoticePath { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int AssemblyID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int SessionID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? RecivedBy { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int NoticeStatus { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? Actice { get; set; }
        #endregion

        #region Variable Declaration For Notices Grid (Pending and Published.)
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageSize { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageIndex { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int loopStart { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int loopEnd { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int selectedPage { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageNumber { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int RowsPerPage { get; set; }
        #endregion

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PAGE_SIZE { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? IsPending { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string MinistryName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int Status { get; set; }
        [NotMapped]
        [DisplayName("Status")]
        public string DataStatus { get; set; }

        public int? Priority { get; set; }

        public bool? IsHindi { get; set; }

        //Sameer
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<QuestionModelCustom> Values { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? StampTypeHindi { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? IsSitting { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<tQuestion> tQuestion { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<QuestionModelCustom> tQuestionModel { get; set; }
        #region Total Count Stared
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int AllMemberTourCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TodayTourCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int UpcomingTourCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int UnpublishedTourCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalStaredReceived { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalUnStaredReceived { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalNoticeCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalValue { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalStaredSubmitted { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalNoticesReceived { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int NoticeResultCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<QuestionModelCustom> objQuestList1 { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalStaredPendingForSubmission { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalStaredLaidInHouse { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalStaredPendingToLay { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mMinisteryMinisterModel> mMinisteryMinister { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mEvent> mEvents { get; set; }
        [NotMapped]
        public virtual ICollection<mMember> mMemberModel { get; set; }
        #endregion

        [NotMapped]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Title { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        [NotMapped]
        public string Details { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int QuestionID { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string Message { get; set; }
        [NotMapped]
        //[HiddenInput(DisplayValue = false)]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string MainQuestion { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int QuestionTypeId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int UnstartResultCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int OtherPaperCount { get; set; }
        //[NotMapped]
        //[HiddenInput(DisplayValue = false)]
        //public int SessionId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int EventId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int AssemblyCode { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string NDate { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AnswerAttachLocation { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mUsers> mUsers { get; set; }
        //[NotMapped]
        //[HiddenInput(DisplayValue = false)]
        //public int UserId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string RoleName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<QuestionModelCustom> tMemberNoticeModel { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool UserIds { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string QuestionValue { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string UId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string UserName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mUsers> UsersList { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<ActionButtonControl> ButtonList { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? IsContentFreeze { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalClubbedQuestion { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalInActiveQuestion { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DiaryNo { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string MemIds { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string QuesIds { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string TypistUserId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalStaredReceivedCl { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<tMemberNotice> DepartmentLt { get; set; }
        [NotMapped]
        public List<mMinisteryMinisterModel> ministerList { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Select Minister")]
        public int MinisterId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool Checked { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public Guid UserID { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool IsPermissions { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string CDiaryNo { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string CMemName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string Msg { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int QuestionStatus { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? IsTypistFreeze { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string RecordBy { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AssesmblyName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string ConstituencyName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string ClubDiaryNo { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string BrackWithQIds { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string MainDiaryNo { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? IsClubbed { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool IsClubChecked { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int UnstaredTotalClubbedQuestion { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalUnStaredReceivedCl { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int? OldMinistryId { get; set; }


        [HiddenInput(DisplayValue = false)]
        public string OldDepartmentId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsDepartmentChangedApprove { get; set; }

        [NotMapped]
        public int TotalQCount { get; set; }
        [NotMapped]
        public int? SessionDateId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string FilePath { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string RoleID { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string CMemNameHindi { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime? Updatedate { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalSBracktedQuestion { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalUBractedQuestion { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string NoticePdfPath { get; set; }


        // For Changer View
        [NotMapped]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string TitleChanger { get; set; }
        [NotMapped]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string DetailsChanger { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int ConstituencyCode { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<mUserModules> MenuList { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string RuleNo { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int OldNoticeTypeID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsTypeChange { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? TypeChangeDate { get; set; }

        [StringLength(500)]
        [HiddenInput(DisplayValue = false)]
        public string ChangeTypeRemarks { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int OriNoticeTypeID { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? IsQuestionInPart { get; set; }


        [HiddenInput(DisplayValue = false)]
        public string FileName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DocFileName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DocFilePath { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? HeaderHindi { get; set; }
        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        public string MinisterNameEnglish { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? IsAssignTypsitDate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsNoticeTypistFreeze { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string TypistUserIdNotice { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? TypistRequired { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? TypistFreezed { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<tMemberNotice> memberNoticeList1 { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? DemandID { get; set; }

        public bool? IsCMClubbed { get; set; }
        public DateTime? IsCMClubbedDate { get; set; }
        public string ReferenceCMMemberCode { get; set; }
        public bool? IsBracketCM { get; set; }
        public string BracketedWithNoticeNo { get; set; }
        public string BracketedWithNoticeIdNo { get; set; }
        public string MergeDiaryNoCM { get; set; }
        public string ReferencedNumberCm { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string EventName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DemandName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? CMDemandId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string CutMotionFilePath { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string CutMotionFileName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string CompletePath { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? QuestionSequence { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AttachFileName { get; set; }

        public DateTime? IsAcknowledgmentDate { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AcknowledgmentDateString { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsTranceLock { get; set; }

        //Added by Durgesh
        public bool? IsWithdrawbyMember { get; set; }

        public bool? IsExcessQuestion { get; set; }

        public bool? IsTimeBarred { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? countClub { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? countClubbed { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AcknowledgedBy { get; set; }


        [HiddenInput(DisplayValue = false)]
        public string AadharId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SesNameEnglish { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalNoticesProofRed { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsProofReading { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime? IsProofReadingDate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string ProofReaderUserId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string DeActivateFlag { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool IsPostpone { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime? IsPostponeDate { get; set; }
        [StringLength(150)]
        public string ManualNoticeNumber { get; set; }
        [DisplayName("Sent By V.S")]
        [NotMapped]
        public string SubmittedDateString { get; set; }
        [NotMapped]
        public List<string> MemberIds { get; set; }
        public bool? IsMobile { get; set; }
        [NotMapped]
        public string SelectedIndexId { get; set; }
        [NotMapped]
        public int iSelectedIndexId { get; set; }
        [NotMapped]
        public int RowIndexId { get; set; }
        [NotMapped]
        public string NoticeDiaryDate { get; set; }
        [NotMapped]
        public bool? IsLaid { get; set; }
        [NotMapped]
        public string DesireLayingDateString { get; set; }
        [NotMapped]
        public string nFileNameSentByMember { get; set; }
        [NotMapped]
        public string nFilePathSentByMember { get; set; }
        [NotMapped]
        public string nFileAttNameSentByMember { get; set; }
        [NotMapped]
        public string nFileAttPathSentByMember { get; set; }
        [NotMapped]
        public string OnlineNoticeByMemberPdf { get; set; }
        [NotMapped]
        public string OnlineAttachByMemberPdf { get; set; }
        [NotMapped]
        public bool IsLOBAttached { get; set; }
    }
}
