﻿namespace SBL.DomainModel.Models.Notice
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    #endregion

    [Table("mNoticeType")]
    [Serializable]
    public partial class mNoticeType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NoticeTypeID { get; set; }

        public string NoticeTypeName { get; set; }

        public string NoticeTypeNameLocal { get; set; }

        public bool IsActive { get; set; }

        [StringLength(10)]

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
