﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.ROM
{
    [Serializable]
    [Table("tRotationMinister")]
    public class tRotationMinister
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }


        public int SessionDateId { get; set; }


        public string MinistryId { get; set; }

        public bool? IsActive { get; set; }


        [Required]
        public int AssemblyId { get; set; }

        [Required]
        public int SessionId { get; set; }





        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        //public bool Status { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string ModifiedBy { get; set; }

        [NotMapped]
        public string AssemblyName { get; set; }

        [NotMapped]
        public string SessionName { get; set; }

        [NotMapped]
        public string SessionDate { get; set; }

        [NotMapped]
        public string MinisteryMinisterName { get; set; }




    }
}
