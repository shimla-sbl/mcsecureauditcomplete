﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.UploadMetaFile
{
    [Table("mUploadmetaDataFile")]
    [Serializable]
    public class mUploadmetaDataFile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int AssemblyId { get; set; }

        public int SessionId { get; set; }

        public string SessionDateId { get; set; }

        public string DocumentTypeId { get; set; }
        public string CommitteeId { get; set; }
        public string DepartmentId { get; set; }
        public string CommitteeTypeId { get; set; }

        public string BillId { get; set; }
        public string QuestionId { get; set; }
        public string PdfFile { get; set; }

        public string Image { get; set; }
        public DateTime CreatedDate { get; set; }
        [NotMapped]
        public DateTime? SessionDate { get; set; }
        [NotMapped]
        public String DeletedIdsList { get; set; }
    }
}
