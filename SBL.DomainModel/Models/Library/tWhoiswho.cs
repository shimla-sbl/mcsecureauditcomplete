﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Library
{
    [Table("tWhoiswho")]
    [Serializable]
    public class tWhoiswho
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WhoiswhoId { get; set; }

        //[Required]
        public string Source { get; set; }

        //[Required]
        public string Date { get; set; }

        //[Required]
        public string Year { get; set; }

        //[Required]
        public string Num { get; set; }

        //[Required]
        public string LocationDetails { get; set; }

        public string Building { get; set; }

        public string Floor { get; set; }

        public string Almirah { get; set; }

        public string Reck { get; set; }


        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

    }
}
