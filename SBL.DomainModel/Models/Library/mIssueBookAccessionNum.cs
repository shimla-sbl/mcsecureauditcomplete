﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Library
{
    [Table("mIssueBookAccessionNum")]
    [Serializable]
    public class mIssueBookAccessionNum
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IssueBookAccessionNumID { get; set; }

        [Required]
        public string AccessNumber { get; set; }

        public int IssueBookId { get; set; }
        [Required]
        public DateTime DateOfReturn { get; set; }

    }
}
