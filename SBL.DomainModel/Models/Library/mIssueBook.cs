﻿using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.LibraryVS;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.PaidMemeberSubscription;
using SBL.DomainModel.Models.StaffManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Library
{
    [Table("mIssueBook")]
    [Serializable]
    public class mIssueBook
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IssueBookID { get; set; }

        [Required]
        [Display(Name = "Author Name")]
        public string SubscriptionType { get; set; }

        [Required]
        public DateTime DateOfIssue { get; set; }      

        [Required]
        public string SubscriberDesignationId { get; set; }

        public int? SubscriberId { get; set; }

        public bool IsMember { get; set; }

        public string ModifiedBy { get; set; }
            
        public DateTime? ModifiedWhen { get; set; }


        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public string SubscriberDesignation { get; set; }

        [NotMapped]
        public string SubscriberName { get; set; }

        [NotMapped]
        public string Prefix { get; set; }

        [NotMapped]
        public string Email { get; set; }

        [NotMapped]
        public string Mobile { get; set; }

        public bool IsPaidMember { get; set; }

        //public int? PaidMemberId { get; set; }

        [NotMapped]
        public string PaidMemberName { get; set; }

        [NotMapped]
        public string PaidMemberDesignation { get; set; }

        [NotMapped]
        public string PaidSubMobileNo { get; set; }

        [NotMapped]
        public PaidMemeberSubs PaidMemberDetailWithId { get; set; }

        [NotMapped]
        public ICollection<PaidMemeberSubs> PaidMemeberSubs { get; set; }

        [NotMapped]
        public ICollection<mMemberDesignation> DesignationCol { get; set; }

        [NotMapped]
        public ICollection<mMember> MemberCol { get; set; }

        [NotMapped]
        public ICollection<VsDocumentIssue> MemberList { get; set; }

        [NotMapped]
        public ICollection<mStaff> mStaffdesignation { get; set; }

        [NotMapped]
        public ICollection<mStaff> mStaffMembers { get; set; }

        [NotMapped]
        public int memberDesignationid;

        [NotMapped]
        public int Designationid
        {
            get { return Convert.ToInt32(SubscriberDesignationId); }
            set { memberDesignationid = Convert.ToInt32(SubscriberDesignationId); }
        }
        
    }
}
