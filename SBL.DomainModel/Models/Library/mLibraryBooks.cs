﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Library
{
    [Table("mLibraryBooks")]
    [Serializable]
    public class mLibraryBooks
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int accession{ get; set; }
        [Required]
        public string material_type { get; set; }

        public int? accession_b { get; set; }

        public int? main_code { get; set; }

        public int? branch_code { get; set; }

        public string call_no { get; set; }

        public int? sections { get; set; }

        public string title { get; set; }

        public string subtitle { get; set; }

        public string series { get; set; }

        public string author1 { get; set; }

        public string author2 { get; set; }

        public string author3 { get; set; }

        public string variant_author { get; set; }

        public string corporate_author { get; set; }

        public string pub_code { get; set; }

        public decimal? year_pub { get; set; }

        public string edition { get; set; }

        [Required]
        public bool multiple_volume { get; set; }

        public string volume { get; set; }

        public string firstvolume_accession { get; set; }

        public string collation_pages { get; set; }

        public string isbn { get; set; }

        public string binding { get; set; }

        public decimal? lang_code { get; set; }

        public float? price_rs { get; set; }

        public float? price_dollar { get; set; }

        public float? price_pound { get; set; }

        public string subject_heading { get; set; }

        public string source { get; set; }

        public string donor_name { get; set; }

        public string supp_code { get; set; }

        public string category { get; set; }

        public string bill_no { get; set; }

        public DateTime?  bill_date { get; set; }

        public int? acc_material { get; set; }

        public int? copy_number { get; set; }

        public string note { get; set; }

        public int? book_status { get; set; }

        public string publisher { get; set; }

        public string location { get; set; }

        public string series_author { get; set; }

        public string vendor { get; set; }

        public int? book_issue_type { get; set; }

        public int? days_of_issue { get; set; }

        public string translator { get; set; }

        public string editor { get; set; }

        public string recom_by { get; set; }

        public string keywords { get; set; }

        public float? price { get; set; }

        public string currency { get; set; }

        public string country { get; set; }

        public DateTime? date_of_entry { get; set; }

        public string physical_location { get; set; }

        public string Maccession { get; set; }

        public string author { get; set; }

        public decimal? serial_source { get; set; }

        public decimal? serial_section { get; set; }

        public string sub_code { get; set; }

        public string cir_ref { get; set; }



    }
}
