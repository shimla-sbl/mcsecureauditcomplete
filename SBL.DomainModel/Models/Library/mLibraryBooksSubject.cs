﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Library
{
    [Table("mLibraryBooksSubject")]
    [Serializable]
   public class mLibraryBooksSubject
    {
        [Key]
        public string sub_code { get; set; }

        [Required]
        public string sub_des { get; set; }

        public string system_user { get; set; }

    }
}
