﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Library
{
    [Table("mDebateSaleCounter")]
    [Serializable]
    public class mDebateSaleCounter
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DebateSaleCounterID { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        public string Khand { get; set; }

        public string Ank { get; set; }
        [Required]
        public DateTime DateOfDebateE { get; set; }
        [MaxLength(2)]
        public string HindiDate { get; set; }
        [MaxLength(15)]
        public string HindiMonth { get; set; }

        [MaxLength(4)]
        public string HindiYear { get; set; }
        [Required]
        [MaxLength(2)]
        public string Assembly { get; set; }

        [Required]
        [MaxLength(2)]
        public string Session { get; set; }

        [Required]
        public string Pages { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public int Availability { get; set; }

        public int Quantity { get; set; }
        [Required]
        public string Building { get; set; }
        [Required]
        public string Floor { get; set; }
        [Required]
        public string Almirah { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
        [NotMapped]
        public string Mode { get; set; }
    }
}
