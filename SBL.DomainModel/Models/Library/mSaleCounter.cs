﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Library
{
    [Table("mSaleCounter")]
    [Serializable]
    public class mSaleCounter
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SaleCounterID { get; set; }

        [Required]
        [Display(Name = "Author Name")]
        public string AuthorName { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Edition { get; set; }
        [Required]
        public string Volume { get; set; }
        [Required]
        [Display(Name = "Publishing Year")]
        public string PublishingYear { get; set; }

        [Required]
        public string Pages { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public string Source { get; set; }

        [Required]
        public int Availability { get; set; }

     //   [StringLength(3, ErrorMessage = "Quantity cannot be longer than 3 digit.")]
        public int Quantity { get; set; }
        [Required]
        public string Building { get; set; }

        [Required]
        public string Floor { get; set; }

        [Required]
        public string Almirah { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }


        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
        [NotMapped]
        public string Mode { get; set; }
    }
}
