﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Library
{
    [Table("tFloorVersion")]
    [Serializable]
    public class tFloorVersion
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FloorVersionId { get; set; }

        public string Title { get; set; }


        public string Date { get; set; }



        public string Year { get; set; }


        public string CopyNo { get; set; }


        public string LocationDetails { get; set; }


        public string Building { get; set; }


        public string Floor { get; set; }


        public string Almirah { get; set; }


        public string Reck { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

    }
}
