﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Library
{
    [Table("mLibraryBooksLanguage")]
    [Serializable]
   public class mLibraryBooksLanguage
    {
        [Key]
        public decimal lang_code { get; set; }

        [Required]
        public string lang_des { get; set; }

        public string system_user { get; set; }

    }
}
