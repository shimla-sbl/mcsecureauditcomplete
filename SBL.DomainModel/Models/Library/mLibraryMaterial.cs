﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Library
{
    [Table("mLibraryMaterial")]
    [Serializable]
   public class mLibraryMaterial
    {
        [Key]
        public string material_code { get; set; }

        public string material_type { get; set; }

        public string MATID { get; set; }

        public string system_user { get; set; }

    }
}
