﻿namespace SBL.DomainModel.Models.Department
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    #endregion

    [Serializable]
    [Table("mDepartment")]
    public partial class mDepartment
    {
        [Key]
        [StringLength(50)]
        public string deptId { get; set; }

        public string deptabbr { get; set; }

        public string deptname { get; set; }

        public string deptnameLocal { get; set; }

        public string deptphoto { get; set; }

        public string DeptConStr { get; set; }

        public string enivaran { get; set; }

        public long? GriNumber { get; set; }

        public DateTime? GriNumberUpdationDate { get; set; }

        public string LatestGriNumber { get; set; }

        public int? RowNumber { get; set; }

        public string eSamadhanNote { get; set; }

        public string eSamadhanOnline { get; set; }

        public string eSameekshaOnlineYN { get; set; }

        public int? eSameekshaWorkCode { get; set; }

        public string deptabbrLocal { get; set; }

        public string eSameekshaWorkCodeYear { get; set; }
        public string StarredBeforeFixPath { get; set; }
        public string UnStarredBeforeFixPath { get; set; }
        public string StarredApproved { get; set; }
        public string UnStarredApproved { get; set; }
        public DateTime? SLastUpdatedDate { get; set; }
        public DateTime? UnSLastUpdatedDate { get; set; }

      
        public string NoticeBeforeFixPath { get; set; }
        public DateTime? NoticeBFLUpdatedDate { get; set; }
        public string NoticeAfterFixPath { get; set; }
        public bool? IsActive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
