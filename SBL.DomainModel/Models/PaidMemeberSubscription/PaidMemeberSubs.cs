﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.PaidMemeberSubscription
{
    [Table("PaidMemeberSubs")]
    [Serializable]
    public class PaidMemeberSubs
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PaidMemeberSubsID { get; set; }

        [Required]
        public string NameOfSubscriber { get; set; }


        [Required]
        public string Address { get; set; }

        [Required]
        public string MobileNumber { get; set; }

        [Required]
        public string Designation { get; set; }
        [Required]
        public DateTime SubsStartDate { get; set; }
        [Required]
        public DateTime SubsEndDate { get; set; }
         [Required]
        public string Remarks { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public int AmountPaid { get; set; }

        [NotMapped]
        public IEnumerable<PaidMemeberSubs> PaidMemeberList { get; set; }
    }
}
