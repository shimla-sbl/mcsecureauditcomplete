﻿using SBL.DomainModel.Models.Constituency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
  public partial   class PanchayatConstituencyModal
    {
        public int PanchayatID { get; set; }
        public int? PanchayatCode { get; set; }
        public string PanchayatName { get; set; }
        public string PanchayatNameLocal { get; set; }
        public bool IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedWhen { get; set; }
        public List<PanchayatConstituencyModal> GetData { get; set; }
        public List<PanchayatConstituencyModal> ShowData { get; set; }
        public int? DstCode { get; set; }
        public int? SCode { get; set; }
        public int CCode { get; set; }
        public bool? IsMapped { get; set; }
        public List<PanchayatConstituencyModal> tCPanchayat { get; set; }
        public int ConstituencyPanchayatID { get; set; }
        public List<tConstituencyPanchayat> PanchayatData { get; set; }
        public mConstituency CName { get; set; }
        public string DName { get; set; }
        public List<PanchayatConstituencyModal> DNamePanchayat { get; set; }

    }
}
