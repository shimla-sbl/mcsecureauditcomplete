﻿using System;
using System.Collections.Generic;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class MemberOuterBill
    {
        public int desginationID { get; set; }

        public string Financialyear { get; set; }

        public int BudgetId { get; set; }

        public string memberName { get; set; }

        public int? memberID { get; set; }

        public string designationName { get; set; }

        public int monthID { get; set; }

        public int yearID { get; set; }

        public string monthName { get; set; }

        public int billNO { get; set; }

        public int tokenNO { get; set; }

        public int voucherNo { get; set; }

        public DateTime? billDate { get; set; }

        public DateTime? tokenDate { get; set; }

        public DateTime? voucherDate { get; set; }

        public IEnumerable<HeadList> headList { get; set; }

        public double grossTotal { get; set; }

        public double total_a { get; set; }

        public double total_b { get; set; }

        public int ispart { get; set; }
    }

    [Serializable]
    public class membersOuterReport
    {
        public int? monthID { get; set; }

        public int? yearID { get; set; }

        public int? catID { get; set; }

        public DateTime? createdDate { get; set; }

        public DateTime? printedDate { get; set; }

        public List<string> columnName { get; set; }
        public int? ispart { get; set; }
    }

    [Serializable]
    public class HouseRentDed
    {
        public string memberName { get; set; }

        public double amount { get; set; }
    }

    [Serializable]
    public class HouseRentDedReport
    {
        public int designationID { get; set; }

        public double total { get; set; }

        public string monthName { get; set; }

        public int monthID { get; set; }

        public string DeductionType { get; set; }

        public int yearID { get; set; }

        public string accountHead { get; set; }

        public IEnumerable<AccountHeadList> accountHeadList { get; set; }

        public IEnumerable<HouseRentDed> houserentDedList { get; set; }
    }

    [Serializable]
    public class ScheduleReport
    {
        public string monthName { get; set; }

        public int monthID { get; set; }

        public string LoanName { get; set; }

        public int yearID { get; set; }

        public string accountHead { get; set; }

        public IEnumerable<AccountHeadList> accountHeadList { get; set; }

        public IEnumerable<ScheduleloanList> scheduleloanList { get; set; }

        public double totalRecover { get; set; }

        public double itotalRecover { get; set; }
    }

    [Serializable]
    public class ScheduleloanList
    {
        public string Name { get; set; }

        public string designation { get; set; }

        public double totalAdvance { get; set; }

        public double pdedOfMonth { get; set; }

        public double idedOfMonth { get; set; }

        public double? outstandingBal { get; set; }

        public double progRecover { get; set; }

        public double balAMount { get; set; }

        public int instNO { get; set; }

        public string AccNumber { get; set; }

        public string type { get; set; }
    }

    [Serializable]
    public class AccountHeadList
    {
        public string code { get; set; }

        public string text { get; set; }
    }

    [Serializable]
    public class MembersNominalReport
    {
        public MembersNominalReport()
        {
            MembersNominalReportLIst = new List<MembersNominalReportLIst>();
        }
        public string financialYear { get; set; }
        public string favourOF { get; set; }
        public string head { get; set; }
        public List<MembersNominalReportLIst> MembersNominalReportLIst { get; set; }
        public double Total { get; set; }
        public string gTotal { get; set; }
    }

    [Serializable]
    public class MembersNominalReportLIst
    {
        public MembersNominalReportLIst()
        {
            Columns = new List<string>();
        }
        public List<string> Columns { get; set; }
    }
}