﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.ConstituencyVS;
using System.ComponentModel.DataAnnotations;

namespace SBL.DomainModel.ComplexModel
{
   [Serializable]
    public class MLADiary_Search
    {
       [Key]
        public int MlaDiaryID { get; set; }
        public int? RecordNumber { get; set; }
        public string Subject { get; set; }
        public int? MlaCode { get; set; }
        public string MappedMemberName { get; set; }
        public string DocFile { get; set; }
        public DateTime? ForwardDate { get; set; }
        public DateTime? ForwardDateForUse { get; set; }
        public int? MinisterId { get; set; }
        public string DeptId { get; set; }
        public int? ActionCode { get; set; }
        public string DocmentType { get; set; }
        public string ForwardedFile { get; set; } 
        public string ActionTakenFile { get; set; }         
        public DateTime? ActionTakenDate { get; set; }
        public string EnclosureFile { get; set; }    
        public DateTime? EnclosureDate { get; set; }
        public int? OfficeId { get; set; }
        public string DocumentTo { get; set; }
        public string DocumentCC { get; set; }
        public DateTime? DiaryDate { get; set; }
        public string RecieveFrom { get; set; }
        public int? DiaryLanguage { get; set; }
        public double? SanctionAmount { get; set; }
        public string FinancialYear { get; set; }
        public string OfficeId_CC { get; set; }
        public DateTime? ActiontakenDateForUse { get; set; }
        public string DeptName { get; set; }
        public string Officename { get; set; }
        public string officenameCC { get; set; }
        public string MinisterName { get; set; }
      public int? ConstituencyId { get; set; }

    }
}

