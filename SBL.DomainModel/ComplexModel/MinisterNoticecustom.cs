﻿using Lib.Web.Mvc.JQuery.JqGrid;
using Lib.Web.Mvc.JQuery.JqGrid.Constants;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.ComplexModel
{
     [Serializable]
  public  class MinisterNoticecustom
    {
         public MinisterNoticecustom()
        {
            this.ListBills = new List<MinisterNoticecustom>();
        }

         public MinisterNoticecustom(MinisterNoticecustom BillsList)
        {

            this.PaperLaidId = BillsList.PaperLaidId;
            this.NoticeNumber = BillsList.NoticeNumber;
            this.Subject = BillsList.Subject;
           
            this.MemberName = BillsList.MemberName;
            this.Version = BillsList.Version;
            this.DeptSubmittedDate = BillsList.DeptSubmittedDate;
            this.MinisterActivePaperId = BillsList.MinisterActivePaperId;
            this.PaperSent = BillsList.PaperSent;
            this.MinisterSubmittedDate = BillsList.MinisterSubmittedDate;
            this.FileName = BillsList.FileName;
            this.EventName = BillsList.EventName;
            
            this.AssemblyCode = BillsList.AssemblyCode;
           
            this.SessionCode = BillsList.SessionCode;
            this.Status = BillsList.Status;
            this.MinistryId = BillsList.MinistryId;
            this.ResultCount = BillsList.ResultCount;
            this.DesireLayingDate = BillsList.DesireLayingDate;
            this.NoticeID = BillsList.NoticeID;
            this.OriDiaryFileName = BillsList.OriDiaryFileName;
            this.OriDiaryFilePath = BillsList.OriDiaryFilePath;
            this.RuleNo = BillsList.RuleNo;
            this.DepartmentSubmittedDate = BillsList.DepartmentSubmittedDate;
            this.evidhanReferenceNumber = BillsList.evidhanReferenceNumber;
            //this.PaperLaidIdForCheckBox = Convert.ToString(BillsList.PaperLaidId);
       
        }


         public ICollection<MinisterNoticecustom> ListBills { get; set; }

         //[JqGridColumnLayout(Width = 10)]
         //[JqGridColumnFormatter("$.PaperLaidIdForCheckBoxFormatter")]
         //[DisplayName("")]
         //public string PaperLaidIdForCheckBox { get; set; }


         [HiddenInput(DisplayValue = false)]
         public long PaperLaidId { get; set; }

         [DisplayName("Rule")]
         public string RuleNo { get; set; }

      
         [DisplayName("Ref.Number")]
         public string evidhanReferenceNumber { get; set; }
        
         [DisplayName("Number")]
         public string NoticeNumber { get; set; }

       
         public string Subject { get; set; }

        
         [DisplayName("Asked By")]
         public string MemberName { get; set; }

          [HiddenInput(DisplayValue = false)]
         public int? Version { get; set; }
        

         [DisplayName("Paper Sent")]
         [JqGridColumnLayout(Alignment = JqGridAlignments.Center)]
         [JqGridColumnFormatter("$.NoticeMinisterpaperSentFormatter")]
         public string PaperSent { get; set; }

        
       //  [DisplayName("Sent on")]
         //[JqGridColumnFormatter(JqGridColumnPredefinedFormatters.Date, SourceFormat = "d.m.Y G:i:s", OutputFormat = "d.m.Y g:i s")]
        // [JqGridColumnFormatter("$.paperSentOnFormatter")]
          [HiddenInput(DisplayValue = false)]
         public DateTime? MinisterSubmittedDate { get; set; }


         [HiddenInput(DisplayValue = false)]
         public string Notice { get; set; }

         [HiddenInput(DisplayValue = false)]
         public string Id { get; set; }

         [HiddenInput(DisplayValue = false)]
         public string Name { get; set; }

         [HiddenInput(DisplayValue = false)]
         public int Count { get; set; }

         [HiddenInput(DisplayValue = false)]
         public bool IsRecieved { get; set; }

         [HiddenInput(DisplayValue = false)]
         public bool IsCurrentLob { get; set; }

         [HiddenInput(DisplayValue = false)]
         public int Status { get; set; }

         [HiddenInput(DisplayValue = false)]
         public string DepartmentName { get; set; }

         [HiddenInput(DisplayValue = false)]
         public bool IsLaidInHome { get; set; }

        

         //ForMinisterNotice
       
         [HiddenInput(DisplayValue = false)]
         public int MinistryId { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string MinistryName { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string DepartmentId { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string DeparmentName { get; set; }



         [HiddenInput(DisplayValue = false)]
         public string FileName { get; set; }
         [HiddenInput(DisplayValue = false)]
         public long tpaperLaidTempId { get; set; }
         [HiddenInput(DisplayValue = false)]
         public int? FileVersion { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string Title { get; set; }
         [HiddenInput(DisplayValue = false)]
         public DateTime? DeptSubmittedDate { get; set; }




         [HiddenInput(DisplayValue = false)]
         public string EventName { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string FilePath { get; set; }

         //uday
        

         [HiddenInput(DisplayValue = false)]
         public int ResultCount { get; set; }
         [HiddenInput(DisplayValue = false)]
         public int loopStart { get; set; }
         [HiddenInput(DisplayValue = false)]
         public int loopEnd { get; set; }

         [HiddenInput(DisplayValue = false)]
         public int AssemblyCode { get; set; }

         [HiddenInput(DisplayValue = false)]
         public int SessionCode { get; set; }
         [HiddenInput(DisplayValue = false)]
         public long? MinisterActivePaperId { get; set; }

         [DisplayName("Desired Date")]
         [JqGridColumnFormatter("$.noticeFixedDateOnFormatter")]
         public DateTime? DesireLayingDate { get; set; }


        
         [HiddenInput(DisplayValue = false)]
         public string MinisterName { get; set; }

        
         [HiddenInput(DisplayValue = false)]
        
         public string BussinessType { get; set; }

       
         [HiddenInput(DisplayValue = false)]
         public int NoticeID { get; set; }

         [HiddenInput(DisplayValue = false)]
         public string OriDiaryFilePath { get; set; }

         [HiddenInput(DisplayValue = false)]
         public string OriDiaryFileName { get; set; }


         [DisplayName("Sent on")]
         // [HiddenInput(DisplayValue = false)]
         public string DepartmentSubmittedDate { get; set; }

    }
}
