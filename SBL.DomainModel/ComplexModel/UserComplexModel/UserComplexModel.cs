﻿using Lib.Web.Mvc.JQuery.JqGrid;
using Lib.Web.Mvc.JQuery.JqGrid.Constants;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.ComplexModel.UserComplexModel
{
    [Serializable]
    public class UserComplexModel
    {


        public UserComplexModel()
        {
        }
        public UserComplexModel(UserComplexModel users)
        {
            this.UserId = users.UserId;
            this.UserName = users.UserName;
            this.Name = users.Name;
            this.Address = users.Address;

            this.AEmployeeIdForCheckBox = users.AEmployeeIdForCheckBox;

        }

       // [JqGridColumnLayout(Width = 150)]
        [JqGridColumnFormatter("$.employestatusCheckBoxFormatter")]
        [DisplayName("")]
        public string AEmployeeIdForCheckBox { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid UserId { get; set; }


      //  [JqGridColumnLayout(Width = 150)]
        public string UserName { get; set; }



      //  [JqGridColumnLayout(Width = 150)]
        public string Name { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string Address { get; set; }


    }
}
