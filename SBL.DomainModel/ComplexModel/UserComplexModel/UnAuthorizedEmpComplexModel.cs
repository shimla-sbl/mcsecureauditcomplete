﻿using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.ComplexModel.UserComplexModel
{
    
    public class UnAuthorizedEmpComplexModel
    {

        public UnAuthorizedEmpComplexModel()
        {
        }
        public UnAuthorizedEmpComplexModel(UnAuthorizedEmpComplexModel users)
        {
            this.UserId = users.UserId;
            this.UserName = users.UserName;
            this.Name = users.Name;
            this.Address = users.Address;
            this.UEmployeeIdForCheckBox = users.UEmployeeIdForCheckBox;
            //this.AEmployeeIdForCheckBox = users.UserId.ToString();

        }


        //[JqGridColumnLayout(Width = 150)]
        [JqGridColumnFormatter("$.uemployestatusCheckBoxFormatter")]
        [DisplayName("")]
        public string UEmployeeIdForCheckBox { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid UserId { get; set; }


        //[JqGridColumnLayout(Width = 150)]
       // [JqGridColumnLayout(Width = 150)]
        public string UserName { get; set; }





       // [JqGridColumnLayout(Width = 150)]
        public string Name { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Address { get; set; }


    }
}
