﻿namespace SBL.DomainModel.ComplexModel.UserComplexModel
{
    using SBL.DomainModel.ComplexModel;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Role;
    using SBL.DomainModel.Models.Secretory;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using SBL.DomainModel.ComplexModel.UserComplexModel;
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.Adhaar;

    [Serializable]
    public partial class mUserModelClass
    {
        public mUserModelClass()
        {
            tUserRoles = new HashSet<tUserRoles>();
            mDepartment = new List<mDepartment>();
            AllDepartmentIds = new List<string>();
            UserDscList = new List<mUserDSHDetails>();
            CustomUserList = new List<UserComplexModel>();
            mUserList = new List<mUsers>();
            AgeList = new List<AgeList>();
        }

        public ICollection<AgeList> AgeList { get; set; } 
    
        public Guid UserId { get; set; }

       
        public string UserName { get; set; }

        public string Password { get; set; }

        public bool? IsActive { get; set; }


        public string DeptId { get; set; }


        public string OfficeId { get; set; }

 
        public string OfficeLevel { get; set; }

 
        public string BranchId { get; set; }

        [StringLength(50)]
        public string ConstituencyId { get; set; }

        [StringLength(50)]
        public string Designation { get; set; }

        [StringLength(50)]
        public string AadarId { get; set; }

        [StringLength(250)]
        public string DSCHash { get; set; }

        [StringLength(50)]
        public string IsMember { get; set; }

        [StringLength(50)]
        public string IsMedia { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public bool IsDSCAuthorized { get; set; }
        [NotMapped]
        public virtual ICollection<tUserRoles> tUserRoles { get; set; }


        #region User Registration

        public int? SecretoryId { get; set; }
        [NotMapped]
        public string SecretoryName { get; set; }

        [NotMapped]
        public List<mUserDSHDetails> UserDscList { get; set; }

        public string DepartmentIDs { get; set; }

        public string EmpId { get; set; }

        public string Photo { get; set; }
        public string MobileNo { get; set; }
        [NotMapped]
        [Required(ErrorMessage = "Please Enter Mobile Number")]
        public string Mobile { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Please Enter Email Address")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string MailID { get; set; }


        public string EmailId { get; set; }
        [NotMapped]
        public List<KeyValuePair<int, string>> radioList { get; set; }

        [NotMapped]
        public virtual ICollection<mDepartment> mDepartment { get; set; }

        [NotMapped]
        public List<string> AllDepartmentIds { get; set; }

        [NotMapped]
        //[Required(ErrorMessage = "Please Enter Code")]
        public string EmpOrMemberCode { get; set; }

        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
        public string Address { get; set; }

        [NotMapped]
        public string FatherName { get; set; }
        [NotMapped]
        [Required(ErrorMessage = "Please Enter OTP Value")]
        public string OTPValue { get; set; }

        [NotMapped]
        [StringLength(50)]
        [Required(ErrorMessage = "Please enter Aadhar ID")]
        public string AdhaarID { get; set; }
        [NotMapped]
        public virtual ICollection<mSecretory> mSecretory { get; set; }

        [NotMapped]
        public virtual ICollection<SecretoryDepartmentModel> SecretoryDepartmentModel { get; set; }
        [NotMapped]
        public string DSCName { get; set; }
        [NotMapped]
        public string DSCType { get; set; }
        [NotMapped]
        public string DSCValidity { get; set; }
        [NotMapped]
        public string CaptchaMessage { get; set; }

        [NotMapped]
        public List<UserComplexModel> CustomUserList { get; set; }

        [NotMapped]
        public ICollection<mUsers> mUserList { get; set; }

        [NotMapped]
        public string DscActiveStatus { get; set; }
        public AdhaarDetails AadharDetails { get; set; }
        public string Gender { get; set; }
        public int? Age { get; set; }
        public string DOB { get; set; }
        public bool IsAuthorized { get; set; }
        public bool IsNotification { get; set; }
        public bool Permission { get; set; }
        public bool? IsHOD { get; set; }
        public bool? IsSecretoryId { get; set; }
        #endregion
    }
}
