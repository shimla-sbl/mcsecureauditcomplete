﻿using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.SubDivision;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel.UserComplexModel
{
    [Serializable]
    public class UserCreation
    {   
        public int ID { get; set; }

        [Display(Name = "User Name")]
        [Required]
        public string UserName { get; set; }

        [Required]
        public string DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        [Required]
        public long OfficeId { get; set; }
        public string OfficeName { get; set; }

        public List<mDepartment> DepartmentList { get; set; }
        public List<mOffice> OfficeList { get; set; }
        public List<mSubDivision> mSubDivisionList { get; set; }

        [Display(Name = "Evidhan ID")]
        [MinLength(12, ErrorMessage = "Evidhan ID Must Be of 12 Characters")]
        //[RegularExpression(@"^\d{12}$", ErrorMessage = "Please enter correct Aadhaar Id")]
        public string AadhaarId { get; set; }

        [Required]
        [MinLength(6, ErrorMessage = "Password Must Have Minimum 6 Characters")]
        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,}", ErrorMessage = "Minimum 6 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character.")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Both Password Should Be Same")]
        [Display(Name = "Confirmation Password")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,}", ErrorMessage = "Minimum 6 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character.")]
        public string ConfirmationPassword { get; set; }

        [Display(Name = "Mobile Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please Input 10 digit valid mobile number")]
        public string Mobile { get; set; }

        public List<QuestionModelCustom> MemberList { get; set; } // using it for member code and id
        public string Gender { get; set; }
        public string CreationDate { get; set; }
        public string mode { get; set; }

        public string MemberCode { get; set; }
        
        public string SubdivisionId { get; set; }
    }
}
