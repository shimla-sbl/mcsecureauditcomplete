﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.DomainModel.ComplexModel.UserComplexModel
{
    [Serializable]
    public partial class AgeList
    {
        public AgeList() { }
        public bool Selected { get; set; }
        public int ID { get; set; }
        public string permissionName { get; set; }
    }
}