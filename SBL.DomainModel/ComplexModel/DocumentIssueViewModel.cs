﻿using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.LibraryVS;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.StaffManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class DocumentIssueViewModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IssueID { get; set; }

        public string IssueDocumentName { get; set; }

        public DateTime DateOfIssue { get; set; }

        [NotMapped]
        [Required]
        public string DisplayedDOI { get; set; }

        public string TimeOfIssue { get; set; }

        public string DisplayedDOR { get; set; }

        public string SubscriberDesignationId { get; set; }

        [Required]
        public int SubscriberId { get; set; }

        [NotMapped]
        public string SubscriberDesignation { get; set; }

        [NotMapped]
        public string SubscriberName { get; set; }

        [NotMapped]
        public ICollection<mMemberDesignation> DesignationCol { get; set; }

        [NotMapped]
        public ICollection<mMember> MemberCol { get; set; }

        [NotMapped]
        public ICollection<VsDocumentIssue> MemberList { get; set; }

        [NotMapped]
        public ICollection<mStaff> mStaffdesignation { get; set; }

        [NotMapped]
        public ICollection<mStaff> mStaffMembers { get; set; }
        
        [DefaultValue(false)]
        public bool IsReturned { get; set; }

        [DefaultValue(true)]
        public bool IsMember { get; set; }

        public DateTime? DateOfReturned { get; set; }

        public string TimeOfReturned { get; set; }

        [NotMapped]
        public DateTime? IssueStartRange { get; set; }

        [NotMapped]
        public DateTime? IssueEndRange {get;set;}

        [NotMapped]
        public DateTime? ReturnStartRange{get;set;}

        [NotMapped]
        public DateTime? ReturnEndRange{get;set;}
    }
}
