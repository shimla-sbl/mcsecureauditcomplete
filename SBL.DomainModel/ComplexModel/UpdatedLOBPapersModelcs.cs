﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public partial class UpdatedLOBPapersModelcs
    {
        public UpdatedLOBPapersModelcs()
        {
            this.UpdatedLOBPapersList = new List<UpdatedLOBPapersModelcs>();
        }

        public string ConcernedEventName { get; set; }
        public int AdminLobId{get;set;}
        public long? tPaperLaidTempId { get; set; }
        public string PreviousFile { get; set; }
        public string LatestFile { get; set; }
        public int? SrNo1 { get; set; }
        public int? SrNo2 { get; set; }
        public int? SrNo3 { get; set; }
        public string LineNumber { get; set; }
        public long tPaperLaidId { get; set; }
        
        public virtual ICollection<UpdatedLOBPapersModelcs> UpdatedLOBPapersList { get; set; }
    }
}
