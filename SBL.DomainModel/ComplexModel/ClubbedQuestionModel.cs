﻿using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public partial class ClubbedQuestionModel
    {
        public ClubbedQuestionModel()
        {
            this.memberNoticeList = new List<tMemberNotice>();
            //this.memberNoticeModelList = new List<tMemberNoticeModel>();
            //this.noticeTypeList = new List<mNoticeType>();
            this.sessionList = new List<mSession>();
            this.memberList = new List<mMember>();
            this.mAssemblyList = new List<mAssembly>();
            this.MinistryList = new List<mMinisteryMinisterModel>();
            this.DepartmentList = new List<mDepartment>();
            this.eventList = new List<mEvent>();
            this.tQuestionModel = new List<ClubbedQuestionModel>();
            this.tMemberNoticeModel = new List<ClubbedQuestionModel>();
        }

        public ClubbedQuestionModel(ClubbedQuestionModel memNotice)
        {

            this.DiaryNumber = memNotice.DiaryNumber;
            this.MemberName = memNotice.MemberName;
            this.NoticeDate = memNotice.NoticeDate;
            this.QuestionID = memNotice.QuestionID;
            this.Subject = memNotice.Subject;
            this.ClubbedDNo = memNotice.ClubbedDNo;
        }

        [JqGridColumnFormatter("$.CheckBoxFormatter")]
        [DisplayName("Check Questions")]
        [HiddenInput(DisplayValue = false)]
        public int QuestionID { get; set; }

        [DisplayName("Diary Number")]
        public string DiaryNumber { get; set; }

        [StringLength(250)]
        [DisplayName("Asked By")]
        public string MemberName { get; set; }

        [DisplayName("Subject")]
        public string Subject { get; set; }

        [DisplayName("Bracket Diary No")]
        public string ClubbedDNo { get; set; }

        [DisplayName("Recieved Date")]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? NoticeDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int NoticeId { get; set; }



        [HiddenInput(DisplayValue = false)]
        public long? PaperLaidId { get; set; }

        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        public string NoticeNumber { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Notice { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string NoticeLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int NoticeTypeID { get; set; }



        //[Required]


        [HiddenInput(DisplayValue = false)]
        public TimeSpan NoticeTime { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? FixedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string NoticeReply { get; set; }

        [StringLength(50)]
        [HiddenInput(DisplayValue = false)]
        public string NoticeReplyPath { get; set; }

        [StringLength(50)]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentId { get; set; }

        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentName { get; set; }

        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MinistryId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MinsiterId { get; set; }

        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        public string MinisterName { get; set; }

        [StringLength(250)]
        [HiddenInput(DisplayValue = false)]
        public string MinisterNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MemberId { get; set; }


        [StringLength(50)]
        [HiddenInput(DisplayValue = false)]
        public string MemberNameLocal { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<tMemberNotice> memberNoticeList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mSession> sessionList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mMember> memberList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mAssembly> mAssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mMinisteryMinisterModel> MinistryList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mDepartment> DepartmentList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<tMemberNoticeModel> memberNoticeModelList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<mEvent> eventList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DateNotice { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string NoticeTypeName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFilePath { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFileName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsSubmitted { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime? SubmittedDate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string SubmittedBy { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string SubInCatchWord { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsNoticeFreeze { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime? IsNoticeFreezeDate { get; set; }


        #region New Variables
        //Location of the path as we will let the Notice department to create the notice.
        [HiddenInput(DisplayValue = false)]
        public string NoticePath { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int AssemblyID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int SessionID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? RecivedBy { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int NoticeStatus { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? Actice { get; set; }
        #endregion

        #region Variable Declaration For Notices Grid (Pending and Published.)
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageSize { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageIndex { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int loopStart { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int loopEnd { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int selectedPage { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageNumber { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int RowsPerPage { get; set; }
        #endregion

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PAGE_SIZE { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? IsPending { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string MinistryName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int Status { get; set; }
        [NotMapped]

        [HiddenInput(DisplayValue = false)]
        public string DataStatus { get; set; }

        //by sameer
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int QuestionTypeId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public Guid UId { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string RoleName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalStaredReceived { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalUnStaredReceived { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalValue { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalStaredSubmitted { get; set; }
        [NotMapped]
        //[HiddenInput(DisplayValue = false)]
        //public virtual ICollection<TypistQuestionModel> tQuestionModel { get; set; }
        public virtual ICollection<mEvent> mEvents { get; set; }
        public virtual ICollection<ClubbedQuestionModel> tQuestionModel { get; set; }
        public virtual ICollection<ClubbedQuestionModel> tMemberNoticeModel { get; set; }
        [HiddenInput(DisplayValue = false)]
        public Guid UserId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string UserName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string UsId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string QuestionValue { get; set; }

        public List<tMemberNotice> DepartmentLt { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string DeptId { get; set; }
    }

}

