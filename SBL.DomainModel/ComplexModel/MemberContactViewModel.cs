﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class MemberContactViewModel
    {
        public string MemberName { get; set; }
        public string ConstituencyName { get; set; }
        public string EMail { get; set; }
        public string Mobile { get; set; }
    }
}
