﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{

    [Serializable]
    public class ReferenceSearchResult
    {
        public int Id { get; set; }

        public int? SectorId { get; set; }

        public string SectorName { get; set; }

        public string Brief { get; set; }

        public DateTime Date { get; set; }

        public string CollectionName { get; set; }

        public int RegionId { get; set; }

        public string RegionName { get; set; }

        public string SectorName_Local { get; set; }

        public int CollectionTypeId { get; set; }

        public string Description { get; set; }

        public string Description_local { get; set; }

        public string FileLocation { get; set; }

        public bool IsActive { get; set; }

        public string mode { get; set; }

        public List<ReferenceSearchResult> RefDataList { get; set; }

        public List<SectorResultDrp> SectorDrp { get; set; }

        public List<CollectionResultDrp> CollectionDrp { get; set; }

    }
    [Serializable]
    public class SectorResultDrp
    {
        public int Id { get; set; }
        public string SectorName { get; set; }
    }

    [Serializable]
    public class CollectionResultDrp
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
