﻿using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class DiaryModelCustom
    {
        public DiaryModelCustom()
        {
            this.memberList = new List<DiaryModelCustom>();
            this.eventList = new List<mEvent>();
            this.DiaryList = new List<DiaryModelCustom>();
            this.SessList = new List<mSession>();
            this.SessDateList = new List<mSessionDate>();
            this.DeptList = new List<mDepartment>();
            this.memMinList = new List<mMinisteryMinisterModel>();
        }

        public DiaryModelCustom(DiaryModelCustom DiaryList)
        {
            this.DType = DiaryList.DType;
            this.QuestionId = DiaryList.QuestionId;
            this.NoticeId = DiaryList.NoticeId;
            this.QuestionNumber = DiaryList.QuestionNumber;
            this.NoticeNumber = DiaryList.NoticeNumber;
            this.Subject = DiaryList.Subject;
            this.NoticeDate = DiaryList.NoticeDate;
            this.NoticeTime = DiaryList.NoticeTime;
            this.DiaryNumber = DiaryList.DiaryNumber;
            this.AskedBy = DiaryList.AskedBy;
            this.DateForBind = DiaryList.DateForBind;
            this.Number = DiaryList.Number;
            this.CommonId = DiaryList.CommonId;
            this.PaperEntryType = DiaryList.PaperEntryType;
            this.UniqueId = DiaryList.UniqueId;
            RuleNo = DiaryList.RuleNo;
            Priority = DiaryList.Priority;
            IsOnlineSubmited = DiaryList.IsOnlineSubmited;
        }


        [HiddenInput(DisplayValue = false)]
        public bool IsOnlineSubmited { get; set; }

        [DisplayName("Type")]
        [HiddenInput(DisplayValue = false)]
        public string DType { get; set; }

        [DisplayName("Diary Number")]
        [JqGridColumnFormatter("$.DiaryFormatter")]
        public string Number { get; set; }

        [DisplayName("Rule")]
        [HiddenInput(DisplayValue = false)]
        public string RuleNo { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int AssemblyID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SessionID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Notice { get; set; }

        [DisplayName("Subject")]
        public string Subject { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime NoticeDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Time Required")]
        public TimeSpan NoticeTime { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? FixedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string NoticeReply { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DepartmentId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DepartmentName { get; set; }


        [Required(ErrorMessage = "Please Select Minister")]
        [HiddenInput(DisplayValue = false)]
        public int? MinistryId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MinsiterId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string CurrentUserName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string UserName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string UserDesignation { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int TotalDiaryCnt { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SCount { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int UCount { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int NCount { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DiaryNumber { get; set; }

        [DisplayName("Asked By")]
        public string AskedBy { get; set; }

        [DisplayName("Recieved Date")]
        [JqGridColumnFormatter("$.paperSentOnFormatter")]
        public DateTime? DateForBind { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime RecievedDateTime { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int QuestionId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string NoticeNumber { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int NoticeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string PaperEntryType { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int QuestionTypeId { get; set; }

        [Required(ErrorMessage = "Please Select BusinessType")]
        [HiddenInput(DisplayValue = false)]
        public int EventId { get; set; }


        [Required(ErrorMessage = "Please Select Member")]
        [HiddenInput(DisplayValue = false)]
        public int MemberId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MemId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string FileName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string FilePath { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MemberName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string BusinessType { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DepartmentNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MinistryNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MinistryName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ViewType { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string BtnCaption { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string SubmittedBy { get; set; }


        [HiddenInput(DisplayValue = false)]
        public string MainQuestion { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ConstituencyCode { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ConstituencyName { get; set; }

        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Select Date")]
        public int? SessionDateId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? QuestionNumber { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string EventName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MinisterName { get; set; }

        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Enter Date")]
        public string stringDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string AssesmblyName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }

        //[HiddenInput(DisplayValue = false)]
        [JqGridColumnFormatter("$.PriorityFormatter")]
        public int? Priority { get; set; }


        [HiddenInput(DisplayValue = false)]
        public IList<DiaryModelCustom> memberList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public IList<mEvent> eventList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<DiaryModelCustom> DiaryList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mSession> SessList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mSessionDate> SessDateList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mDepartment> DeptList { get; set; }


        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mMinisteryMinisterModel> memMinList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public TimeSpan? TimeForBind { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string CatchWordSubject { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string StringTime { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int CommonId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int UniqueId { get; set; }

        #region Variable Declaration For Notices Grid (Pending and Published.)
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageSize { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageIndex { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int loopStart { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int loopEnd { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int selectedPage { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageNumber { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int RowsPerPage { get; set; }
        #endregion




    }
}
