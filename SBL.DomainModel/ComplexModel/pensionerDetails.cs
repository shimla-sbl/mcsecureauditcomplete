﻿using SBL.DomainModel.Models.Pension;
using System;
using System.Collections.Generic;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class pensionerDetailsviewModel
    {
        //member Details
        public int? pensionerNo { get; set; }

        public string PensionerName { get; set; }

        public int memberCode { get; set; }

        public int? photoCode { get; set; }

        public int? DesignationCode { get; set; }

        public string DesignationName { get; set; }

        public string MobileNo { get; set; }

        public string Address { get; set; }

        public int? constituencyCode { get; set; }

        public string ConstituencyName { get; set; }

        public string description { get; set; }

        // acccount details
        public string BankName { get; set; }

        public string IFSCCode { get; set; }

        public string PPOCode { get; set; }

        public long? bankAccountNumber { get; set; }

        // nominee details
        public string nomineeName { get; set; }

        public string nomineeBankName { get; set; }

        public long? nomineeBankAccountNumber { get; set; }

        public DateTime? firstTermStart { get; set; }

        public DateTime? firstTermEnd { get; set; }
        public string NameofTreasury { get; set; }
        public List<pensionerTenuresDetails> _pensionerTenuresDetails { get; set; }
        public string Mode { get; set; }
    }

}