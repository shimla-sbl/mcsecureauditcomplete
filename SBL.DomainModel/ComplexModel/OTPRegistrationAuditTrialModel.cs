﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace SBL.DomainModel.ComplexModel
{
      [Serializable]
   public partial class OTPRegistrationAuditTrialModel
    {
          public OTPRegistrationAuditTrialModel()
        {

            this.OTPRegistrationModel = new List<OTPRegistrationAuditTrialModel>();
        }
          public OTPRegistrationAuditTrialModel(OTPRegistrationAuditTrialModel OTPList)
        {

            this.DiaryNumber = OTPList.DiaryNumber;
            this.MobileNumber = OTPList.MobileNumber;
            this.MainReplyDocPath = OTPList.MainReplyDocPath;
            this.OTPId = OTPList.OTPId;
          
        }

          public int Id { get; set; }

          [DisplayName("DiaryNumber")]
          public string DiaryNumber { get; set; }

          [StringLength(250)]
          [DisplayName("Asked By")]
          public string MobileNumber { get; set; }

           [HiddenInput(DisplayValue = false)]
          public int AssemblyID { get; set; }

           [HiddenInput(DisplayValue = false)]
          public int SessionID { get; set; }
           [HiddenInput(DisplayValue = false)]
          public int DocTypeId { get; set; }
           [DisplayName("Doc File")]
          public string MainReplyPdfPath { get; set; }
           [HiddenInput(DisplayValue = false)]
          public string MainReplyDocPath { get; set; }

           [DisplayName("OTP ID")]
          public string OTPId { get; set; }
           [HiddenInput(DisplayValue = false)]
          public DateTime? UpdatedDT { get; set; }
           [HiddenInput(DisplayValue = false)]
          public int? RuleId { get; set; }
          [NotMapped]
          [HiddenInput(DisplayValue = false)]
          public virtual ICollection<OTPRegistrationAuditTrialModel> OTPRegistrationModel { get; set; }

            [HiddenInput(DisplayValue = false)]
          public string AddressLocations { get; set; }
          
           [HiddenInput(DisplayValue = false)]
            public string UserName { get; set; }
           [NotMapped]
           public List<OTPRegistrationAuditTrialModel> objList { get; set; }

           [DisplayName("Aadhar Id")]
           public string AadharId { get; set; }
       
        
    }
}
