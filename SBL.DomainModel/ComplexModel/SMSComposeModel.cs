﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.User;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class SMSComposeModel
    {
        public SMSComposeModel()
        {
            this.MinisterContactCol = new List<MinisterContactViewModel>();

            this.MemberContactCol = new List<MemberContactViewModel>();

            this.UserContactCol = new List<mUsers>();
        }

        public string To { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public ICollection<MinisterContactViewModel> MinisterContactCol { get; set; }

        public ICollection<MemberContactViewModel> MemberContactCol { get; set; }

        public ICollection<mUsers> UserContactCol { get; set; }

        public bool SendSMS { get; set; }

        public bool SendEmail { get; set; }
    }
}
