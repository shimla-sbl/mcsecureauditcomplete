﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class PaperLaidPriority
    {
        public long PaperlaidId { get; set; }

        public int Priority { get; set; }

        public int FilteredBy { get; set; }

    }
}
