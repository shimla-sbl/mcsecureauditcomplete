﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Notice;
using System.Web.Mvc;
using SBL.DomainModel.ComplexModel;
using System.ComponentModel;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.UserModule;
using SBL.DomainModel.Models.PaperLaid;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public partial class BillsDraftCustomModel : PaperLaidCounter
    {
        public BillsDraftCustomModel()
        {
            this.mAssembly = new List<mAssembly>();
            this.mSession = new List<mSession>();
            this.mDepartment = new List<mDepartment>();
            this.mMinisteryMinister = new List<mMinisteryMinisterModel>();
            this.ListtPaperLaidV = new List<tPaperLaidV>();
            this.tPaperLaidTemp = new List<tPaperLaidTemp>();
            this.mPaperCategoryTypeList = new List<mPaperCategoryType>();
            this.tMemberNotice = new List<tMemberNotice>();
            this.DepartmentSubmitList = new List<PaperMovementModel>();
            this.PaperLaidByPaperCategoryTypeList = new List<PaperLaidByPaperCategoryType>();

        }


        public BillsDraftCustomModel(BillsDraftCustomModel papLaidV)
        {
            this.NoticeNumber = papLaidV.NoticeNumber;
            this.MemberName = papLaidV.MemberName;
            this.PaperLaidId = papLaidV.PaperLaidId;
            this.MinistryId = papLaidV.MinistryId;
            this.DepartmentId = papLaidV.DepartmentId;
            this.MinistryName = papLaidV.MinistryName;
            this.DeparmentName = papLaidV.DeparmentName;
            this.FileName = papLaidV.FileName;
            this.FilePath = papLaidV.FilePath;
            this.tpaperLaidTempId = papLaidV.tpaperLaidTempId;
            this.FileVersion = papLaidV.FileVersion;
            this.Title = papLaidV.Title;
            this.actualFilePath = papLaidV.actualFilePath;
            this.NoticeID = papLaidV.NoticeID;
            this.Status = papLaidV.Status;
            this.PaperSent = papLaidV.PaperSent;
            this.DeptSubmittedDate = papLaidV.DeptSubmittedDate;
            this.DesireLayingDate = papLaidV.DesireLayingDate;
            this.BussinessType = papLaidV.BussinessType;
            this.BOTStatus = papLaidV.BOTStatus;
            this.OriDiaryFileName = papLaidV.OriDiaryFileName;
            this.OriDiaryFileName = papLaidV.OriDiaryFilePath;
            this.RuleNo = papLaidV.RuleNo;
            this.BillId = papLaidV.BillId;
            this.PaperLaidIdForCheckBox = Convert.ToString(papLaidV.PaperLaidId);
            this.evidhanReferenceNumber = papLaidV.evidhanReferenceNumber;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [HiddenInput(DisplayValue = false)]
        public long PaperLaidId { get; set; }


        [JqGridColumnFormatter("$.BillsNumberForCheckBoxFormatter")]
        [DisplayName("Tick to Send")]
        public string PaperLaidIdForCheckBox { get; set; }

       // [DisplayName("Ref.Number")]
          [HiddenInput(DisplayValue = false)]
        public string evidhanReferenceNumber { get; set; }

        [DisplayName("Rule")]
        public string RuleNo { get; set; }

        [NotMapped]
        [DisplayName("Number")]
        public string NoticeNumber { get; set; }



        [Required()]
        [DisplayName("Subject")]
        [AllowHtml]
        public string Title { get; set; }

        [NotMapped]
        [DisplayName("Asked By")]
        public string MemberName { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? FileVersion { get; set; }

        //Changes based on 13th June 2014 Review
        [DisplayName("Paper")]
        [JqGridColumnLayout(Alignment = JqGridAlignments.Center)]
        [JqGridColumnFormatter("$.DraftBillsFormatter")]
        [NotMapped]
        public string PaperSent { get; set; }

        [DisplayName("Sent on")]
        [JqGridColumnFormatter("$.noticepaperSentOnFormatter")]
        [NotMapped]
        public DateTime? DeptSubmittedDate { get; set; }

        [DisplayName("Desired Date")]
        [JqGridColumnFormatter("$.FixedDateOnFormatter")]
        //[JqGridColumnFormatter("$.paperSentOnFormatter")]
        //[JqGridColumnFormatter("$.noticeFixedDateOnFormatter")]
        public DateTime? DesireLayingDate { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int AssemblyId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SessionId { get; set; }




        [HiddenInput(DisplayValue = false)]
        public int EventId { get; set; }

        [HiddenInput(DisplayValue = false)]
        [Required]
        public int MinistryId { get; set; }

        [StringLength(200)]
        [HiddenInput(DisplayValue = false)]
        public string MinistryName { get; set; }

        [NotMapped]
        [DisplayName("Status")]
        [HiddenInput(DisplayValue = false)]
        public int Status { get; set; }

        [StringLength(200)]
        [HiddenInput(DisplayValue = false)]
        public string MinistryNameLocal { get; set; }

        [Required]
        [StringLength(20)]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ChangedDepartmentId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? ChangedMinisteryId { get; set; }

        [StringLength(200)]
        [HiddenInput(DisplayValue = false)]
        public string DeparmentName { get; set; }

        [StringLength(200)]
        [HiddenInput(DisplayValue = false)]
        public string DeparmentNameByids { get; set; }

        [StringLength(200)]
        [HiddenInput(DisplayValue = false)]
        public string DeparmentNameLocal { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<tCommitteeModel> tCommitteeModel { get; set; }




        [Required()]
        [HiddenInput(DisplayValue = false)]
        [AllowHtml]
        public string Description { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PaperTypeID { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int DesireLayingDateId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? CommitteeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? CommitteeChairmanMemberId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? LOBRecordId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ProvisionUnderWhich { get; set; }


        [HiddenInput(DisplayValue = false)]
        public bool? IsClubbed { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Remark { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? AcknowledgmentBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? AcknowledgmentDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? LaidDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ModifiedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFilePath { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFileName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string NoticePdfPath { get; set; }



        [HiddenInput(DisplayValue = false)]
        public DateTime? ModifiedWhen { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? DeptActivePaperId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? MinisterActivePaperId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? CommitteeActivePaeperId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? LOBPaperTempId { get; set; }

        //Paging Properties
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PAGE_SIZE { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageIndex { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int loopStart { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int loopEnd { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int selectedPage { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PageNumber { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int RowsPerPage { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int OtherPaperCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mAssembly> mAssembly { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mDepartment> mDepartment { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mSession> mSession { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mEvent> mEvents { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<mDocumentType> mDocumentTypes { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<mSessionDate> mSessionDates { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<mBillType> mBillTypes { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<tQuestionType> tQuestioType { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<tQuestion> tQuestion { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<QuestionModelCustom> tQuestionModel { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<PaperSendModelCustom> tQuestionSendModel { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<PaperDraftModelCustom> tQuestionDraftModel { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<NoticePaperSendModelCustom> tNoticeSendModel { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<mNoticeType> mNoticeTypes { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<tMemberNotice> tMemberNotice { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<PaperMovementModel> DepartmentPendingList { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<PaperMovementModel> DepartmentSubmitList { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<PaperLaidByPaperCategoryType> PaperLaidByPaperCategoryTypeList { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public int AssemblyCode { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public int SessionCode { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public int QuestionTypeId { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public int noticeTypeId { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public int QuestionID { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public int BillTypeId { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int NoticeID { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public string categoryType { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public string FileName { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string FilePath { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DSCApplicable { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int Count { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string RespectivePaper { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<SelectListItem> yearList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string actualFilePath { get; set; }

        //Pending List
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mPaperCategoryType> mPaperCategoryTypeList { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int PaperCategoryTypeId { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual List<tPaperLaidV> ListtPaperLaidV { get; set; }



        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<tPaperLaidV> MinisterPendingDataList { get; set; }


        // public IList<mEvent> mCategoryTypeList { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mEvent> mCategoryTypeList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mMinisteryMinisterModel> mMinisteryMinister { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<tPaperLaidTemp> tPaperLaidTemp { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mMinistry> mMinistry { get; set; }

        //Bill variables
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Enter Memorandum Number")]
        public int BillNUmberValue { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int BillNumberYear { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string BillDate { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int MinInboxCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int MinSentCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AssesmblyName { get; set; }

        #region Added By Me :)

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string CurrentUserName { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string UserDesignation { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public PaperMovementModel paperMovementModel { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? QuestionNumber { get; set; }

        #endregion

        #region Added By Venkat

        [HiddenInput(DisplayValue = false)]
        public int DepartmentwisePriority { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int MinistrywisePriority { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int PaperlaidPriority { get; set; }

        #endregion


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string LoginId { get; set; }

        //[NotMapped]
        //[HiddenInput(DisplayValue = false)]
        //public int MinistryID { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string MinisterName { get; set; }




        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public Guid UserID { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string HashKey { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<MinisterNoticecustom> MinisterNoticecustomList { get; set; }

        //Himanshu
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public string EventName { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string MinisterSubmittedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<MinisterPaperMovementModel> MinisterPendingList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string BussinessType { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string lSM { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string CustomMessage { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string ProgressText { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string ProgressValue { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public ICollection<tPaperLaidV> DepartmentProgressList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int BOTStatus { get; set; }


        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<MinisterOtherPaperDraft> MinisterOtherPaperDraft { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<MinisterBillsDraftModel> MinisterBillsDraftModel { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string AssesmblyNameLocal { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string SessionNameLocal { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DiaryNumber { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? SessionDateId { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool IsPermissions { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MergeDiaryNo { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<mAssembly> mAssemblyList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<mSession> sessionList { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public List<mUserModules> MenuList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MemberId { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int ModuleId { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string Subject { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? IsAmendment { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int BillId { get; set; }


        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<tBillRegister> tBillAmendment { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public bool? IsHOD { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsLaid { get; set; }
    }
}
