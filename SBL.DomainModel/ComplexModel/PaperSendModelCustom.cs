﻿using Lib.Web.Mvc.JQuery.JqGrid;
using Lib.Web.Mvc.JQuery.JqGrid.Constants;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using SBL.DomainModel.Models.Question;
using System;
using System.ComponentModel;
using System.Web.Mvc;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class PaperSendModelCustom
    {
        #region Constructors
        public PaperSendModelCustom()
        {
        }
        public PaperSendModelCustom(PaperSendModelCustom questionList)
        {
            this.QuestionID = questionList.QuestionID;
            this.QuestionNumber = questionList.QuestionNumber;
            this.Subject = questionList.Subject;
            this.PaperLaidId = questionList.PaperLaidId;
            this.Version = questionList.Version;
            this.DeptActivePaperId = questionList.DeptActivePaperId;
            this.MemberName = questionList.MemberName;
            this.MinisterSubmittedDate = questionList.MinisterSubmittedDate;
            this.DeptSubmittedDate = questionList.DeptSubmittedDate;
            this.MinisterActivePaperId = questionList.MinisterActivePaperId;
            this.PaperSent = questionList.PaperSent;
            this.FileName = questionList.FileName;
            this.EventName = questionList.EventName;
            this.Status = questionList.Status;
            this.AssemblyCode = questionList.AssemblyCode;
            this.SessionCode = questionList.SessionCode;
            this.QuestionTypeId = questionList.QuestionTypeId;
            this.MinistryId = questionList.MinistryId;
            this.ResultCount = questionList.ResultCount;
            this.PaperLaidIdForCheckBox = Convert.ToString(questionList.PaperLaidId);
            this.DesireLayingDate = questionList.DesireLayingDate;
            this.DiaryNumber = questionList.DiaryNumber;
            this.DeActivateFlag = questionList.DeActivateFlag;
            this.IsPostpone = questionList.IsPostpone;
            this.IsPostponeDate = questionList.IsPostponeDate;
            this.IsClubbed = questionList.IsClubbed;
            this.MergeDiaryNo = questionList.MergeDiaryNo;
            this.SessionDateId = questionList.SessionDateId;
            this.IsBracket = questionList.IsBracket;
            this.DepartmentSubmittedDate = questionList.DepartmentSubmittedDate;
            this.evidhanReferenceNumber = questionList.evidhanReferenceNumber;
            this.DesiredLayingDate = questionList.DesiredLayingDate;
        }
        #endregion

        #region Properties

          [HiddenInput(DisplayValue = false)]
        [JqGridColumnFormatter("$.PaperLaidIdForCheckBoxFormatter")]
        [DisplayName("Tick to send")]
        public string PaperLaidIdForCheckBox { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int QuestionID { get; set; }

        [DisplayName("Diary Number")]
        public string DiaryNumber { get; set; }

        [DisplayName("Ref.Number")]
        public string evidhanReferenceNumber { get; set; }

        [DisplayName("Number")]
        [JqGridColumnFormatter("$.QuestionNumberFormatter")]
        public int? QuestionNumber { get; set; }


      
        public string Subject { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? PaperLaidId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? Version { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? DeptActivePaperId { get; set; }

        [DisplayName("Asked By")]
        public string MemberName { get; set; }
        //New
        [HiddenInput(DisplayValue = false)]
        public DateTime? MinisterSubmittedDate { get; set; }

        [DisplayName("Paper")]
        [JqGridColumnLayout(Alignment = JqGridAlignments.Center)]
       // [JqGridColumnFormatter("$.SendPaperSentFormatter")]
        [JqGridColumnFormatter("$.noticepaperSentFormatter")]
            
        public string PaperSent { get; set; }

       // [DisplayName("Sent on")]
        //[JqGridColumnFormatter(JqGridColumnPredefinedFormatters.Date, SourceFormat = "d.m.Y G:i:s", OutputFormat = "d.m.Y g:i s")]
        //[JqGridColumnFormatter("$.paperSentOnFormatter")]
         [HiddenInput(DisplayValue = false)]
        public DateTime? DeptSubmittedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? MinisterActivePaperId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string FileName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string EventName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int Status { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DeActivateFlag { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsPostpone { get; set; }

         [HiddenInput(DisplayValue = false)]
        public DateTime? IsPostponeDate { get; set; }



         [HiddenInput(DisplayValue = false)]
         public string ReferenceMemberCode { get; set; }
       

        //Added by uday
        [HiddenInput(DisplayValue = false)]
        public int AssemblyCode { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SessionCode { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int QuestionTypeId { get; set; }

       

        [HiddenInput(DisplayValue = false)]
        public int MinistryId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsClubbed { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsBracket { get; set; }
        #endregion        

       
        [HiddenInput(DisplayValue = false)]
        public DateTime? NoticeDate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? MemberId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string MainQuestion { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int EventId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public TimeSpan? NoticeTime { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsContentFreeze { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? Stats { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string NoticeNumber { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int NoticeId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string Notice { get; set; }
        [HiddenInput(DisplayValue = false)]
        public Guid UserId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string UserName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public Guid UId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFilePath { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFileName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string TypistUserId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string SubjectCatchWords { get; set; }

        //Himanshu
          [HiddenInput(DisplayValue = false)]
        public string MemberNameLocal { get; set; }
          [HiddenInput(DisplayValue = false)]
        public string ConstituencyName { get; set; }
          [HiddenInput(DisplayValue = false)]
        public string ConstituencyName_Local { get; set; }

          //Sujeet
          //[DisplayName("Fixed Date")]
          //[JqGridColumnFormatter("$.SendFixedDateOnFormatter")]
        [HiddenInput(DisplayValue = false)]
          public DateTime? DesireLayingDate { get; set; }

          [DisplayName("Number")]
          [HiddenInput(DisplayValue = false)]
          public int? ConvertQuestionNumber { get; set; }

          [HiddenInput(DisplayValue = false)]
          public string MergeDiaryNo { get; set; }

          [HiddenInput(DisplayValue = false)]
          public int? SessionDateId { get; set; }

          [DisplayName("Reply Sent on")]
          // [HiddenInput(DisplayValue = false)]
          public string DepartmentSubmittedDate { get; set; }

          [DisplayName("Fixed Date")]
          // [HiddenInput(DisplayValue = false)]
          public string DesiredLayingDate { get; set; }

         
          [HiddenInput(DisplayValue = false)]
          public long PaperLaidTempId { get; set; }
    }
}
