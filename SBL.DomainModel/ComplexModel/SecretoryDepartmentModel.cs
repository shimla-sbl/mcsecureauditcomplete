﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class SecretoryDepartmentModel
    {
        public SecretoryDepartmentModel()
        {
            this.SecretoryDepartmentModels = new List<SecretoryDepartmentModel>();
        }

        public int SecretoryId { get; set; }
        public string DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public virtual ICollection<SecretoryDepartmentModel> SecretoryDepartmentModels { get; set; }
    }
}
