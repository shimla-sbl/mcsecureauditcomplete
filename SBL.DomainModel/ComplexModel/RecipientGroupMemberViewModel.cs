﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.RecipientGroups;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class RecipientGroupMemberViewModel
    {
        public RecipientGroupMemberViewModel(){

            this.RecipientGroupCol = new List<RecipientGroup>();

            this.RecipientGroupMemberCol = new List<RecipientGroupMemberViewModel>();

            this.DepartmentListCol = new List<mDepartment>();

            this.GenderList = new List<string>();
        }

        public int GroupMemberID { get; set; }

        public int GroupId { get; set; }

        public string EditMode { get; set; }

        public List<mDepartment> DepartmentListCol { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public string Designation { get; set; }

        public string EMail { get; set; }

        public string Mobile { get; set; }

        public string Address { get; set; }

        public string PinCode { get; set; }

        public string DepartmentID { get; set; }

        public bool IsActive { get; set; }

        public List<RecipientGroup> RecipientGroupCol { get; set; }

        public List<RecipientGroupMemberViewModel> RecipientGroupMemberCol { get; set; }

        public List<String> GenderList { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string Message { get; set; }

        public int RecipientGroupMemberCnt { get; set; }

		public int? ConstituencyCode { get; set; }

		public string Officecode { get; set; }

		public int? RankingOrder { get; set; }

		public string LandLineNumber { get; set; }

		public string FileName { get; set; }

		public string FileType { get; set; }

		public int FileLength { get; set; }

		public System.Data.DataTable DataTableDt { get; set; }

		public List<ErrorList> ErrorList { get; set; }
		
    }

	public class ErrorList
	{
		public string GroupMemberName { get; set; }
		public string Gender { get; set; }
		public string Designation { get; set; }
		public string Email { get; set; }

		public string Mobile { get; set; }
		public string Address { get; set; }
		public string Pincode { get; set; }
		public string RankingOrder { get; set; }
		public string LandLineNo { get; set; }
		public string Error { get; set; }
	}
}
