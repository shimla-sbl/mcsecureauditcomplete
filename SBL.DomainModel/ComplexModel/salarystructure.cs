﻿using System;
using System.Collections.Generic;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class salarystructure
    {
        public int memberCode { get; set; }
        public string memberName { get; set; }
        public string designationName { get; set; }
        public int monthID { get; set; }
        public int yearId { get; set; }
        public int catID { get; set; }
        public double totalDeduction { get; set; }
        public double totalAllowances { get; set; }
        public double netSalary { get; set; }
        public string netSalaryInWords { get; set; }
        public bool approved { get; set; }
        public IEnumerable<HeadList> headList { get; set; }
        public string Mode { get; set; }
        public string mobileNO { get; set; }
        public string email { get; set; }
        public int ispart { get; set; }
        public int AssemblyId { get; set; }
        
    }
    [Serializable]
    public class HeadList
    {
        public int headID { get; set; }
        public string headName { get; set; }
        public string htype { get; set; }
        public double amount { get; set; }
        public string subType { get; set; }
        public long? loanNumber { get; set; }


    }
}