﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel.Conzstituency
{
    public class ConsistuencyWorks
    {
    }

    [Serializable]
    public class DepartmentWiseWorksCummalativeProgressD
    {
        private string _deptId = string.Empty;

        private string _deptName = string.Empty;

        private string _totalWorks = string.Empty;

        private string _sanctionedAmount = string.Empty;

        public string DeptId
        {
            get { return _deptId; }
            set { _deptId = value; }
        }

        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }

        public string TotalWorks
        {
            get { return _totalWorks; }
            set { _totalWorks = value; }
        }

        public string SanctionedAmount
        {
            get { return _sanctionedAmount; }
            set { _sanctionedAmount = value; }
        }
    }

    [Serializable]
    public class DepartmentWiseWorksProgressD
    {
        private string _schemeConstituencyId = string.Empty;

        private string _workName = string.Empty;

        private string _sanctionedAmount = string.Empty;

        private string _workStatus = string.Empty;

        public string SchemeConstituencyId
        {
            get { return _schemeConstituencyId; }
            set { _schemeConstituencyId = value; }
        }

        public string WorkName
        {
            get { return _workName; }
            set { _workName = value; }
        }

        public string SanctionedAmount
        {
            get { return _sanctionedAmount; }
            set { _sanctionedAmount = value; }
        }

        public string WorkStatus
        {
            get { return _workStatus; }
            set { _workStatus = value; }
        }
    }

    [Serializable]
    public class WorkDetailsD
    {
        private string _controllingAuthorityName = string.Empty;

        private string _financialYear = string.Empty;

        private string _demand = string.Empty;

        private string _workCode = string.Empty;

        private string _workName = string.Empty;

        private string _programmeName = string.Empty;

        private string _workNatureName = string.Empty;

        private string _ownerDeptName = string.Empty;

        private string _executingDeptName = string.Empty;

        //private string _officeName = string.Empty;

        //private string _distictName = string.Empty;

        private string _officer = string.Empty;

        // private string _constituencyName = string.Empty;

        private string _panchayatName = string.Empty;

        private string _estimatedAmount = string.Empty;

        private string _estimatedCompletionDate = string.Empty;

        private string _sanctionedAmount = string.Empty;

        private string _sanctionedDate = string.Empty;

        private string _expenditureDuringPreviousYear = string.Empty;

        private string _expenditureDuringCurrentYear = string.Empty;

        private string _totalExpenditure = string.Empty;

        private string _physicalProgressUptoPreviousYear = string.Empty;

        private string _physicalProgressDuringCurrentYear = string.Empty;

        private string _totalPhysicalProgress = string.Empty;

        private string _workStatus = string.Empty;

        private string _imageUrl = string.Empty;


        private string _ConstituencyName = string.Empty;

        private string _revisedAmount = string.Empty;

        public string ControllingAuthorityName
        {
            get { return _controllingAuthorityName; }
            set { _controllingAuthorityName = value; }
        }

        public string FinancialYear
        {
            get { return _financialYear; }
            set { _financialYear = value; }
        }

        public string Demand
        {
            get { return _demand; }
            set { _demand = value; }
        }

        public string WorkCode
        {
            get { return _workCode; }
            set { _workCode = value; }
        }

        public string WorkName
        {
            get { return _workName; }
            set { _workName = value; }
        }

        public string ProgrammeName
        {
            get { return _programmeName; }
            set { _programmeName = value; }
        }

        public string WorkNatureName
        {
            get { return _workNatureName; }
            set { _workNatureName = value; }
        }

        public string OwnerDeptName
        {
            get { return _ownerDeptName; }
            set { _ownerDeptName = value; }
        }

        public string ExecutingDeptName
        {
            get { return _executingDeptName; }
            set { _executingDeptName = value; }
        }

        //public string OfficeName
        //{
        //    get { return _officeName; }
        //    set { _officeName = value; }
        //}

        //public string DistictName
        //{
        //    get { return _distictName; }
        //    set { _distictName = value; }
        //}

        public string Officer
        {
            get { return _officer; }
            set { _officer = value; }
        }

        //public string ConstituencyName
        //{
        //    get { return _constituencyName; }
        //    set { _constituencyName = value; }
        //}

        public string PanchayatName
        {
            get { return _panchayatName; }
            set { _panchayatName = value; }
        }

        public string EstimatedAmount
        {
            get { return _estimatedAmount; }
            set { _estimatedAmount = value; }
        }

        public string EstimatedCompletionDate
        {
            get { return _estimatedCompletionDate; }
            set { _estimatedCompletionDate = value; }
        }

        public string SanctionedAmount
        {
            get { return _sanctionedAmount; }
            set { _sanctionedAmount = value; }
        }

        public string SanctionedDate
        {
            get { return _sanctionedDate; }
            set { _sanctionedDate = value; }
        }

        public string ExpenditureDuringPreviousYear
        {
            get { return _expenditureDuringPreviousYear; }
            set { _expenditureDuringPreviousYear = value; }
        }

        public string ExpenditureDuringCurrentYear
        {
            get { return _expenditureDuringCurrentYear; }
            set { _expenditureDuringCurrentYear = value; }
        }

        public string TotalExpenditure
        {
            get { return _totalExpenditure; }
            set { _totalExpenditure = value; }
        }

        public string PhysicalProgressUptoPreviousYear
        {
            get { return _physicalProgressUptoPreviousYear; }
            set { _physicalProgressUptoPreviousYear = value; }
        }

        public string PhysicalProgressDuringCurrentYear
        {
            get { return _physicalProgressDuringCurrentYear; }
            set { _physicalProgressDuringCurrentYear = value; }
        }

        public string TotalPhysicalProgress
        {
            get { return _totalPhysicalProgress; }
            set { _totalPhysicalProgress = value; }
        }

        public string WorkStatus
        {
            get { return _workStatus; }
            set { _workStatus = value; }
        }

        public string ImageUrl
        {
            get { return _imageUrl; }
            set { _imageUrl = value; }
        }
        public string ConstituencyName
        {
            get { return _ConstituencyName; }
            set { _ConstituencyName = value; }
        }
        public string revisedAmount
        {
            get { return _revisedAmount; }
            set { _revisedAmount = value; }
        }
    }
}
