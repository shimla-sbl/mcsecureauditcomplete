﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.ConstituencyVS;
using System.ComponentModel.DataAnnotations;


namespace SBL.DomainModel.ComplexModel.Constituency
{
    [Serializable]
    public class schemeMapping
    {
        public int? memberCode { get; set; }
        public string FinancialYear { get; set; }

        public string startYear { get; set; }

        public string ControllingAuthorityID { get; set; }

        public long? vsConstituencyID { get; set; }

        public long? eConstituencyID { get; set; }

        public long? districtID { get; set; }

        public int count { get; set; }

        public string constituenctName { get; set; }

        public IEnumerable<vsSchemeList> vsSchemeList { get; set; }

        public IEnumerable<myShemeList> mySchemeList { get; set; }
    }

    [Serializable]
    public class vsSchemeList
    {
        public long vsSchemeID { get; set; }

        public string workCode { get; set; }

        public string workName { get; set; }

        public double? sanctionedAmount { get; set; }

        public double? EstimatedAmount { get; set; }

        public bool picked { get; set; }
    }

    [Serializable]
    public class myShemeList
    {
        public long mySchemeID { get; set; }

        public string workCode { get; set; }

        public string workName { get; set; }

        public string programName { get; set; }

        public long? Panchayat { get; set; }

        public string PanchayatName { get; set; }
        public string WardName { get; set; }

        public long? districtID { get; set; }

        public double? sanctionedAmount { get; set; }

        public string sanctionedDate { get; set; }

        public string CompletionDate { get; set; }

        public double? physicalProgress { get; set; }

        public double? financialProgress { get; set; }

        public double? EstimatedAmount { get; set; }

        public string ExecutiveDepartment { get; set; }

        public string oWnerDepartment { get; set; }

        public string ExecutiveOffice { get; set; }

        public string financialYear { get; set; }

        public string workStatus { get; set; }

        public string dprFileName { get; set; }

        public string SalientFeatures { get; set; }

        public string ControllingAuthorityId { get; set; }

        public long? workStatusCode { get; set; }

        public long constituencyID { get; set; }

        public double? ExecutiveOfficeId { get; set; }

        public int? imageCount { get; set; }

        public string CompletionFinancialYear { get; set; }
        public int? subdevisionId { get; set; }
        public int? schemeTypeId { get; set; }
        [NotMapped]
        public string ExecutiveDepartmentName { get; set; }
        [NotMapped]
        public string ExecutiveOfficeName { get; set; } 
            [NotMapped]
        public string ProgrammeName { get; set; }
            [NotMapped]
            public string WorkType { get; set; }
    }

    public class Client
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }

    [Serializable]
    public class WorkDetails
    {
        public string FinancialYear { get; set; }

        public int FinancialYearID { get; set; }

        public string ControllingAuthorityID { get; set; }

        public string ControllingAuthorityName { get; set; }

        public long? vsConstituencyID { get; set; }

        public long? eConstituencyID { get; set; }

        public string constituenctName { get; set; }

        public long mySchemeID { get; set; }

        public string workCode { get; set; }

        public string workName { get; set; }

        public string WorkType { get; set; }

        public string WorkTypeID { get; set; }

        public string WorkNatureID { get; set; }

        public string programName { get; set; }

        public long? Panchayat { get; set; }

        public String PanchayatName { get; set; }

        public long? districtID { get; set; }

        public double? sanctionedAmount { get; set; }

        public double? revisedAmount { get; set; }

        public string sanctionedDate { get; set; }

        public string EstimatedCompletionDate { get; set; }

        
        public double? EstimatedAmount { get; set; }

        public double? ExpenditureUptoPreviousYears { get; set; }

        public double? ExpenditureInCurrentYears { get; set; }

        public double? TotalExpenditureAmount { get; set; }

        public double? PhysicalUptoPreviousYears { get; set; }

        public double? PhysicalInCurrentYears { get; set; }

        public double? TotalPhysicalProgress { get; set; }

        //Madhur
        public double? TotalFundReleased { get; set; }

        public String Owner_deptid { get; set; }

        public long? ExecutingOfficeID { get; set; }

        public string[] OfficerIDList { get; set; }

        public String ExecutingDepartmentId { get; set; }

        public string Owner_deptName { get; set; }

        public string ExecutingDepartmentName { get; set; }

        public string Mode { get; set; }

        public string userid { get; set; }

        public long Demand { get; set; }
        public string demandName { get; set; }

        public int workStatus { get; set; }

        public string workStatusText { get; set; }

        public int? memberCode { get; set; }

        public bool isMember { get; set; }

        public string districtName { get; set; }
        public string ExecutingOfficeName { get; set; }

        public string ImagePath { get; set; }
        public string dprFile { get; set; }
        public string featuresfile { get; set; } //Madhur

        public string ContractorAddress { get; set; }

        public string ContractorName { get; set; }

        public string completionDate { get; set; }

        public int? subdevisionId { get; set; }
        public int? schemeTypeId { get; set; }
        public int? AssemblyId { get; set; }
        public int? StateCode { get; set; }
        [NotMapped]
        public string SubdivisionName { get; set; }
        [NotMapped]
        public string ConId_ForSDM_ { get; set; }

        [NotMapped]
        public List<ConstituencyWorkProgressImages> Imagelist { get; set; }
        [NotMapped]
        public List<ConstituencyWorkProgress> WorkProgresslist { get; set; }
         [NotMapped]
        public string ConId_ForSDM { get; set; }
    }
}