﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace SBL.DomainModel.ComplexModel.LinqKitSource
{
	/// <summary>
	/// </summary>
	public static class Linq
	{
		// Returns the given anonymous method as a lambda expression
		public static Expression<Func<T, TResult>> Expr<T, TResult> (Expression<Func<T, TResult>> expr)
		{
			return expr;
		}

		// Returns the given anonymous function as a Func delegate
		public static Func<T, TResult> Func<T, TResult> (Func<T, TResult> expr)
		{
			return expr;
		}
	}
}
