﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.Resourcefile
{
    public class Resourcefile
    {
        public string ResourceFileName { get; set; }
        public List<SelectListItem> ResourceList { get; set; }
    }
}