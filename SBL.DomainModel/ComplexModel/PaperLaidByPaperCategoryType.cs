﻿using Lib.Web.Mvc.JQuery.JqGrid;
using Lib.Web.Mvc.JQuery.JqGrid.Constants;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel
{
    [Serializable]
    public class PaperLaidByPaperCategoryType
    {

        public PaperLaidByPaperCategoryType()
        {
            this.ListBills = new List<PaperLaidByPaperCategoryType>();
        }

        public PaperLaidByPaperCategoryType(PaperLaidByPaperCategoryType BillsList)
        {

            this.PaperLaidId = BillsList.PaperLaidId;
            this.NoticeNumber = BillsList.NoticeNumber;
            this.Subject = BillsList.Title;

            this.PaperLaidId = BillsList.PaperLaidId;
            this.Version = BillsList.Version;
            this.MemberName = BillsList.MemberName;

            this.DeptSubmittedDate = BillsList.DeptSubmittedDate;
            this.MinisterActivePaperId = BillsList.MinisterActivePaperId;
            this.PaperSent = BillsList.PaperSent;
            this.MinisterSubmittedDate = BillsList.MinisterSubmittedDate;
            this.FileName = BillsList.FileName;
            this.EventName = BillsList.EventName;
          
            this.AssemblyCode = BillsList.AssemblyCode;
            this.Status = BillsList.Status;
            this.SessionCode = BillsList.SessionCode;
          
            this.MinistryId = BillsList.MinistryId;
            this.ResultCount = BillsList.ResultCount;
            this.PaperLaidIdForCheckBox = Convert.ToString(BillsList.PaperLaidId);
            this.DesireLayingDate = BillsList.DesireLayingDate;
            this.MinisterSubmittedDatemini = BillsList.MinisterSubmittedDatemini;
            this.evidhanReferenceNumber = BillsList.evidhanReferenceNumber;
       
        }


        public ICollection<PaperLaidByPaperCategoryType> ListBills { get; set; }

        [HiddenInput(DisplayValue = false)]
        [JqGridColumnFormatter("$.PaperLaidIdForCheckBoxFormatter")]
        [DisplayName("")]
        public string PaperLaidIdForCheckBox { get; set; }

       
        [DisplayName("Number")]
        public string NoticeNumber { get; set; }

      
        [DisplayName("Ref.Number")]
        public string evidhanReferenceNumber { get; set; }

      
        public string Subject { get; set; }       
        
        [HiddenInput(DisplayValue = false)]
        public string MemberName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? Version { get; set; }


        [DisplayName("Paper Sent")]
        [JqGridColumnLayout(Alignment = JqGridAlignments.Center)]
        [JqGridColumnFormatter("$.paperSentFormatter")]
        public string PaperSent { get; set; }

      


        [DisplayName("Sent on")]
        //[JqGridColumnFormatter(JqGridColumnPredefinedFormatters.Date, SourceFormat = "d.m.Y G:i:s", OutputFormat = "d.m.Y g:i s")]
       // [JqGridColumnFormatter("$.paperSentOnFormatter")]
        [HiddenInput(DisplayValue = false)]
        public DateTime? MinisterSubmittedDate { get; set; }

           [HiddenInput(DisplayValue = false)]
        public string Id { get; set; }

           [HiddenInput(DisplayValue = false)]
        public string Name { get; set; }

           [HiddenInput(DisplayValue = false)]
        public int Count { get; set; }

           [HiddenInput(DisplayValue = false)]
        public bool IsRecieved { get; set; }

           [HiddenInput(DisplayValue = false)]
        public bool IsCurrentLob { get; set; }

           [HiddenInput(DisplayValue = false)]
           public bool IsLaidInHome { get; set; }

           [HiddenInput(DisplayValue = false)]
           public int Status { get; set; }
          
        //ForMinisterNotice
           [HiddenInput(DisplayValue = false)]
        public long PaperLaidId { get; set; }
           [HiddenInput(DisplayValue = false)]
        public int MinistryId { get; set; }
           [HiddenInput(DisplayValue = false)]
        public string MinistryName { get; set; }
           [HiddenInput(DisplayValue = false)]
        public string DepartmentId { get; set; }
           [HiddenInput(DisplayValue = false)]
        public string DeparmentName { get; set; }



           [HiddenInput(DisplayValue = false)]
        public string FileName { get; set; }
           [HiddenInput(DisplayValue = false)]
        public long tpaperLaidTempId { get; set; }
           [HiddenInput(DisplayValue = false)]
        public int? FileVersion { get; set; }
           [HiddenInput(DisplayValue = false)]
        public string Title { get; set; }
           [HiddenInput(DisplayValue = false)]
        public DateTime? DeptSubmittedDate { get; set; }




           [HiddenInput(DisplayValue = false)]
        public string EventName { get; set; }
           [HiddenInput(DisplayValue = false)]
        public string FilePath { get; set; }

        //uday
         

        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int loopStart { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int loopEnd { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int AssemblyCode { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SessionCode { get; set; }
        [HiddenInput(DisplayValue = false)]
        public long? MinisterActivePaperId { get; set; }

      //  [JqGridColumnFormatter("$.FixedDateOnFormatter")]
        [HiddenInput(DisplayValue = false)]
        public DateTime? DesireLayingDate { get; set; }


        [DisplayName("Sent on")]
        public string MinisterSubmittedDatemini { get; set; }

         [HiddenInput(DisplayValue = false)]
        public string DesiredLayingDate { get; set; }

       

    }
}
