﻿using Lib.Web.Mvc.JQuery.JqGrid;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Question;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace SBL.DomainModel.ComplexModel
{
     [Serializable]
   public class OtherPaperDraftCustom
   {
       public OtherPaperDraftCustom()
        {
            this.SubmittedFileListByPaperLaidId = new List<tPaperLaidTemp>();
            this.ListPaperMovementModel = new List<PaperMovementModel>();
            this.PaperMovementCollection = new List<PaperMovementModel>();
            this.ListtQuestion = new List<tQuestion>();
        }

       public OtherPaperDraftCustom(OtherPaperDraftCustom model)
        {
            this.PaperLaidId = model.PaperLaidId;
            this.Title = model.Title;
            this.DeptActivePaperId = model.DeptActivePaperId;
            this.actualFilePath = model.actualFilePath;
            this.version = model.version;
            this.FilePath = model.FilePath;
            this.DeptSubmittedDate = model.DeptSubmittedDate;
            this.BusinessType = model.BusinessType;
            this.DeptSubmittedBy = model.DeptSubmittedBy;
            this.Status = model.Status;
            this.PaperLaidIdForCheckBox = Convert.ToString(model.PaperLaidId);
            this.DesiredDateId = model.DesiredDateId;
            this.DesireLayingDate = model.DesireLayingDate;
            this.DiaryNumber = model.DiaryNumber;
            this.QuestionNumber = model.QuestionNumber;
            this.DepartmentSubmittedDate = model.DepartmentSubmittedDate;
            this.DesiredLayingDate = model.DesiredLayingDate;
            this.evidhanReferenceNumber = model.evidhanReferenceNumber;
        }

       [JqGridColumnFormatter("$.OtherPaperForCheckBoxFormatter")]
       [DisplayName("Tick to Send")]
       public string PaperLaidIdForCheckBox { get; set; }

       [NotMapped]
       [HiddenInput(DisplayValue = false)]
       public string evidhanReferenceNumber { get; set; }

       [HiddenInput(DisplayValue = false)]
      // [DisplayName("Number")]
       public long PaperLaidId { get; set; }

       [DisplayName("Subject")]
       public string Title { get; set; }

       [DisplayName("Business Type")]
       public string BusinessType { get; set; }

       [DisplayName("Paper")]
       [JqGridColumnLayout(Alignment = JqGridAlignments.Center)]
       [JqGridColumnFormatter("$.OtherPaperDraftFormatter")]
       public string actualFilePath { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int? version { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string FilePath { get; set; }

       // [DisplayName("Sent On")]
       //  [JqGridColumnFormatter("$.paperSentOnFormatter")]
       [HiddenInput(DisplayValue = false)]
       public DateTime? DeptSubmittedDate { get; set; }

       // [DisplayName("Desire Date")]
       // [JqGridColumnFormatter("$.FixedDateOnFormatter")]
       [HiddenInput(DisplayValue = false)]
       public DateTime? DesireLayingDate { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string DeptSubmittedBy { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int AssemblyId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string AssemblyName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int SessionId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string SessionName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int Status { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int EventId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string EventName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int MinistryId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string MinistryName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string MinistryNameLocal { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string MinisterName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string MinisterNameLocal { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string DepartmentId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string DeparmentName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string DeparmentNameLocal { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string Description { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int? PaperTypeID { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string PaperTypeName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int? CommitteeId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string CommitteeName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int? CommitteeChairmanMemberId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int LOBRecordId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int? AcknowledgmentBy { get; set; }

       [HiddenInput(DisplayValue = false)]
       public DateTime? AcknowledgmentDate { get; set; }

       [HiddenInput(DisplayValue = false)]
       public DateTime? LaidDate { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int? ModifiedBy { get; set; }

       [HiddenInput(DisplayValue = false)]
       public DateTime? ModifiedWhen { get; set; }

       //[DisplayName("Number")]
       [HiddenInput(DisplayValue = false)]
       public long? DeptActivePaperId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public long? MinisterActivePaperId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int PAGE_SIZE { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int PageIndex { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int ResultCount { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int loopStart { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int loopEnd { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int selectedPage { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int PageNumber { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int RowsPerPage { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string FileName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int? FileVersion { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int? PaperCategoryTypeId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string PaperCategoryTypeName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string Number { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int? QuestionNumber { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string MinisterSubmittedBy { get; set; }

       [HiddenInput(DisplayValue = false)]
       public DateTime? MinisterSubmittedDate { get; set; }

       [HiddenInput(DisplayValue = false)]
       public DateTime? PaperSubmittedDate { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string ProvisionUnderWhich { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string Remark { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int QuestionTypeId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string ReplyPathFileLocation { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string MemberName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int DesiredDateId { get; set; }

       //nitin
       [HiddenInput(DisplayValue = false)]
       public ICollection<tPaperLaidTemp> SubmittedFileListByPaperLaidId { get; set; }
       public ICollection<PaperMovementModel> PaperMovementCollection { get; set; }
       public ICollection<PaperMovementModel> ListPaperMovementModel { get; set; }
       public ICollection<tQuestion> ListtQuestion { get; set; }


       //uday

       [HiddenInput(DisplayValue = false)]
       public string HashKey { get; set; }

       //[JqGridColumnFormatter("$.PaperLaidIdForCheckBoxFormatter")]
       //[DisplayName("")]
       //[HiddenInput(DisplayValue = false)]
       //public string PaperLaidIdForCheckBox { get; set; }

       [HiddenInput(DisplayValue = false)]
       public ICollection<MovementDetailsCustom> ListMovementDetails { get; set; }
       [HiddenInput(DisplayValue = false)]
       public string AadarId { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string DiaryNumber { get; set; }


       [HiddenInput(DisplayValue = false)]
       public string ReferencedNumber { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string ReferenceMemberCode { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string MergeQuestion { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string CDiaryNo { get; set; }

       [HiddenInput(DisplayValue = false)]
       public bool? IsClubbed { get; set; }

       [HiddenInput(DisplayValue = false)]
       public virtual ICollection<tQuestion> tQuestionModel { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string RMemberName { get; set; }

       [HiddenInput(DisplayValue = false)]
       public string MergeDiaryNo { get; set; }

       [DisplayName("Sent on")]
       // [HiddenInput(DisplayValue = false)]
       public string DepartmentSubmittedDate { get; set; }

       [DisplayName("Desire Date")]
       // [HiddenInput(DisplayValue = false)]
       public string DesiredLayingDate { get; set; }


       [NotMapped]
       [HiddenInput(DisplayValue = false)]
       public int TotalOtherPaperSend { get; set; }

       [HiddenInput(DisplayValue = false)]
       public int OtherPaperSentCount { get; set; }

   }
}
