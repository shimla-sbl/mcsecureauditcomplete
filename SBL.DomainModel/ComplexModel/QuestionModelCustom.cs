﻿using Lib.Web.Mvc.JQuery.JqGrid;
using Lib.Web.Mvc.JQuery.JqGrid.Constants;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using SBL.DomainModel.Models.Question;
using System;
using System.ComponentModel;
using System.Web.Mvc;
using SBL.DomainModel.Models.Notice;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class QuestionModelCustom
    {
        #region Constructors
        public QuestionModelCustom()
        {
        }
        public QuestionModelCustom(QuestionModelCustom questionList)
        {
            this.QuestionID = questionList.QuestionID;
            this.QuestionNumber = questionList.QuestionNumber;

            this.Subject = questionList.Subject;
            this.PaperLaidId = questionList.PaperLaidId;
            this.Version = questionList.Version;
            this.DeptActivePaperId = questionList.DeptActivePaperId;
            this.MemberName = questionList.MemberName;
            this.MinisterSubmittedDate = questionList.MinisterSubmittedDate;
            this.DeptSubmittedDate = questionList.DeptSubmittedDate;
            this.MinisterActivePaperId = questionList.MinisterActivePaperId;
            this.PaperSent = questionList.PaperSent;
            this.FileName = questionList.FileName;
            this.EventName = questionList.EventName;
            this.Status = questionList.Status;
            this.AssemblyCode = questionList.AssemblyCode;
            this.SessionCode = questionList.SessionCode;
            this.QuestionTypeId = questionList.QuestionTypeId;
            this.MinistryId = questionList.MinistryId;
            this.ResultCount = questionList.ResultCount;
            this.DesireLayingDate = questionList.DesireLayingDate;
            this.DiaryNumber = questionList.DiaryNumber;
            this.SessionDateId = questionList.SessionDateId;
            this.DeActivateFlag = questionList.DeActivateFlag;
            this.IsPostpone = questionList.IsPostpone;
            this.IsPostponeDate = questionList.IsPostponeDate;
            this.IsClubbed = questionList.IsClubbed;
            this.MergeDiaryNo = questionList.MergeDiaryNo;
            this.IsBracket = questionList.IsBracket;
            this.DepartmentSubmittedDate = questionList.DepartmentSubmittedDate;
            this.DesiredLayingDate = questionList.DesiredLayingDate;
            this.evidhanReferenceNumber = questionList.evidhanReferenceNumber;
            this.SubmittedDate = questionList.SubmittedDate;
            this.SubmittedDateString = questionList.SubmittedDateString;
            this.IsAcknowledgmentDate = questionList.IsAcknowledgmentDate;
            this.IsAcknowledgmentDateString = questionList.IsAcknowledgmentDateString;
          

        }
        #endregion

        #region Properties
        [HiddenInput(DisplayValue = false)]
        public int QuestionID { get; set; }

        [DisplayName("Diary Number")]
        public string DiaryNumber { get; set; }

       // [DisplayName("Ref.Number")]
          [HiddenInput(DisplayValue = false)]
        public string evidhanReferenceNumber { get; set; }

        [DisplayName("Number")]
        [JqGridColumnFormatter("$.AllQuestionNumberFormatter")]
         public int? QuestionNumber { get; set; }


      
        public string Subject { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? PaperLaidId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? Version { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? DeptActivePaperId { get; set; }

        [DisplayName("Asked By")]
        public string MemberName { get; set; }
        //New
        [HiddenInput(DisplayValue = false)]
        public DateTime? MinisterSubmittedDate { get; set; }

        //Changes based on 13th June 2014 Review
        [DisplayName("Paper")]
        [JqGridColumnLayout(Alignment = JqGridAlignments.Center)]
        [JqGridColumnFormatter("$.questionpaperSentFormatter")]
        public string PaperSent { get; set; }

        //[DisplayName("Sent on")]
        [HiddenInput(DisplayValue = false)]
        //[JqGridColumnFormatter(JqGridColumnPredefinedFormatters.Date, SourceFormat = "d.m.Y G:i:s", OutputFormat = "d.m.Y g:i s")]
        [JqGridColumnFormatter("$.paperSentOnFormatter")]
        public DateTime? DeptSubmittedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? MinisterActivePaperId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string FileName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string EventName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int Status { get; set; }

        //Added by uday

        [HiddenInput(DisplayValue = false)]
        public string DeActivateFlag { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsPostpone { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? IsPostponeDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int AssemblyCode { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SessionCode { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int QuestionTypeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int MinistryId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }


        [HiddenInput(DisplayValue = false)]
        public bool? IsClubbed { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? IsBracket { get; set; }
        #endregion        

        //Added By Sammer        
      
        [HiddenInput(DisplayValue = false)]
        public DateTime? NoticeDate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? MemberId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string MainQuestion { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int EventId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public TimeSpan? NoticeTime { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsContentFreeze { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? Stats { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string NoticeNumber { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int NoticeId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string Notice { get; set; }
        [HiddenInput(DisplayValue = false)]
        public Guid UserId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string UserName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public Guid UId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFilePath { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFileName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string TypistUserId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string SubjectCatchWords { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsSitting { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DepartmentId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? MinisterID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool Checked { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string MergeQuestion { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string CDiaryNo { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string RefMemCode { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string CMemName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string BrackWithQIds { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string MainDiaryNo { get; set; }
       
        [HiddenInput(DisplayValue = false)]
        public bool? IsBracketed { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string BracketedDNo { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime? SessionDate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string SessionDateLocal { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string SfilePath { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsSecApproved { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsHindi { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string CMemNameHindi { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string DepartmentName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string MinistryNameLocal { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string MinistryName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime? Updatedate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string MinisterName { get; set; }
        //Himanshu
          [HiddenInput(DisplayValue = false)]
        public string MemberNameLocal { get; set; }
          [HiddenInput(DisplayValue = false)]
        public string ConstituencyName { get; set; }
          [HiddenInput(DisplayValue = false)]
        public string ConstituencyName_Local { get; set; }

          //Sujeet
          [HiddenInput(DisplayValue = false)]
         // [DisplayName("Fixed Date")]
         // [JqGridColumnFormatter("$.FixedDateOnFormatter")]
          public DateTime? DesireLayingDate { get; set; }

         [HiddenInput(DisplayValue = false)]
          public int? SessionDateId { get; set; }
         [HiddenInput(DisplayValue = false)]
         public int? QuestionStatus { get; set; }
         [HiddenInput(DisplayValue = false)]
         public bool? IsTypistFreeze { get; set; }

      

         [HiddenInput(DisplayValue = false)]
         public string ReferenceMemberCode { get; set; }

         [HiddenInput(DisplayValue = false)]
         public string RMemberName { get; set; }

         [HiddenInput(DisplayValue = false)]
         public string MergeDiaryNo { get; set; }

         [DisplayName("Sent By MC")]
       
         public string SubmittedDateString { get; set; }
       
         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public DateTime? QSubmitedDate { get; set; }
         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public DateTime? QFixDate { get; set; }
         [DisplayName("Fixed Date")]
         // [HiddenInput(DisplayValue = false)]
         public string DesiredLayingDate { get; set; }

         [DisplayName("Number")]
         [HiddenInput(DisplayValue = false)]
         public int? ConvertQuestionNumber { get; set; }

          [HiddenInput(DisplayValue = false)]
          public bool? IsLaid { get; set; }
       
         [HiddenInput(DisplayValue = false)]
         public string SignatureName { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string SignatureNameLocal { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string SignaturePlace { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string SignaturePlaceLocal { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string SignatureDate { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string SignatureDateLocal { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string SignatureDesignations { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string SignatureDesignationsLocal { get; set; }
         [HiddenInput(DisplayValue = false)]
         public bool? IsQuestionInPart { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string SessionDateEnglish { get; set; }

        // Sunil
         [HiddenInput(DisplayValue = false)]
         public string RefQNumber { get; set; }

         [HiddenInput(DisplayValue = false)]
         public int NoticeTypeId { get; set; }
         [HiddenInput(DisplayValue = false)]
         public int ? CMDemandId { get; set; }
         [HiddenInput(DisplayValue = false)]
         public string DemandName { get; set; }
        [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public string CutMotionFilePath { get; set; }
        [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public string CutMotionFileName { get; set; }

         [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? SrNo1 { get; set; }
         [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? SrNo2 { get; set; }
         [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int? SrNo3 { get; set; }
         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public int? Questionsequence { get; set; }

         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public string MDiaryNum { get; set; }

         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public string DocFilePath { get; set; }

          [NotMapped]
          [HiddenInput(DisplayValue = false)]
         public string DepartmentSubmittedDate { get; set; }

          [NotMapped]
          [HiddenInput(DisplayValue = false)]
          [JqGridColumnFormatter("$.paperSentOnFormatter")]
          public DateTime? SubmittedDate { get; set; }
          [NotMapped]
          [HiddenInput(DisplayValue = false)]
          public int ConstituencyID { get; set; }

          [NotMapped]
          [HiddenInput(DisplayValue = false)]
         
          public DateTime? IsAcknowledgmentDate { get;set; }

         [HiddenInput(DisplayValue = false)]
          public string IsAcknowledgmentDateString { get; set; }

           [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public int? ConcernedEventId { get; set; }
           [NotMapped]
           [HiddenInput(DisplayValue = false)]
           public string LocalLobPdf { get; set; }
           [NotMapped]
           [HiddenInput(DisplayValue = false)]
           public int? LobId { get; set; }
           [HiddenInput(DisplayValue = false)]
           public bool? IsLockedTranslator { get; set; }


           [NotMapped]
           [HiddenInput(DisplayValue = false)]
           public string AcknowledgedBy { get; set; }
           [NotMapped]
           [HiddenInput(DisplayValue = false)]
           public string SesNameEnglish { get; set; }
           [NotMapped]
           [HiddenInput(DisplayValue = false)]
           public bool? IsProofReading { get; set; }
          [NotMapped]
          [HiddenInput(DisplayValue = false)]
           public DateTime? IsProofReadingDate { get; set; }
          [NotMapped]
          [HiddenInput(DisplayValue = false)]
           public string ProofReaderUserId { get; set; }


          [NotMapped]
          [HiddenInput(DisplayValue = false)]
          public string LastVarifiedVersion { get; set; }

          [NotMapped]
          [HiddenInput(DisplayValue = false)]
          public DateTime? LastVarifiedVersionDate { get; set; }

          [NotMapped]
          [HiddenInput(DisplayValue = false)]
          public DateTime? EmailSentDate { get; set; }
    }
}
