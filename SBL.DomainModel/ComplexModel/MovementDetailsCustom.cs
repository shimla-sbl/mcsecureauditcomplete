﻿using SBL.DomainModel.Models.PaperMovement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public partial class MovementDetailsCustom
    {
       
        public long MovementDetailId { get; set; }
        public int AssemblyId { get; set; }
        public int SessionId { get; set; }       
        public string AadarId { get; set; }     
        public string Number { get; set; }
        public int? BussinessTypeId { get; set; }
        public int? PaperCategoryTypeId { get; set; }       
        public string MovementUser { get; set; }
        public DateTime? MovementDate { get; set; }
        public bool? IsActive { get; set; }       
        public virtual List<PaperMovementDetail> ListtPaperLaidMovement { get; set; }       
        public int? EmpCode { get; set; }        
        public string BussinessType { get; set; }       
        public string DepartmentName { get; set; }
        public string DepartmentId { get; set; }

    }
}
