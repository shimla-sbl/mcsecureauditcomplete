﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.Office;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.ComplexModel
{
	[Serializable]
	public class RecipientGroupViewModel
	{
		public RecipientGroupViewModel()
		{
			this.RecipientGroupsCol = new List<RecipientGroupViewModel>();

			this.DepartmentListCol = new List<mDepartment>();
		}

		public int GroupId { get; set; }

		public string GroupName { get; set; }

		public string DepartmentID { get; set; }

		public string GroupDesc { get; set; }

		public bool IsPublic { get; set; }

		public bool IsActive { get; set; }

		public List<mDepartment> DepartmentListCol { get; set; }

		public List<RecipientGroupViewModel> RecipientGroupsCol { get; set; }

		public string EditMode { get; set; }

		public int? ConstituencyCode { get; set; }

		public int RecipientGroupCount { get; set; }

		public string Message { get; set; }

		public int? PanchayatCode { get; set; }
		public List<mPanchayat> PanchayatListCol { get; set; }

		public int OfficeId { get; set; }

		public List<mOffice> OfficeList { get; set; }
		public int? RankingOrder { get; set; }
		public bool? IsConstituencyOffice { get; set; }
        
	}
}
