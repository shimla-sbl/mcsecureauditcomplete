﻿using System;
using System.Collections.Generic;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class ModuleList
    {
        public int moduleID { get; set; }

        public string ModuleName { get; set; }
    }

    [Serializable]
    public class ModuleUserList
    {
        public string ModuleUseID { get; set; }

        public string ModuleUseName { get; set; }
    }

    [Serializable]
    public class SMSEmailReport
    {
        public int count { get; set; }

        public DateTime timeSpan { get; set; }

        public List<string> MailorMobile { get; set; }

        public string subject { get; set; }

        public string Body { get; set; }

        public string status { get; set; }

        public int MessageID { get; set; }

        public string Name { get; set; }
    }

    [Serializable]
    public class memberWiseQuestionReport
    {
        public memberWiseQuestionReport()
        {
            this.OnlineCount = 0;
            this.offlineCount = 0;
            this.totalCount = 0;
            this.OnlineCountList = new List<memberWiseQuestionReportList>();
            this.offlineCountList = new List<memberWiseQuestionReportList>();
            this.postpondCounttList = new List<memberWiseQuestionReportList>();
        }

        public string ModuleName { get; set; }

        public int OnlineCount { get; set; }

        public List<memberWiseQuestionReportList> OnlineCountList { get; set; }

        public int offlineCount { get; set; }

        public List<memberWiseQuestionReportList> offlineCountList { get; set; }

        public int postpondCount { get; set; }

        public List<memberWiseQuestionReportList> postpondCounttList { get; set; }

        public int totalCount { get; set; }

        public string Type { get; set; }

        public string ruleNO { get; set; }

        public int? notAdmitted { get; set; }

        public int? admitted { get; set; }

        public int? changeType { get; set; }

        public int? breacked { get; set; }

        public int? clubbed { get; set; }

        public int? raisedInHouse { get; set; }

        public int? withdrawByMember { get; set; }

        public int? excessQuestion { get; set; }

        public int? FixedQuestion { get; set; }

        public int? timeBarred { get; set; }
    }

    [Serializable]
    public class memberWiseQuestionReportList
    {
        public int QuestionID { get; set; }

        public string Date { get; set; }

        public string Subject { get; set; }
    }

    [Serializable]
    public class memberWiseQuestionList
    {
        public int QuestionID { get; set; }

        public int? QuestionNumber { get; set; }

        public long? PaperLaidId { get; set; }

        public string ConstituencyName { get; set; }

        public string DepartmentID { get; set; }

        public string subject { get; set; }

        public string DiaryNumber { get; set; }

        public string NoticeRecievedDate { get; set; }

        public string Minister { get; set; }

        public int? memberID { get; set; }

        public string MainQuestion { get; set; }

        public string MainAnswer { get; set; }

        public string noticeType { get; set; }

        public string noticeNumber { get; set; }

        public string filePath { get; set; }

        public string MemberName { get; set; }

        public string ReceivedTime { get; set; }
    }



    [Serializable]
    public class DepartmentWiseQuestionNoticeReport
    {
        public DepartmentWiseQuestionNoticeReport()
        {
            this.OnlineCount = 0;
            this.offlineCount = 0;
            this.totalCount = 0;
            this.OnlineCountList = new List<DepartmentWiseQuestionNoticeReport>();
            this.offlineCountList = new List<DepartmentWiseQuestionNoticeReport>();
            this.postpondCounttList = new List<DepartmentWiseQuestionNoticeReport>();
        }

        public string ModuleName { get; set; }

        public int OnlineCount { get; set; }

        public List<DepartmentWiseQuestionNoticeReport> OnlineCountList { get; set; }

        public int offlineCount { get; set; }

        public List<DepartmentWiseQuestionNoticeReport> offlineCountList { get; set; }

        public int postpondCount { get; set; }

        public List<DepartmentWiseQuestionNoticeReport> postpondCounttList { get; set; }

        public int totalCount { get; set; }

        public string Type { get; set; }

        public string ruleNO { get; set; }

        public int? notAdmitted { get; set; }

        public int? admitted { get; set; }

        public int? changeType { get; set; }

        public int? ReplyNotRecieved { get; set; }

        public int? QuestionSent { get; set; }
        public int? ReplyRecieved { get; set; }

        public int? withdrawByMember { get; set; }

        public int? excessQuestion { get; set; }

        public int? timeBarred { get; set; }

        public int? notAcknowledged { get; set; }

        public int? notAccepted { get; set; }
        public int? Acknowledged { get; set; }
        public int? Accepted { get; set; }
        public int? Fixed { get; set; }
        public string AnswerAttachLocation { get; set; }
    }
}