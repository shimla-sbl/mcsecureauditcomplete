﻿using SBL.DomainModel.Models.Activity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.ComplexModel
{
   [Serializable]
    public class ActivitySearchzResult
    {
        public int Id { get; set; }

        public int ActivityId { get; set; }

        public int ActivityCategoryId { get; set; }

        public int ActivityCatCode { get; set; }

        public string ActivityName { get; set; }

        public string ActivityName_Local { get; set; }


        public int MemberCode { get; set; }

        public DateTime Date { get; set; }

        public string ActivityLocation { get; set; }

        public string ActivityDescription { get; set; }

        public string FileLocation { get; set; }
       
        public string FileName { get; set; }

        public string FilePath { get; set; }
      
       // public SelectList Dropdownlist { get; set; }

        public List<ActivitySearchzResult> ActivityDataList { get; set; }

        public List<ActivitySearchzResult> ActivityImagesList { get; set; }


        public List<ActivityResultDrp> Actdrp { get; set; }
    }
     [Serializable]
   public class ActivityResultDrp
   {
       public int Id { get; set; }
       public string ActivityName_Local { get; set; }
   }
}
