﻿using SBL.DomainModel.Models.Promotion;
using SBL.DomainModel.Models.StaffManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.ComplexModel
{
    public class ePartialViewModel
    {
        public ePartialViewModel()
        {
            this.mstaffList = new List<mStaff>();
            this.mstaffNameList = new List<mStaff>();
           // this.tPromotionsList = new List<tPromotions>();
        }
        public mStaff mStaff { get; set; }
        public tPensiondetail tPensiondetail { get; set; }
        public tPromotions tPromotions { get; set; }
        public List<mStaff> mstaffList { get; set; }
        public List<mStaff> mstaffNameList { get; set; }
        public List<tPromotions> tPromotionsList { get; set; }
        public List<tTrainingDetails> tTrainingDetailsList { get; set; } 
        public List<tLeaveDetail> tLeaveDetailList { get; set; }

        public int StaffID { get; set; }
        public string StaffName { get; set; }
        public string Mode { get; set; }
        public string PensionMode { get; set; }
        public string GrossService { get; set; }
        public string NonService { get; set; }
        public string NetService { get; set; }
        public SelectList GenderList { get; set; }
        public string Gender { get; set; }
        public string GPFCPF { get; set; }

        public string ImageShow { get; set; }
        public string WheatherContribution { get; set; }


        public int OrderNumber { get; set; }

        public string Designation { get; set; }

        public string DOJ { get; set; }

        public string Branch { get; set; }

        public string PayScale { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}
