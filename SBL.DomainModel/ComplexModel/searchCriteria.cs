﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class FillDesignation
    {
        public FillDesignation()
        {
            //this.FillDesignationCol = new List<FillDesignation>();
        }

        public string designationName { get; set; }

        public int? designationID { get; set; }

        //public ICollection<FillDesignation> FillDesignationCol { get; set; }
    }
    [Serializable]
    public class fillCurrentMamber
    {
        public string MemberName { get; set; }
        public int MemberCode { get; set; }
    }
    [Serializable]
    public class SpeakerList
    {
        public int speakerID { get; set; }
        public string speakerName { get; set; }
    }
    [Serializable]
    public class fillLoanee
    {
        public string MemberName { get; set; }
        public long loaneeID { get; set; }
    }
    [Serializable]
    public class fillLoanAccountList
    {
        public string MemberName { get; set; }
        public string accountNumber { get; set; }
    }
    [Serializable]
    public class fillLoanType
    {
        public int loanID { get; set; }
        public string loanType { get; set; }
        public string AccountHeadsName { get; set; }

    }
    [Serializable]
    public class fillListGenric
    {

        public string Text { get; set; }
        public string value { get; set; }

    }
    [Serializable]   //Madhur
    public class fillDistrict
    {
        public int? DistrictCode { get; set; }
        public string DistrictName { get; set; }
    }

    [Serializable]
    public class fillListGenricInt
    {
        public string Text { get; set; }
        public int value { get; set; }
    }
    [Serializable]
    public class fillListGenricLong
    {
        public string Text { get; set; }
        public long value { get; set; }
    }
    [Serializable]
    public class fillSessionDate
    {

        public string Text { get; set; }
        public DateTime value { get; set; }

    }

    [Serializable]
    public class fillDepartment
    {
        public string DepartmentName { get; set; }
        public string DepartrmentID { get; set; }
    }
    [Serializable]
    public class FillBranch
    {
        public FillBranch()
        {
            //this.FillDesignationCol = new List<FillDesignation>();
        }

        public string BranchName { get; set; }

        public int? BranchId { get; set; }
    }
}
