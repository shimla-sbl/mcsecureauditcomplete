﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class MemberAttendanceViewModel
    {

        public int MemberCode { get; set; }

        public string name { get; set; }

        public string constituencyname { get; set; }

        public DateTime AttendanceDate { get; set; }

        public string MemberSignaturePath { get; set; }

        public bool Status { get; set; }

        public int AssemblyID { get; set; }

        public int sessionId { get; set; }
        public string AttenIds { get; set; }
        public string Date { get; set; }

    }
}
