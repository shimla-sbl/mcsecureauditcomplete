﻿using Lib.Web.Mvc.JQuery.JqGrid;
using Lib.Web.Mvc.JQuery.JqGrid.Constants;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using SBL.DomainModel.Models.Question;
using System;
using System.ComponentModel;
using System.Web.Mvc;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class NoticePaperSendModelCustom
    {
        #region Constructors
        public NoticePaperSendModelCustom()
        {
        }
        public NoticePaperSendModelCustom(NoticePaperSendModelCustom questionList)
        {
            this.QuestionID = questionList.QuestionID;
            this.QuestionNumber = questionList.QuestionNumber;
            this.Subject = questionList.Subject;
            this.PaperLaidId = questionList.PaperLaidId;
            this.Version = questionList.Version;
            this.DeptActivePaperId = questionList.DeptActivePaperId;
            this.MemberName = questionList.MemberName;
            this.MinisterSubmittedDate = questionList.MinisterSubmittedDate;
            this.DeptSubmittedDate = questionList.DeptSubmittedDate;
            this.MinisterActivePaperId = questionList.MinisterActivePaperId;
            this.PaperSent = questionList.PaperSent;
            this.FileName = questionList.FileName;
            this.EventName = questionList.EventName;
            this.Status = questionList.Status;
            this.AssemblyCode = questionList.AssemblyCode;
            this.SessionCode = questionList.SessionCode;
            this.QuestionTypeId = questionList.QuestionTypeId;
            this.MinistryId = questionList.MinistryId;
            this.ResultCount = questionList.ResultCount;
            this.PaperLaidIdForCheckBox = Convert.ToString(questionList.PaperLaidId);
            this.DesireLayingDate = questionList.DesireLayingDate;
            this.DeptSubmittedDate = questionList.DeptSubmittedDate;
            this.RuleNo = questionList.RuleNo;
            this.NoticeId = questionList.NoticeId;
            this.evidhanReferenceNumber = questionList.evidhanReferenceNumber;
        }
        #endregion

        #region Properties

        [JqGridColumnFormatter("$.NoticePaperLaidIdForCheckBoxFormatter")]
        [DisplayName("Tick to Send")]
        public string PaperLaidIdForCheckBox { get; set; }

       // [DisplayName("Ref.Number")]
         [HiddenInput(DisplayValue = false)]
        public string evidhanReferenceNumber { get; set; }

        //[DisplayName("Rule")]
          [HiddenInput(DisplayValue = false)]
        public string RuleNo { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int QuestionID { get; set; }

        [DisplayName("Number")]
        public string QuestionNumber { get; set; }

        public string Subject { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? PaperLaidId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? Version { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? DeptActivePaperId { get; set; }

        [DisplayName("Asked By")]
        public string MemberName { get; set; }
        //New
        [HiddenInput(DisplayValue = false)]
        public DateTime? MinisterSubmittedDate { get; set; }

        [DisplayName("Paper")]
        [JqGridColumnLayout(Alignment = JqGridAlignments.Center)]
        [JqGridColumnFormatter("$.noticepaperSentFormatter")]
        public string PaperSent { get; set; }

        [DisplayName("Sent on")]
        //[JqGridColumnFormatter(JqGridColumnPredefinedFormatters.Date, SourceFormat = "d.m.Y G:i:s", OutputFormat = "d.m.Y g:i s")]
        [JqGridColumnFormatter("$.paperSentOnFormatter")]
        public DateTime? DeptSubmittedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? MinisterActivePaperId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string FileName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string EventName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int Status { get; set; }

        //Added by uday
        [HiddenInput(DisplayValue = false)]
        public int AssemblyCode { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SessionCode { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int QuestionTypeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int MinistryId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }
        #endregion        

        //Added By Sammer
        [HiddenInput(DisplayValue = false)]
        public string DiaryNumber { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime? NoticeDate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? MemberId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string MainQuestion { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int EventId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public TimeSpan? NoticeTime { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? IsContentFreeze { get; set; }
        [HiddenInput(DisplayValue = false)]
        public bool? Stats { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string NoticeNumber { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int NoticeId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string Notice { get; set; }
        [HiddenInput(DisplayValue = false)]
        public Guid UserId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string UserName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public Guid UId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFilePath { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string OriDiaryFileName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string TypistUserId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string SubjectCatchWords { get; set; }

        //Himanshu
          [HiddenInput(DisplayValue = false)]
        public string MemberNameLocal { get; set; }
          [HiddenInput(DisplayValue = false)]
        public string ConstituencyName { get; set; }
          [HiddenInput(DisplayValue = false)]
        public string ConstituencyName_Local { get; set; }

          //Sujeet
          [DisplayName("Desired Date")]
          [JqGridColumnFormatter("$.NoticeSendFixedDateOnFormatter")]
          public DateTime? DesireLayingDate { get; set; }

          [DisplayName("Number")]
          [HiddenInput(DisplayValue = false)]
          public int? ConvertQuestionNumber { get; set; }
    }
}
