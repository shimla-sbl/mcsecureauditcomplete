﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SBL.DomainModel.Models.User;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class EMailModel
    {
        public EMailModel()
        {
            this.MinisterContactCol = new List<MinisterContactViewModel>();

            this.MemberContactCol = new List<MemberContactViewModel>();

            this.UserContactCol = new List<mUsers>();
        }

        public string To { get; set; }

        public string Subject { get; set; }

        [AllowHtml]
        public string Message { get; set; }

        public ICollection<MinisterContactViewModel> MinisterContactCol { get; set; } 

        public ICollection<MemberContactViewModel> MemberContactCol { get; set; }

        public ICollection<mUsers> UserContactCol { get; set; }

        public string Result { get; set; }

        public bool SendSMS { get; set; }

        public bool SendEmail { get; set; }
    }
}
