﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel.EPWSanctionBill
{
    [Serializable]
    public class MLAWaterSanction
    {
        public string Name { get; set; }
        public string SetNo { get; set; }
        public string MeterNo { get; set; }
        public string BillDate { get; set; }
        public string TotalAmount { get; set; }
    }
}
