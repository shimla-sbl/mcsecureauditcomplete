﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel.EPWSanctionBill
{
    [Serializable]
    public class MLAPhoneSanction
    {
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string DateRange { get; set; }
        public string TotalAmount { get; set; }
    }
}
