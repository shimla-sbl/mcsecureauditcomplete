﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel.EPWSanctionBill
{
    [Serializable]
    public class SanctionTotalAmount
    {
        public int TotalAmount { get; set; }
    }
}
