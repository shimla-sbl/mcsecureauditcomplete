﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;

namespace SBL.DomainModel.ComplexModel.MediaComplexModel
{
    [Serializable]
    public partial class MediaModel
    {
        //For List Of Assembly and Session-- sanjay
        public string AssemblyName { get; set; }

        public string SessionName { get; set; }
        public int AssemblyId { get; set; }
        public int SessionId { get; set; }


        public IList<mAssembly> mAssemblyList { get; set; }


        public IList<mSession> sessionList { get; set; }

    }
}
