﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Passes;

namespace SBL.DomainModel.ComplexModel.MediaComplexModel
{
    [Serializable]
    public class FilterModel
    {
        public int SessionCode { get; set;}
        public int AssemblyCode { get; set; }
        public string DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int PassCategoryID { get; set; }
        public string PassTypeName { get; set; }
        public int TotalCount { get; set; }
        public int TotalDeptPanding { get; set; }
        public int TotalDeptApproval { get; set; }
        public int TotalJApproval { get; set; }
        public int TotalJPanding { get; set; }
        public int TotalPPanding { get; set; }
        public int TotalPApproved { get; set; }


    }
}
