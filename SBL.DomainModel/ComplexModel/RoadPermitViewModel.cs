﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.RoadPermit;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class RoadPermitViewModel
    {
        public RoadPermitViewModel()
        {
            this.MemberCol = new List<mMember>();

            this.DesignationCol = new List<mMemberDesignation>();

            this.ConstituencyCol = new List<mConstituency>();

            this.SealedRoadsCol = new List<mSealedRoads>();

            this.RestrictedRoadsCol = new List<mRestrictedRoads>();
        }

        public string Mode { get; set; }
        public int? PermitNo { get; set; }

        public string PermitCode { get; set; }

        public int? MemberCode { get; set; }

        public string Address { get; set; }

        public ICollection<mConstituency> ConstituencyCol { get; set; }

        public ICollection<mMember> MemberCol { get; set; }

        public ICollection<mMemberDesignation> DesignationCol { get; set; }

        public ICollection<mSealedRoads> SealedRoadsCol { get; set; }
        public ICollection<tPermitSealedRoads> SealedRoadsColA { get; set; }

        public ICollection<mRestrictedRoads> RestrictedRoadsCol { get; set; }
        public ICollection<tPermitRestrictedRoads> RestrictedRoadsColA { get; set; }



        // public ICollection<mRestrictedRoads> RestrictedRoadsColNew { get; set; }
        //    public List<tPermitRestrictedRoads> RestrictedRoadsColNew { get; set; }
        //    public List<mRestrictedRoads> RestrictedRoadsColNew1 { get; set; }
        public bool ischecked { get; set; }

        //  public List<mSealedRoads> SealedRoadsColNew { get; set; }

        public int? MemberConstituencyCode { get; set; }

        public string VehicleNo { get; set; }

        public string ValidFrom { get; set; }

        public string ValidTo { get; set; }

        public int ConstituencyCode { get; set; }

        public int DesignationCode { get; set; }
        public int AssemblyId { get; set; }

        //    public SelectList FinancialYearList { get; set; }
        public string FinancialYear { get; set; }


        public ICollection<mAssembly> AssemblyCol { get; set; }


    }
}
