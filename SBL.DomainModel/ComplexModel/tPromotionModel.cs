﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    public  class tPromotionModel
    {
        public tPromotionModel()
        {
           
           

        }


        public int OrderNumber { get; set; }

        [StringLength(200)]
        public string Designation { get; set; }
        [Display(Name = "Date of Joining")]
        public string DOJ { get; set; }
        [StringLength(200)]
        public string Branch { get; set; }
        [StringLength(200)]
        public string PayScale { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}
