﻿using SBL.DomainModel.Models.Pension;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class PensionReport
    {
        public int pensionerID { get; set; }
        public string pensionerName { get; set; }
        public string address { get; set; }
        public DateTime firstDate { get; set; }
        public DateTime lastDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public int firstTermYear { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public int extraYear { get; set; }
        public List<pensionerTenuresDetails> _pensionerTenuresDetails { get; set; }
        public string pensionAmount { get; set; }
        public string PPONO { get; set; }
        public string bankName { get; set; }
        public List<string> AmountList { get; set; }
        public double totalPension { get; set; }
        public List<string> tPeriodList { get; set; }



       

    }
}
