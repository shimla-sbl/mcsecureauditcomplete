﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Diaries;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class QuestionListPDFViewModel
    {
        public QuestionListPDFViewModel()
        {
            this.QuestionListCol = new List<QuestionListPDFViewModel>();

            this.QuestionTypeCol = new List<tQuestionTypeModel>();

            this.MemberCol = new List<mMember>();

            this.AssemblyCol = new List<mAssembly>();

            this.SessionCol = new List<mSession>();
            this.NewmemberList = new List<DiaryModel>();
        }

        public int? AssemblyCode { get; set; }

        public int? SessionID { get; set; }

        public DateTime? NoticeRecievedDate { get; set; }

        public TimeSpan? NoticeRecievedTime { get; set; }

        public int? QuestionNumber { get; set; }

        public int? QuestionType { get; set; }

        public int? MemberCode { get; set; }

        public string DiaryNumber { get; set; }

        public string Name { get; set; }

        public string Subject { get; set; }

        public string MainQuestion { get; set; }

        public DateTime? IsFixedDate { get; set; }

        public ICollection<tQuestionTypeModel> QuestionTypeCol { get; set; }

        public ICollection<QuestionListPDFViewModel> QuestionListCol { get; set; }

        public ICollection<mMember> MemberCol { get; set; }

        public ICollection<mAssembly> AssemblyCol { get; set; }

        public ICollection<mSession> SessionCol { get; set; }
        public int AID { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public IList<DiaryModel> NewmemberList { get; set; }
    }
}
