﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class MinisterContactViewModel
    {
        public string MinisterName { get; set; }
        public string MinistryName { get; set; }
        public string EMail { get; set; }
        public string Mobile { get; set; }
    }
}
