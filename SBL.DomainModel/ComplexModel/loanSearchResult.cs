﻿using SBL.DomainModel.Models.Loan;
using System;
using System.Collections.Generic;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class loanSearchResult
    {
        public string memberName { get; set; }

        public int? memberCode { get; set; }

        public long? loaneeCode { get; set; }

        public string designation { get; set; }

        public string contactNumber { get; set; }


        public string constituencyName { get; set; }

        public int constituencyCode { get; set; }

        public string Address { get; set; }

        public int anyActiveLoan { get; set; }

        public long? loanCode { get; set; }
        public double? loanAmount { get; set; }
        public string LoanName { get; set; }
        public DateTime? EMIDate { get; set; }
        public double? EMIAmount { get; set; }
        public double? EMIPAmount { get; set; }
        public double? EMIIAmount { get; set; }
        public int? installmentID { get; set; }
        public int? totalEMI { get; set; }
        public double? intRate { get; set; }
        public String sanDate { get; set; }
        public DateTime? relDate { get; set; }

        public double? outstarndingBalance { get; set; }
        public double? outstarndingIntereset { get; set; }
        public string lastInstallment { get; set; }
        public string loanAccontNumber { get; set; }

    }

    [Serializable]
    public class memberAccountSearchResult
    {
        public string memberName { get; set; }
        public int? memberCode { get; set; }
        public string designation { get; set; }
        public string contactNumber { get; set; }
        public string constituencyName { get; set; }
        public int constituencyCode { get; set; }
        public string Address { get; set; }
        public string BankDetails { get; set; }

        public string BankName { get; set; }
        public string IFSCCode { get; set; }
        public Int64 AccountNumber { get; set; }

    }


    [Serializable]
    public class disbursmentList
    {
        public int autoID { get; set; }
        public long loanNumber { get; set; }
        public string Name { get; set; }
        public string designation { get; set; }
        public int? loanTypeID { get; set; }
        public string loanTypeName { get; set; }
        public double totalLoanAmount { get; set; }
        public double givenAmount { get; set; }
        public double? receiveAmount { get; set; }
        public double? pendingAmount { get; set; }
        public double disburmentAMount { get; set; }
        public string disburmenDate { get; set; }
        public string disbursmentRecDate { get; set; }
        public bool status { get; set; }
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public List<disbursementDetails> disbursmentListofMember { get; set; }

    }

    [Serializable]
    public class disbursmentListofMember
    {
        public string Date { get; set; }
        public string Amount { get; set; }
        public string status { get; set; }
    }

    [Serializable]
    public class installmentDetails
    {

        public int memberCode { get; set; }
        public string memberName { get; set; }
        public string Designation { get; set; }
        public string mobileNumber { get; set; }
        public string Address { get; set; }
        public int ConsCode { get; set; }
        public string ConsName { get; set; }
        public long loanNumber { get; set; }
        public string loanAccountNumber { get; set; }
        public string loanType { get; set; }
        public int loantypeID { get; set; }
        public string sancationDate { get; set; }
        public double loanAmount { get; set; }
        public int noOFEMI { get; set; }
        public double intRate { get; set; }
        public int EMI { get; set; }
        public double pAmount { get; set; }
        public double iAmount { get; set; }
        public double paidInt { get; set; }
        public string PaymentMode { get; set; }
        public string PaymentCode { get; set; }
        public string PaymentDate { get; set; }
        public string treasuryName { get; set; }
        public string DDOCode { get; set; }
        public string voucherNumber { get; set; }
        public string voucherDate { get; set; }
        public string transType { get; set; }
        public long? AutoID { get; set; }
        public double? outstarndingBalance { get; set; }
        public double? outstarndingIntereset { get; set; }
        public string lastInstallment { get; set; }
        public int month { get; set; }
        public int year { get; set; }

    }

    [Serializable]
    public class LoanStatement
    {
        public string memberName { get; set; }
        public string LoaNNumber { get; set; }
        public string LoanAmount { get; set; }
        public string EMI { get; set; }
        public string senctionDate { get; set; }
        public List<statementList> statement { get; set; }

    }
    [Serializable]

    public class statementList
    {
        public long autoID { get; set; }
        public int instno { get; set; }
        public string Date { get; set; }
        public double pAmount { get; set; }
        public double iAmount { get; set; }
        public double interest { get; set; }
        public double outstandingint { get; set; }
        public double openingBalance { get; set; }
        public double closingBalance { get; set; }
        public double intsRate { get; set; }
        public string PaymentMode { get; set; }
        public string Voucher { get; set; }
        public string TreasuryName { get; set; }
        public string DDOCode { get; set; }

    }
    [Serializable]
    public class loanChallanDetails
    {
        public int installmentID { get; set; }
        public string loanName { get; set; }
        public string Tenderedby { get; set; }
        public string paymentMode { get; set; }
        public string pAmount { get; set; }
        public string iAmount { get; set; }
        public string Amount { get; set; }
        public string AmountInWords { get; set; }
        public string PAmountInWords { get; set; }
        public string IAmountInWords { get; set; }
        public string TreasuryCode { get; set; }
        public string DDOCODE { get; set; }
        public long loanNumber { get; set; }
        public List<string> CodesList { get; set; }
        public List<string> CodetextList { get; set; }



    }

    [Serializable]
    public class financialYearList
    {
        public string yearText { get; set; }
        public string yearValue { get; set; }
    }

    [Serializable]
    public class YearlyReport
    {
        public string loanName { get; set; }
        public string pAmount { get; set; }
        public string iAmount { get; set; }
        public string Amount { get; set; }

    }
    [Serializable]
    public class otherLoanReports
    {
        public string memberName { get; set; }
        public string loanName { get; set; }
        public string FromDate { get; set; }
        public string toDate { get; set; }
        public string LoaNNumber { get; set; }
        public string LoanAmount { get; set; }
        public string EMI { get; set; }
        public string senctionDate { get; set; }
        public List<YearlyReport> YearlyReport { get; set; }
    }



}
