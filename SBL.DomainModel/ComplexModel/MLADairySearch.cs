﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.ConstituencyVS;
using System.ComponentModel.DataAnnotations;

namespace SBL.DomainModel.ComplexModel
{
    [Serializable]
    public class MLADairySearch
    {
     [Key]
        public int mlaDiaryId { get; set; }
        public int? RecordNumber { get; set; }
        public string DocmentType { get; set; }
        public string Subject { get; set; }
        public int? MinisterId { get; set; }
        public string DeptId { get; set; }
        public int? ActionCode { get; set; }
        public DateTime? ForwardDate { get; set; }
        public DateTime? DiaryDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? DiaryLanguage { get; set; }
        public string DocumentCC { get; set; }
        public string DiaryRefId { get; set; }
        public int? OfficeId { get; set; }
        public string OfficeId_CC { get; set; }
        public string Subject2 { get; set; }
     
    }
}
